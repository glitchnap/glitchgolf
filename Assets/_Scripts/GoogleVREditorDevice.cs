﻿// Copyright 2015 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//Edited by @pyjamads, for a specific golfing usecase.
#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// @cond
namespace Gvr.Internal
{
    // Sends simulated values for use when testing within the Unity Editor.
    public class GoogleVREditorDevice : BaseVRDevice
    {
        // Simulated neck model.  Vector from the neck pivot point to the point between the eyes.
        private static readonly Vector3 neckOffset = new Vector3(0, 0.075f, 0.08f);

        // Use mouse to emulate head in the editor.
        private float inputX = 0;
        private float inputY = 0;
        private float inputZ = 0;

        public override void Init()
        {
            Debug.Log("Started EditorDevice!");
            Input.gyro.enabled = true;
        }
            
        // Since we can check all these settings by asking Gvr.Instance, no need
        // to keep a separate copy here.
        public override void SetVRModeEnabled(bool enabled)
        {
        }

        public override void SetNeckModelScale(float scale)
        {
        }

        private Quaternion initialRotation = Quaternion.identity;

        private bool remoteCommunicating = false;

        private bool RemoteCommunicating
        {
            get
            {
                if (!remoteCommunicating)
                {
                    remoteCommunicating = EditorApplication.isRemoteConnected;
                }
                return remoteCommunicating;
            }
        }

        public override void UpdateState()
        {
            Quaternion rot;
            if (RemoteCommunicating) // && GvrViewer.Instance.useUnityRemote
            {
                var att = Input.gyro.attitude * initialRotation;
                att = new Quaternion(att.x, att.y, -att.z, -att.w);
                rot = Quaternion.Euler(90, 0, 0) * att;
            }
            else
            {
                bool rolled = false;
                if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.LeftAlt) || Input.GetMouseButton(1))
                {
                    inputY += Input.GetAxis("Mouse X") * 5;
                    if (inputY <= -180)
                    {
                        inputY += 360;
                    }
                    else if (inputY > 180)
                    {
                        inputY -= 360;
                    }
                }
                else
                {
                    rolled = true;
                    var yRotation = inputY;
                    while (yRotation < 0)
                    {
                        yRotation += 360;
                    }

                    yRotation = yRotation % 360;
                        
                    inputZ -= (Input.GetAxis("Mouse X") * Mathf.Cos(yRotation * Mathf.Deg2Rad) - Input.GetAxis("Mouse Y") * Mathf.Sin(yRotation * Mathf.Deg2Rad)) * 5;    
                    inputX -= (Input.GetAxis("Mouse Y") * Mathf.Cos(yRotation * Mathf.Deg2Rad) + Input.GetAxis("Mouse X") * Mathf.Sin(yRotation * Mathf.Deg2Rad)) * 2.4f;

                    inputZ = Mathf.Clamp(inputZ, -85, 85);

                    inputX = Mathf.Clamp(inputX, 120, 150);
                }

                if (!rolled) // && GvrViewer.Instance.autoUntiltHead
                {
                    // People don't usually leave their heads tilted to one side for long.
                    inputZ = Mathf.Lerp(inputZ, 0, Time.deltaTime / (Time.deltaTime + 0.1f));
                    //mouseY = Mathf.Lerp(mouseY, 135, Time.deltaTime / (Time.deltaTime + 0.1f));
                }
                rot = Quaternion.Euler(inputX, inputY, inputZ);
            }
            //GvrViewer.Instance.NeckModelScale = 0;
            var neck = (rot * neckOffset - neckOffset.y * Vector3.up) * 0;//GvrViewer.Instance.NeckModelScale;
            headPose.Set(neck, rot);

            tilted = Input.GetKeyUp(KeyCode.Escape);
        }

        public override void UpdateScreenData()
        {
            //Profile = GvrProfile.GetKnownProfile(GvrViewer.Instance.ScreenSize, GvrViewer.Instance.ViewerType);
            Profile = GvrProfile.Default;
            ComputeEyesFromProfile();
            profileChanged = true;
        }

        public override void Recenter()
        {
            inputY = inputZ = 0;  // Do not reset pitch, which is how it works on the phone.
            if (RemoteCommunicating)
            {
                //initialRotation = Quaternion.Inverse(Input.gyro.attitude);
            }
        }
    }
}
/// @endcond

#endif
