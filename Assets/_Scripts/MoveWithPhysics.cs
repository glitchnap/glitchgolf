﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MoveWithPhysics : MonoBehaviour
{
    public float frequency;
    public float amplitude;
    public Vector3 axis;
    public bool on;
    private Vector3 startPos;
    private float timer;
    public bool abs;
    public bool randomOffset;
    public float offset = 0;
    private Rigidbody rb;
    public AnimationCurve speedCurve;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (randomOffset) offset = Random.value * frequency;
       
        startPos = transform.position;
       
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (on)
        {
            timer += Time.deltaTime;

            float val = Mathf.Sin(timer * frequency + offset) * amplitude;

            if (speedCurve != null && speedCurve.keys.Length > 0)
            {
                val = speedCurve.Evaluate(timer * frequency + offset) * amplitude;
            }

            if (abs) val = Mathf.Abs(val);
           
            var result = transform.position;
            if (axis.x != 0)
                result = result.withX(startPos.x + val);
            if (axis.y != 0)
                result = result.withY(startPos.y + val);
            if (axis.z != 0)
                result = result.withZ(startPos.z + val);

            rb.MovePosition(result);
        }
    }
}
