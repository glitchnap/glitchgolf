﻿using UnityEngine;
using System.Collections;

public class StretchWithAnimation : MonoBehaviour {
    public Transform target;
    private float coefficient;
    public float modifier;
    private float baseDist;

    void Start()
    {
        baseDist = Vector3.Distance(transform.position, target.position);
        coefficient = transform.localScale.y / baseDist;
        
    }

	void Update () {
        transform.localScale = transform.localScale.withY(coefficient * ((Vector3.Distance(transform.position, target.position) - baseDist) * modifier + baseDist));

    }
}
