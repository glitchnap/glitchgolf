﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

//[RequireComponent(typeof(Rigidbody), typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class BallSettings : MonoBehaviour
{
    public BallTypes ballType;
    public PhysicMaterial mat;
    private Rigidbody rb;
    public string ballNiceName;

    // Use this for initialization
    void Start()
    {
        var settings = PutterAndBallManager.instance.ballPhysicsSettings;
        var sharedSettings = PutterAndBallManager.instance.sharedBallSettings;

        rb = GetComponent<Rigidbody>();
        mat = GetComponentInChildren<Collider>().material;
        var ballRollingPhysics = GetComponent<BallRollingPhysics>();

        var setting = settings.SingleOrDefault(item => item.type == ballType);

        if (setting != null)
        {
            rb.mass = setting.mass;
            rb.drag = setting.drag;
            rb.angularDrag = setting.angularDrag;

            if (sharedSettings.limitAngularVelocity)
            {
                rb.maxAngularVelocity = sharedSettings.maxAngularVelocity;
            }

            mat.staticFriction = setting.staticFriction;
            mat.dynamicFriction = setting.dynamicFriction;
            mat.bounciness = setting.bounciness;
            mat.frictionCombine = setting.frictionCombine;
            mat.bounceCombine = setting.bounceCombine;

            if (ballRollingPhysics != null)
            {
                ballRollingPhysics.useFakeSleepThreshold = sharedSettings.useFakeSleep;
                ballRollingPhysics.fakeSleepThreshold = sharedSettings.fakeSleepThreshold;
                ballRollingPhysics.fakeVelocityReductionRate = sharedSettings.fakeVelocityReductionRate;
                ballRollingPhysics.fakeVelocityCutoff = sharedSettings.fakeVelocityCutoff;

                ballRollingPhysics.limitVelocity = sharedSettings.limitVelocity;
                ballRollingPhysics.maxVelocity = sharedSettings.maxVelocity;

                ballRollingPhysics.useHoleMagnetism = sharedSettings.useHoleMagnetism;
                ballRollingPhysics.magnetismRadius = sharedSettings.magnetismRadius;
                ballRollingPhysics.magnetismAmount = sharedSettings.magnetismAmount;
                ballRollingPhysics.magnetismApproachAngle = sharedSettings.magnetismApproachAngle;
                ballRollingPhysics.magnetismMaxTime = sharedSettings.magnetismMaxTime;
            }

            if (setting.customGravity != Vector3.zero)
            {
                Physics.gravity = setting.customGravity;
            }
        }

    }
	
    // Update is called once per frame
    void Update()
    {
	
    }
}
