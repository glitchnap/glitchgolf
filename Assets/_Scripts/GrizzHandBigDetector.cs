﻿using UnityEngine;
using System.Collections;

public class GrizzHandBigDetector : MonoBehaviour {
    public float force;
    public void OnTriggerStay(Collider col)
    {
        if (!col.GetComponent<Rigidbody>().isKinematic)
        {
            col.GetComponent<Rigidbody>().useGravity = false;
            col.GetComponent<Rigidbody>().velocity *= 0.9f;
            col.GetComponent<Rigidbody>().AddForce((transform.position - col.transform.position) * force);
        }
    }

}
