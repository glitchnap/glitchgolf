using UnityEngine;
using System.Collections;

public class GolfHole : MonoBehaviour
{
    public Transform StartPosition;
    public Transform Hole;
    public Transform FlyOverCenter;
    [ReadOnly]
    public Vector3 FlyOverCenterPosition;
    public int par = 3;
    public bool allowFloorTouching;
    public Star[] Stars;

    void Awake()
    {
        if (StartPosition == null)
        {
            StartPosition = transform.FindChild("Start");
        }

        if (Hole == null)
        {
            Hole = transform.FindChild("Finish");
        }

        if (FlyOverCenter == null)
        {
            FlyOverCenter = transform.FindChild("FlyOverCenter");
            if (FlyOverCenter == null)
            {
//                if (Hole != null && StartPosition != null)
//                {
//                    FlyOverCenterPosition = (StartPosition.position + Hole.position) / 2f;
//                }
//                else
//                {
//                    FlyOverCenterPosition = transform.position;  
//                }
                FlyOverCenterPosition = transform.position;
            }
            else
            {
                FlyOverCenterPosition = FlyOverCenter.position;
            }
        }
        else
        {
            FlyOverCenterPosition = FlyOverCenter.position;
        }
    }

    void Start()
    {
        Stars = GetComponentsInChildren<Star>();
    }
}
