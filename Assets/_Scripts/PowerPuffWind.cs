﻿using UnityEngine;
using System.Collections;

public class PowerPuffWind : MonoBehaviour
{

    public float force;
    public GameObject trail;
    public static GameObject newTrail;

    void DestroyTrail()
    {
        if (newTrail != null)
            Destroy(newTrail, 2);
    }

    void OnTriggerEnter(Collider col)
    {
        DestroyTrail();
        if (col.tag == "Ball")
        {
            var rb = col.GetComponent<Rigidbody>();
            if (!rb.IsSleeping())
            {
                newTrail = Instantiate(trail, col.transform) as GameObject;
                newTrail.transform.ResetLocal();
                newTrail.GetComponent<TrailRenderer>().startWidth = 0.08f;
            }
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Ball")
        {
            var rb = col.GetComponent<Rigidbody>();
            if (!rb.IsSleeping())
                rb.AddForce(transform.forward * force);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Ball")
        {
            DestroyTrail();
        }
    }
}
