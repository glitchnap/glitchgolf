using UnityEngine;
using System.Collections;

public class DebugLineToOtherTransform : MonoBehaviour {
    public Transform other;
    public bool on;

    void Start()
    {
        // should only work in edit mode...
        Destroy(this);
    }

    void OnDrawGizmos()
    {
        if (other == null) return;

        if (!on) return;
        Gizmos.color = Color.white;
        if(other!=null)
            Gizmos.DrawLine(transform.position, other.position);
        Gizmos.DrawSphere(transform.position, 0.04f);

        var dist = (transform.position - other.position).magnitude;
        Gizmos.color = Color.red;

        Gizmos.DrawLine(transform.position, transform.position + Vector3.down * dist);
        Gizmos.DrawSphere(transform.position + Vector3.down * dist, 0.04f);
    }
}
