﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreBoardBackgroundStars : MonoBehaviour {
    private Image[] stars;
    
    void Start()
    {
        ShowStars(0);
    }

    void Init()
    {
        if(stars == null)
        {
            stars = GetComponentsInChildren<Image>();
        }
        if(stars == null)
        {
            stars = new Image[0];
        }
    }

    public void ShowStars(int count)
    {
        Init();
        for (int i = 0; i < stars.Length; i++)
        {
            stars[i].enabled = false;
        }

        for (int i = 0; i < Mathf.Min(stars.Length, count); i++)
        {
            stars[i].enabled = true;
        }
    }
}
