﻿using UnityEngine;
using System.Collections;

public class AdjustPhysicsVelocity : MonoBehaviour
{
    public Vector3 targetDirection;
    public float magnitudeOverride;
    public float lerpSpeed;

    void OnTriggerStay(Collider col)
    {
        var rb = col.GetComponent<Rigidbody>();

        if (rb == null) return; //Only affect rigidbodies

        var tempVel = transform.TransformPoint(targetDirection) - transform.position;

        var tempMagnitude = magnitudeOverride;

        if (magnitudeOverride == 0)
        {
            tempMagnitude = (new Vector3(
                (tempVel.x != 0 ? rb.velocity.x : 0), 
                (tempVel.y != 0 ? rb.velocity.y : 0), 
                (tempVel.z != 0 ? rb.velocity.z : 0))
            ).magnitude;
        }

        tempVel = tempVel.normalized * tempMagnitude;

        if (tempVel.x == 0) tempVel.x = rb.velocity.x;
        if (tempVel.y == 0) tempVel.y = rb.velocity.y;
        if (tempVel.z == 0) tempVel.z = rb.velocity.z;
        
        rb.velocity = Vector3.Lerp(rb.velocity, tempVel, lerpSpeed);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        var tempVel = transform.TransformPoint(targetDirection) - transform.position;
        Gizmos.DrawLine(transform.position, transform.position + tempVel);
    }
}
