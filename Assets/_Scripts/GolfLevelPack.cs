﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GolfLevelPack
{
    public Sprite smallCoursePreview;
    //[HideInInspector]
    public string sceneName;
    public string niceName;
    public int sortOrder;
#if UNITY_EDITOR
    public UnityEditor.SceneAsset scene;
    #endif
}
