﻿using UnityEngine;
using System.Collections;

public class CoursePreviewCamera : MonoBehaviour
{

    public float rotateSpeed;

    void Start()
    {
	
    }

    // TODO: Optimize update
    void Update()
    {
        if (CourseManager.instance.CurrentCourse != null)
        {
            transform.parent.position = CourseManager.instance.GetCurrentHole().FlyOverCenterPosition;
        }
        transform.parent.Rotate(Vector3.up, rotateSpeed * Time.deltaTime, Space.Self);
    }
}
