﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class BridgeSpinner : MonoBehaviour
{
    public Vector3 direction;
    public float speed;
    public AnimationCurve speedCurve;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rb.MoveRotation(Quaternion.Euler(direction * speedCurve.Evaluate(Mathf.Sin(Time.time * speed) + 1)) * Quaternion.Euler(0, 90, 0) * transform.parent.rotation);  
    }
}
