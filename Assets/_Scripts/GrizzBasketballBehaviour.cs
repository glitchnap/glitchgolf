﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using System;

public class GrizzBasketballBehaviour : MonoBehaviour {

    Sequence seq;
    public BezierCurve curve;
    public Transform grizz;
    public Animator animator;
    public AnimationCurve animcurve; 
    public float x;
    private bool needsRestore;
    private Rigidbody rb;
    
    void Start()
    {
        CourseManager.OnHoleChanged += onHoleChanged;
    }

    private void onHoleChanged(int obj)
    {
        Restore(rb);
    }

    public void OnTriggerEnter(Collider col)
    {
        ExecuteDunk(col.GetComponent<Rigidbody>());
    }

    public void ExecuteDunk(Rigidbody rb)
    {
        DebugExtension.Blip();

        if (seq != null && !seq.isComplete)
        {
            var sc = rb.GetComponent<SimpleConstraints>();
            if (sc != null) Destroy(sc);

            seq.Kill();
        }

        seq = new Sequence();

        rb.isKinematic = true;

        // turn 
        var l = 1f;
        var t = 1f;

        needsRestore = true;
        this.rb = rb;
        var simplec = rb.gameObject.AddComponent<SimpleConstraints>();
        simplec.targetPosition = transform;

        seq.Append(HOTween.To(grizz, l, new TweenParms()
            .Prop("localRotation", new Vector3(0, -101, 0))
            .Ease(EaseType.EaseInOutSine)
            .OnComplete(() => { Destroy(simplec); })
        ));

        // Jump anim
        t += l - 0.4f;

        Dummy.InsertDummyInSequence(seq, () => animator.SetTrigger("do"), t);

        x = 0;
        seq.Insert(t + 0.2f, HOTween.To(this, 1f, new TweenParms()
            .Prop("x", 1)
            .Ease(animcurve)
            .OnUpdate(() =>
                {
                    rb.transform.position = curve.GetPointAt(x);
                }
            )
            .OnComplete(() =>
                {
                    Restore(rb);

                }
            ))
        );

        // turn back
        t += 1.5f;

        seq.Insert(t,HOTween.To(grizz, l, new TweenParms()
            .Prop("localRotation", new Vector3(0, -0, 0))
            .Ease(EaseType.EaseOutSine)
        ));

        seq.Play();
    }

    void Restore(Rigidbody rb)
    {
        if (needsRestore)
        {
            var sc = rb.transform.GetComponent<SimpleConstraints>();
            if (sc != null) Destroy(sc);
            rb.isKinematic = false;
            rb.useGravity = true;
            needsRestore = false;
            if (seq != null && !seq.isComplete)
            {
                seq.Kill();
            }
        }
    }
}
