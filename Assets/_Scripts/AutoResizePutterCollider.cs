﻿using UnityEngine;
using System.Collections;

public class AutoResizePutterCollider : MonoBehaviour {

    private Vector3 lastPosition;
    private Vector3 localPosDiff;
    public float multiplier;
    private BoxCollider col;

    void Start () {
        col = GetComponent<BoxCollider>();
	}
	
	void Update () {
    }

    void FixedUpdate()
    {
        // Calculate distance between last and curr position, relative to foot-space
        localPosDiff = transform.InverseTransformPoint(lastPosition);
        
        // Calculate foot club size
        var sizeX = localPosDiff.x;
        if (sizeX > -1 && sizeX < 1)
            sizeX = Mathf.Sign(sizeX) * 1;             // make sure it's never between -1 and 1
        sizeX = sizeX * multiplier;             // extend it slightly

        sizeX += Mathf.Sign(sizeX) * 1;         // add 1

        // Calculate center
        var centerX = (sizeX / 2f) - (Mathf.Sign(sizeX) * 0.5f);

        // only apply if Ball is on the right side, otherwise set to normal
        if (Mathf.Sign(transform.InverseTransformPoint(GameplayManager.instance.ball.transform.position).x) == Mathf.Sign(sizeX))
        {
            col.size = col.size.withX(1);
            col.center = col.center.withX(0);
        }
        else
        {
            // Abs size, cause it can't be negative
            sizeX = Mathf.Abs(sizeX);

            // Apply to collider
            col.size = col.size.withX(sizeX);
            col.center = col.center.withX(centerX);
        }

        // store last pos
        lastPosition = transform.position;
    }
}
