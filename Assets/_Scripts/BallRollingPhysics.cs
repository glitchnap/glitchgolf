using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class BallRollingPhysics : MonoBehaviour
{
    public bool useFakeSleepThreshold;
    public float fakeSleepThreshold;
    public float fakeVelocityReductionRate;
    public float fakeVelocityCutoff;

    public bool limitVelocity;
    public float maxVelocity;

    public bool useHoleMagnetism;
    public float magnetismAmount;
    public float magnetismRadius;
    public float magnetismApproachAngle;
    public float magnetismMaxTime;
 
    private Rigidbody rb;
    private bool firstHit;

    private float activeMagnetismTime;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (rb.isKinematic) return; // just don't do anything if we're kinematic...


        if (firstHit == false)
        {
            //NOTE: Don't do this stuff until the ball is on the ground.
            return;
        }

        if (rb.IsSleeping())
        {
            activeMagnetismTime = 0;
            return;
        }

        var velocitySquared = rb.velocity.sqrMagnitude;


        if (useFakeSleepThreshold && velocitySquared < (fakeSleepThreshold * fakeSleepThreshold))
        {
            var doFakeSleepThreshold = true;
            var grounded = false;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                var groundAngle = Vector3.Angle(-hit.normal, Vector3.down);

                if (hit.distance > 0.05f || groundAngle > 1f) //ground angles above 1 degree mean we're on a slope
                {
                    doFakeSleepThreshold = false;
                }

                if (hit.distance <= 0.05f)
                {
                    grounded = true;
                }
            }

            if (doFakeSleepThreshold)
            {
                rb.velocity = rb.velocity * fakeVelocityReductionRate;
            }

            if (grounded && rb.velocity.sqrMagnitude < (fakeVelocityCutoff * fakeVelocityCutoff))
            {
                rb.velocity = Vector3.zero;
                rb.Sleep();
            }
        }

        if (limitVelocity && velocitySquared > (maxVelocity * maxVelocity))
        {
            var brakeSpeed = rb.velocity.magnitude - maxVelocity;

            var breakVelocity = rb.velocity.normalized * brakeSpeed;

            rb.AddForce(-breakVelocity);
        }
        else
        {
            if (useHoleMagnetism && CourseManager.IsInstantiated() && CourseManager.instance.CurrentCourse != null)
            {
                var hole = CourseManager.instance.GetCurrentHole();
                if (hole != null)
                {
                    var direction = hole.Hole.position.withY(transform.position.y) - transform.position;
                    var magnitude = direction.magnitude;
                    var angle = Vector3.Angle(rb.velocity, hole.Hole.position.withY(transform.position.y) - transform.position);
                    if (activeMagnetismTime < magnetismMaxTime && magnitude < magnetismRadius && angle < magnetismApproachAngle)
                    {
                        var amount = magnetismRadius - magnitude;
                        rb.velocity = Vector3.Lerp(rb.velocity, direction.normalized * amount, magnetismAmount);    
                        activeMagnetismTime += Time.fixedDeltaTime;
                    }
                }
            }
        }
    }

    void Update()
    {
        if (Application.isEditor)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                rb.WakeUp();
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        firstHit = true;
    }

    //    void OnDrawGizmos()
    //    {
    //        if (Application.isPlaying)
    //        {
    //            if (CourseManager.IsInstantiated() && CourseManager.instance.CurrentCourse != null)
    //            {
    //                var hole = CourseManager.instance.GetCurrentHole();
    //                if (hole != null)
    //                {
    //                    Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    //                    Gizmos.DrawSphere(hole.Hole.position.withY(transform.position.y), magnetismRadius);
    //
    //
    //                    if (Vector3.Angle(rb.velocity, hole.Hole.position.withY(transform.position.y) - transform.position) < magnetismApproachAngle)
    //                    {
    //                        Gizmos.color = Color.blue;
    //                    }
    //                    else
    //                    {
    //                        Gizmos.color = Color.red;
    //                    }
    //                    Gizmos.DrawLine(transform.position, hole.Hole.position.withY(transform.position.y));
    //                }
    //            }
    //
    //            if (!rb.IsSleeping())
    //            {
    //                Gizmos.color = Color.white;
    //                Gizmos.DrawLine(transform.position, transform.position + rb.velocity);
    //
    //                Gizmos.color = Color.yellow;
    //                Gizmos.DrawLine(transform.position, transform.position + (Quaternion.AngleAxis(magnetismApproachAngle, Vector3.up) * (rb.velocity.normalized * magnetismRadius)));
    //                Gizmos.DrawLine(transform.position, transform.position + (Quaternion.AngleAxis(-magnetismApproachAngle, Vector3.up) * (rb.velocity.normalized * magnetismRadius)));
    //            }
    //        }
    //    }
}
