using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    public bool isTouchingClub;
    private Rigidbody rb;
    private bool wasSleeping;
    private bool wasJustReset;
    [SerializeField]
    private int colliderCounter;
    public int lastColliderCounter;

    public float radius;
    public bool isTouchingCurrentHole;
    public bool isTouchingFloor;

    [ReadOnly]
    [SerializeField]
    private bool overrideFloorTouching;
    [ReadOnly]
    public bool isOutOfBounds;
    public bool createOcclusionModel;
    public Transform model;

    void Start()
    {
        Init();
        CreateOcclusionModel();
    }

    void CreateOcclusionModel()
    {
        if (createOcclusionModel)
        {
            var occ = Instantiate(model.gameObject, model.parent) as GameObject;
            occ.transform.CopyLocalFrom(model);
            foreach (var ren in occ.GetComponentsInChildren<Renderer>())
            {
                ren.material = PutterAndBallManager.instance.ballOcclusionMaterial;
            }
        }
    }

    void Init()
    {
        if (rb == null) rb = GetComponent<Rigidbody>();
    }

    public void Reset()
    {
        Init();
        wasJustReset = true;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.WakeUp();
        isTouchingFloor = false;
    }

    void FixedUpdate()
    {
        if (CourseManager.instance.CurrentCourse == null)
        {
            transform.position = transform.position.withY(200f);
            colliderCounter = 0;
            rb.Sleep();
        }

        wasSleeping = rb.IsSleeping();

        if (!rb.IsSleeping())
        {
            // Only record the collider hits when we're awake.
            // !!! When asleep, they don't update
            lastColliderCounter = colliderCounter;
        }

        isOutOfBounds = isTouchingFloor && !(overrideFloorTouching && isTouchingCurrentHole);
        colliderCounter = 0;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Goal"))
        {
            GameplayManager.instance.BallHitGoal(col);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        // Collision doens't call 'stay' on enter, so if ball falls asleep in same frame it collides, it needs to know as well
        // We're resetting this value in a 'late fixed update' kind of way, so we can just count up all collisions
        colliderCounter++;

        if (col.collider.tag == "Club")
        {
            GameplayManager.instance.BallHitPutter();
            wasJustReset = false;
        }
        else if (col.collider.tag.Equals("Floor"))
        {
            GameplayManager.instance.BallHitBottomFloor();
            isTouchingFloor = true;
        }
        else if (col.collider.tag.Equals("Goal"))
        {
            // no sound cause there's a sound triggered by BallHitGoal
        }
        else
        {
            if (CourseManager.instance.CurrentCourse != null)
            {
                var hole = CourseManager.instance.GetCurrentHole();
                overrideFloorTouching = hole.allowFloorTouching;

                if (col.transform.IsChildOf(hole.transform))
                {
                    isTouchingCurrentHole = true;
                }
            }

            if (col.contacts.Length > 0)
            {
                var relativeContactPoint = (col.contacts[0].point - transform.position);

                if (Mathf.Abs(relativeContactPoint.y) < 0.03f)
                {
                    GameplayManager.instance.BallHitWall();
                }
            }
        }
    }

    void OnCollisionStay(Collision col)
    {
        // We're resetting this value in a 'late fixed update' kind of way, so we can just count up all collisions
        colliderCounter++;

//        if (col.collider.tag.Equals("Floor"))
//        {
//            isTouchingFloor = true;
//        }
//        else if (CourseManager.instance.CurrentCourse != null)
//        {
//            var hole = CourseManager.instance.GetCurrentHole();
//            if (col.transform.IsChildOf(hole.transform))
//            {
//                isTouchingCurrentHole = true;
//            }
//        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.collider.tag.Equals("Floor"))
        {
            isTouchingFloor = false;
        }

        if (CourseManager.instance.CurrentCourse != null)
        {
            var hole = CourseManager.instance.GetCurrentHole();
            overrideFloorTouching = hole.allowFloorTouching;
            if (col.transform.IsChildOf(hole.transform))
            {
                isTouchingCurrentHole = false;
            }
        }
    }

    public bool IsSleeping()
    {
        return rb.IsSleeping();
    }

    public bool WasSleeping()
    {
        return wasSleeping;
    }

    public bool WasJustReset()
    {
        return wasJustReset;
    }

    public bool IsOnlyTouchingBottomFloor()
    {
        return isOutOfBounds;  
    }

    public bool IsFalling()
    {
        return !(lastColliderCounter > 0 || rb.IsSleeping());
    }

    public void Bump(Vector3 force)
    {
        rb.velocity = force;
    }
}