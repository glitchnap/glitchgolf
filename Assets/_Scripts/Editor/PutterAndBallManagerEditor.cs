﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PutterAndBallManager))]
public class PutterAndBallManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var instance = (target as PutterAndBallManager);


        if (Application.isPlaying && GUILayout.Button("Refresh Current Putter and Ball"))
        {
            var settings = instance.ballPhysicsSettings[instance.BallID];
            Debug.Log("Refreshing putter and ball, ball settings:\n" +
                "type: " + settings.type.ToString() + "\n" +
                "mass: " + settings.mass + "\n" +
                "drag: " + settings.drag + "\n" +
                "angularDrag: " + settings.angularDrag + "\n" +
                "staticFriction: " + settings.staticFriction + "\n" +
                "dynamicFriction: " + settings.dynamicFriction + "\n" +
                "bounciness: " + settings.bounciness + "\n" +
                "frictionCombine: " + settings.frictionCombine + "\n" +
                "bounceCombine: " + settings.bounceCombine + "\n" +
                "customGracity: " + settings.customGravity);

            GameplayManager.instance.SetBall(instance.BallID);
            GameplayManager.instance.SetPutter(instance.BallID);
            GameplayManager.instance.SetBallToStart();
        }
    }
   
}
