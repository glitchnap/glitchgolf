﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(GolfLevelPack))]
public class GolfLevelPackDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        var elementHeight = position.height / 6f;

        // Calculate rects
        var previewRect = new Rect(position.x, position.y, position.width, elementHeight);
        var sceneRect = new Rect(position.x, position.y + elementHeight, position.width, elementHeight);
        var niceName = new Rect(position.x, position.y + elementHeight * 2, position.width, elementHeight);
        var nameRect = new Rect(position.x, position.y + elementHeight * 3, position.width, elementHeight);
        var sortOrderRect = new Rect(position.x, position.y + elementHeight * 4, position.width, elementHeight);
        //var noteRect = new Rect(position.x, position.y + elementHeight * 3, position.width, elementHeight);
        var noteRect = new Rect(position.x, position.y + elementHeight * 5, position.width, elementHeight);

        EditorGUI.PropertyField(previewRect, property.FindPropertyRelative("smallCoursePreview"), new GUIContent("Small Preview"));
        EditorGUI.PropertyField(sceneRect, property.FindPropertyRelative("scene"), new GUIContent("Scene"));
        EditorGUI.PropertyField(niceName, property.FindPropertyRelative("niceName"), new GUIContent("Nice Name"));
        EditorGUI.PropertyField(sortOrderRect, property.FindPropertyRelative("sortOrder"), new GUIContent("Sort Order"));

        var scene = property.FindPropertyRelative("scene").objectReferenceValue as SceneAsset;
        if (scene != null)
        {
            property.FindPropertyRelative("sceneName").stringValue = scene.name;
        }

        EditorGUI.LabelField(nameRect, "Scene Name", property.FindPropertyRelative("sceneName").stringValue);

        var col = GUI.color;
        GUI.color = Color.red;
        EditorGUI.LabelField(noteRect, "NOTE: Scene Name updated OnGUI update!");
        GUI.color = col;

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var baseHeight = base.GetPropertyHeight(property, label);
        return baseHeight * 6f;
    }
   
}