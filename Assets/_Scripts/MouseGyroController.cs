using UnityEngine;
using System.Collections;

public class MouseGyroController : MonoBehaviour
{

    public bool disableMouseInput;
	int direction = -1;

    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(-Input.GetAxis("Mouse Y") * 5,0,-Input.GetAxis("Mouse X") * 5);
//#if UNITY_IOS
//        transform.RotateAround(transform.position, transform.right, TouchListener.VelocityY * 5);
//        transform.RotateAround(transform.position, transform.forward, TouchListener.VelocityX * 5);
//#else
        transform.RotateAround(transform.position, transform.right, Input.GetAxis("Mouse ScrollWheel") * 50);
        if (disableMouseInput == false)
        {
            transform.RotateAround(transform.position, transform.forward, Input.GetAxis("Mouse Y") * 5);
        }
        transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Horizontal") * 5);
//#endif

		if (Input.GetMouseButtonDown(1))
		{
			direction = direction * -1;
		}
        if (Input.GetMouseButton(1))
        {
            transform.RotateAround(transform.position, Vector3.up, 1.5f * direction);

        }
    }
}
