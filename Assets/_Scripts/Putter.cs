using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Putter : MonoBehaviour
{
    public Rigidbody putterJointed;
    public Rigidbody putterKinematic;
    public Collider footCollider;
    public Vector3 golfclubInitialOffset;

    public Vector3 lastFootColliderPosition;
    public Vector3 lastLastFootColliderPosition;

    void Awake()
    {
        
    }

    void Start()
    {
        golfclubInitialOffset = GameplayManager.instance.ball.transform.position - putterKinematic.transform.position;
        RefreshFootCollider();
    }

    void FixedUpdate()
    {
        lastLastFootColliderPosition = lastFootColliderPosition;
        lastFootColliderPosition = footCollider.transform.position;
    }

    public void RefreshFootCollider()
    {

        foreach (var col in putterJointed.GetComponentsInChildren<Collider>())
        {
            if (col.tag == "Club")
            {
                footCollider = col;
                break;
            }
        }
    }

    public void VisuallyEnable()
    {
        foreach (var item in putterJointed.GetComponentsInChildren<Renderer>())
        {
            item.material.color = item.material.color.SetA(1f);
        }

    }

    public void VisuallyDisable()
    {
        foreach (var item in putterJointed.GetComponentsInChildren<Renderer>())
        {
            item.material.color = item.material.color.SetA(0.33f);
        }     
    }

    public void DoMoveRotation(Quaternion rotation, float lerpSpeed)
    {
        putterKinematic.MoveRotation(Quaternion.Lerp(putterKinematic.rotation, rotation, lerpSpeed));
    }

    public void DoMovePosition(Vector3 position, bool instant)
    {
        if (instant)
        {
            putterKinematic.transform.position = position - golfclubInitialOffset;
            GameplayManager.instance.hasFinishedMovingPutter = true;
        }
        else
        {
            putterKinematic.transform.DOMove(position - golfclubInitialOffset, 1.5f).SetEase(Ease.OutCubic).OnComplete(() =>
                {
                    GameplayManager.instance.hasFinishedMovingPutter = true;
                });
        }
    }

    public float GetFootVelocity()
    {
        return (lastLastFootColliderPosition - lastFootColliderPosition).magnitude;
    }
}
