﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class AnimateText : MonoBehaviour
{

    private TMPro.TextMeshPro text;
    public AnimationCurve curve;
    public float amplitude;
    public bool updateEveryFrame;
    private Vector3[] vertsOriginal;
    public float speed;
    public float letterSpeed;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<TMPro.TextMeshPro>();
        text.ForceMeshUpdate();
        vertsOriginal = text.mesh.vertices;
        Place();
    }

    void Update()
    {
        if(updateEveryFrame)
        {
            Place();
        }
    }


    public void Place()
    {
        var verts = text.mesh.vertices;

        var height = 0f;
        for (int i = 0; i < vertsOriginal.Length; i += 4)
        {
            height = curve.Evaluate((i * letterSpeed) / (float)vertsOriginal.Length + Time.time * speed) * amplitude ;
            verts[i] = vertsOriginal[i].addY(height);
            verts[i+1] = vertsOriginal[i+1].addY(height);
            verts[i+2] = vertsOriginal[i+2].addY(height);
            verts[i+3] = vertsOriginal[i+3].addY(height);
        }

        text.mesh.vertices = verts;
        text.SetVerticesDirty();
        text.ForceMeshUpdate();
    }
}
    