﻿using UnityEngine;
using System.Collections;

public class CandyPeople : MonoBehaviour {
    public Animator controller;
    public float force;
    public float yForce;
    void Start () {
	}

    void Update()
    {
        transform.LookAt(GameplayManager.instance.ball.transform.position.withY(transform.position.y));
    }

    void OnCollisionEnter(Collision col)
    {
        controller.SetTrigger("Bump");
        var ball = col.collider.GetComponent<Ball>();
        if (ball != null)
        {
            ball.Bump((transform.position - ball.transform.position).withY(yForce).normalized * force);
        }
        
    }
}
