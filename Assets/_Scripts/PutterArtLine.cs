﻿using UnityEngine;
using System.Collections;

public class PutterArtLine : MonoBehaviour {

    public LineRenderer line;
    public Transform pivot;
    public Transform putter;
    public Transform ball;
    public Rigidbody ballRigidBody;
    public bool debugUpdate;
    public bool flip;
    public float extraTilt = 1;

    private Vector3 ballOriginalOffsetPos;

	void Awake() {
        line = GetComponent<LineRenderer>();
        UpdateLine();
        ballOriginalOffsetPos = ball.position - pivot.position; 

    }

    void UpdateLine()
    {
        var count = 10;
        line.SetVertexCount(count);
        var positions = new Vector3[count];

        for (int i = 0; i < count; i++)
        {
            var localPos = ballOriginalOffsetPos;
            var rotation = Quaternion.AngleAxis(i*(flip?-1:1)*10, (Quaternion.AngleAxis(putter.eulerAngles.y,Vector3.up + Vector3.forward* extraTilt) * Vector3.forward));
            positions[i] = pivot.position + rotation *  localPos;
        }

        line.SetPositions(positions);
    }
	

    void Update()
    {
        if (ballRigidBody.IsSleeping())
        {
            UpdateLine();
            line.enabled = true;
        }
        else
        {
            line.enabled = false;
        }

    }

    void OnDrawGizmos()
    {
        if (debugUpdate)
        {
            UpdateLine();
        }
    }
}

