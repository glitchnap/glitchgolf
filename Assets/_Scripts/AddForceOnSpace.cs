using UnityEngine;
using System.Collections;

public class AddForceOnSpace : MonoBehaviour {

    public Vector3 force;
    public bool onStart;
    public float arrowKeyForce;

	// Use this for initialization
	void Start () {
	    if(onStart)
        {
            GetComponent<Rigidbody>().AddForce(force);
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Rigidbody>().velocity = force;
            //GetComponent<Rigidbody>().AddForce(force);
            //GetComponent<Rigidbody>().WakeUp();
        }

        //GetComponent<Rigidbody>().AddForce(arrowKeyForce * Input.GetAxis("Horizontal"), 0, arrowKeyForce * Input.GetAxis("Vertical"));
	}
}
