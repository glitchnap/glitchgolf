﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GlitchConnect.Commands;
using System;

public class TutorialMenu : GlitchUIElement
{
    public TutorialSlideshow SDTutorialSlideshow;
    public TutorialSlideshow MDPutterTutorialSlideshow;
    public GlitchUIElement MDWorldTutorialOverlay;

    public static bool calibrateOnHide;

    void Start()
    {
        MenuCommand.CommandReceived += OnCommandReceived;
        MDPutterTutorialSlideshow.OnHideEvent += OnHideMDPutterTutorial;
        SDTutorialSlideshow.OnHideEvent += OnHideSDTutorial;

        SDTutorialSlideshow.Hide(false);
        MDPutterTutorialSlideshow.Hide(false);
        MDWorldTutorialOverlay.Hide(false);
    }

    private void OnHideSDTutorial()
    {
        this.Hide();
    }

    private void OnHideMDPutterTutorial()
    {
        ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.MDTutorialHide));
        this.Hide();
    }

    private void OnCommandReceived(MenuCommand.Actions command)
    {
        if (command == MenuCommand.Actions.MDTutorialHide)
        {
            this.Hide();
        }
        if (command == MenuCommand.Actions.MDTutorialShow)
        {
            this.Show();
        }
    }

    public override void OnShow()
    {
        switch (RoleManager.instance.role)
        {
            case GlitchGolfRoles.none:
                break;
            case GlitchGolfRoles.putter:
                MDPutterTutorialSlideshow.Show();
                ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.MDTutorialShow));
                break;
            case GlitchGolfRoles.world:
                MDWorldTutorialOverlay.Show();
                break;
            case GlitchGolfRoles.singleDevice:
                SDTutorialSlideshow.Show();
                GameplayManager.instance.isPlaying = false;
                break;
            default:
                break;
        }
    }

    public override void OnHide()
    {
        if (calibrateOnHide)
        {
            GlobalUIManager.instance.inGameMenu.CalibrateShow();
            calibrateOnHide = false;
        }
        GameplayManager.instance.isPlaying = true;
    }
}
