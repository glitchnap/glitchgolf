﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ScoreBoardStrokeRow : MonoBehaviour {
    public Transform numberGroup;
    public CanvasGroup AddPlayerButton;
    public CanvasGroup RemovePlayerButton;
    public ScoreBoardUI parent;

    public CanvasGroup inputField;
    public InputField inputFieldTextInput;
    public UnityEngine.UI.Text inputFieldTextGhost;
    public TMPro.TextMeshProUGUI inputFieldText;

    private bool isEditing;

    void Start()
    {
        FadeAssistant.FadeOut(inputField, 0).Play();
        inputFieldText.raycastTarget = true;
    }

    public void HideAddPlayerbutton()
    {
        FadeAssistant.FadeOut(AddPlayerButton, 0).Play();
    }

    public void HideRemovePlayerbutton()
    {
        FadeAssistant.FadeOut(RemovePlayerButton, 0).Play();
    }

    public void ShowRemovePlayerButton()
    {
        FadeAssistant.FadeOut(AddPlayerButton, 0).Play();
        FadeAssistant.FadeIn(RemovePlayerButton, 1).Play();
        
    }

    public void RemovePlayerPressed()
    {
        parent.RemovePlayerPressed(this);
    }


    void Update()
    {
        if (isEditing)
        {
            inputFieldText.text = inputFieldTextGhost.text;
        }
    }

    public void SetName(string name)
    {
        var t = name;
        inputFieldTextGhost.text = t;
        inputFieldTextInput.text = t;
        inputFieldText.text = t;
    }

    public void EditName()
    {
        DebugExtension.Blip();
        isEditing = true;
        FadeAssistant.FadeIn(inputField, 0).Play();
        inputFieldTextInput.ActivateInputField();
        inputFieldText.raycastTarget = false;
    }

    public void EditNameDone()
    {
        isEditing = false;
        FadeAssistant.FadeOut(inputField, 0).Play();
        inputFieldText.raycastTarget = true;

        ScoreManager.instance.SetPlayerName(parent.GetStrokeRowID(this), inputFieldText.text);
    }
}
