using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;
using GlitchConnect.Commands;
using System;

public class MDPutterPlayMenu : GlitchUIElement
{
    public Slider ZoomSlider;
    public TextMeshProUGUI CourseAndholeLabel;
    

    void Start()
    {
        GlitchConnect.Commands.CourseInfoCommand.CommandReceived += OnCommandReceived;
    }

    private void OnCommandReceived(int courseID, int holeID)
    {
        CourseAndholeLabel.text = CourseManager.instance.Courses[courseID].niceName + "\n<size=70%>HOLE " + (holeID +1) + "</size>";
    }

    public void Show(bool animate = true, GlitchUITransitionTypes transition = GlitchUITransitionTypes.Fade)
    {
        ZoomSlider.maxValue = NavigationManager.instance.maxCameraHeight;
        ZoomSlider.minValue = NavigationManager.instance.minCameraHeight;
        ZoomSlider.value = NavigationManager.instance.GetCameraHeight();
        (this as GlitchUIElement).Show();
    }


    public void MainMenuButtonPressed()
    {
        GlobalUIManager.instance.GotoMainMenu();
        ConnectionManager.instance.SendCommand(new GlitchConnect.Commands.MenuCommand(GlitchConnect.Commands.MenuCommand.Actions.GotoMainMenu));
    }

    public void ZoomSliderChanged()
    {
        ConnectionManager.instance.SendCommand(new NavigationCommand(NavigationCommands.Zoom, ZoomSlider.value));
    }

    public void PrevHolePressed()
    {
        CourseManager.instance.GoToPreviousLevel();
    }

    public void NextHolePressed()
    {
        CourseManager.instance.GoToNextLevel();
    }

    public void CalibratePressed()
    {
        GlobalUIManager.instance.inGameMenu.CalibrateShow();
    }

    public void ActivateClubUpdate()
    {
        //DebugExtension.Blip();
        MDInputManager.instance.isPressing = true;
    }
}
