using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;

public class SDPlayUI : GlitchUIElement
{
    public GlitchUIButton SwingButton;
    public GlitchUIButton ResetButton;
    public Slider ZoomSlider;

    [InspectorNote("Power bar")]
    public bool powerbarstuff;
    
    public int lowPassAmount = 5;
    public float velocityMultiplier = 0.1f;
    public Image powerbarFill;
    public TextMeshProUGUI powerbarText;
    public GlitchUIElement powerbar;
    private FixedSizedQueue<float> velocityHistory;
    private bool updateBar;

    void Start()
    {
        SDInputManager.instance.fsm.Changed += OnInputManagerModeChanged;
        
        ZoomSlider.maxValue = NavigationManager.instance.maxAngledCameraHeight;
        ZoomSlider.minValue = NavigationManager.instance.minAngledCameraHeight;
        ZoomSlider.value = NavigationManager.instance.GetCameraHeight();
        //ZoomSlider.gameObject.SetActive(!GolfCameraFollow.instance.cameraZoomEnabled);

        velocityHistory = new FixedSizedQueue<float>(lowPassAmount);
    }

    void OnInputManagerModeChanged(SDInputManager.States state)
    {
        SwingButton.Hide();
        ResetButton.Hide();
        powerbar.Hide();

        switch (state)
        {
            case SDInputManager.States.Disabled:
                break;
            case SDInputManager.States.Navigate:
                SwingButton.Show();
                if (GameplayManager.instance.ballLastSet)
                    ResetButton.Show();
                break;
            case SDInputManager.States.Swing:
                updateBar = true;
                powerbar.Show();
                SwingButton.Show();
                break;
            case SDInputManager.States.Ballmoving:
                updateBar = false;
                powerbar.Show();
                break;
            default:
                break;
        }
    }

    void FixedUpdate()
    {
        velocityHistory.Enqueue(GameplayManager.instance.putter.GetFootVelocity());

        var total = 0f;
        var array = velocityHistory.ToArray();
        for (int i = 0; i < velocityHistory.Count; i++)
        {
            total += array[i];
        }

        if (updateBar)
        {
            powerbarFill.fillAmount = velocityMultiplier * total / (float)velocityHistory.Count;
            powerbarText.text = "" + (int)(powerbarFill.fillAmount * 100);
        }
    }

    public void SwingPressed()
    {
        SDInputManager.instance.Swing();
    }

    public void ResetPressed()
    {
        GameplayManager.instance.SetBallToStart();
    }


    public void CancelPressed()
    {
        SDInputManager.instance.CancelSwing();
    }

    public void ZoomSliderChanged()
    {
        NavigationManager.instance.Zoom(ZoomSlider.value);
    }
}
