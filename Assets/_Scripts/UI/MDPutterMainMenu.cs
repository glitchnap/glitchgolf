using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;

public class MDPutterMainMenu : GlitchUIElement
{
    public Image coursePreviewImage;
    public TMPro.TextMeshProUGUI courseNameText;
    
    private int currentCourseID;

    void Start()
    {
        currentCourseID = 0;
        RefreshCourseImage();

    }

    public void PutterAndBallPressed()
    {

    }

    public override void OnShow()
    {
        currentCourseID = 0;
        RefreshCourseImage();
    }

    public void PlayPressed()
    {
        CourseManager.instance.ChangeCourse(currentCourseID);
        GlobalUIManager.instance.GotoInGame();
        ConnectionManager.instance.SendCommand(new GlitchConnect.Commands.MenuCommand(GlitchConnect.Commands.MenuCommand.Actions.GotoInGame));

        TutorialMenu.calibrateOnHide = true;
        GlobalUIManager.instance.ShowTutorial();
    }

    public void DisconnectPressed()
    {
        ConnectionManager.instance.Disconnect();
    }

    public void NextCoursePressed()
    {
        currentCourseID++;
        RefreshCourseImage();
        ConnectionManager.instance.SendCommand(new GlitchConnect.Commands.CoursePreviewCommand(currentCourseID));
    }

    public void PrevCoursePressed()
    {
        currentCourseID--;
        RefreshCourseImage();
        ConnectionManager.instance.SendCommand(new GlitchConnect.Commands.CoursePreviewCommand(currentCourseID));
    }

    void RefreshCourseImage()
    {
        if (currentCourseID >= CourseManager.instance.Courses.Length) currentCourseID = 0;
        if (currentCourseID < 0) currentCourseID = CourseManager.instance.Courses.Length-1;

        courseNameText.text = CourseManager.instance.Courses[currentCourseID].niceName;
        coursePreviewImage.sprite = CourseManager.instance.Courses[currentCourseID].smallCoursePreview;
    }
}
