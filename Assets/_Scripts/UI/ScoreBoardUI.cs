﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ScoreBoardUI : GlitchUIElement {

    public Transform HoleGroup;
    public List<ScoreBoardStrokeRow> StrokesGroups;
    public Transform ParGroup;
    public Transform Border;
    public bool activeInMD;
    public bool activeInSD;
    public bool editable = true;

    void Start()
    {
        StrokesGroups[0].HideRemovePlayerbutton();

        if(!editable)
        {
            HideAllButtons();
        }

        ScoreManager.instance.OnPlayerAddedEvent += OnAddPlayer;
        ScoreManager.instance.OnPlayerRemovedEvent += OnRemovePlayer;
        ScoreManager.instance.OnSetFirstPlayerEvent += OnSetFirstPlayer;
        ScoreManager.instance.OnScoreChanged += UpdateScores;
        ScoreManager.instance.OnPlayerRenamedEvent += OnPlayerRenamed;
    }

    

    public void UpdateScores()
    {
        if ((!activeInMD && RoleManager.instance.IsMultiDevice()) ||
            !activeInSD && RoleManager.instance.IsSingleDevice())
            return;

        ResetScores();

        if (!ScoreManager.IsInstantiated() || CourseManager.instance.CurrentCourse== null) return;

        
        // loop over holes
        for (int currHole = 0; currHole < CourseManager.instance.CurrentCourse.Holes.Length; currHole++)
        {
            HoleGroup.GetChild(currHole).GetComponent<TMPro.TextMeshProUGUI>().text = (currHole +1) + "" ;

            // loop over players
            var players = ScoreManager.instance.GetPlayers();
            for (int currPlayer = 0; currPlayer < players.Count; currPlayer++)
            {
                var strokes = players[currPlayer].strokes;
                var sg = StrokesGroups[currPlayer].numberGroup;
                var sgchild = sg.GetChild(currHole);
                var sgchildtxt = sgchild.GetComponent<TMPro.TextMeshProUGUI>();
                sgchildtxt.text = strokes[currHole] + "";


                var stars = sgchild.GetComponentInChildren<ScoreBoardBackgroundStars>();
                if(stars!=null) stars.ShowStars(CourseManager.instance.GetStarCountForHole(currHole));
            }
            
            ParGroup.GetChild(currHole).GetComponent<TMPro.TextMeshProUGUI>().text = CourseManager.instance.CurrentCourse.Holes[currHole].par + "";

            if(currHole == CourseManager.instance.CurrentHoleIndex)
                Border.position = HoleGroup.GetChild(currHole).position;
        }
    }

    public void ResetScores()
    {
        foreach (TMPro.TextMeshProUGUI item in HoleGroup.GetComponentsInChildren<TMPro.TextMeshProUGUI>())
        {
            item.text = "-";
        }
        for (int i = 0; i < StrokesGroups.Count; i++)
        {
            foreach (TMPro.TextMeshProUGUI item in StrokesGroups[i].numberGroup.GetComponentsInChildren<TMPro.TextMeshProUGUI>())
            {
                item.text = "-";
            }
        }
        foreach (TMPro.TextMeshProUGUI item in ParGroup.GetComponentsInChildren<TMPro.TextMeshProUGUI>())
        {
            item.text = "-";
        }
    }

    public void OnSetFirstPlayer(string name)
    {
        StrokesGroups[0].SetName(name);
    }

    public void AddPlayerPressed()
    {
        var index = StrokesGroups[0].transform.GetSiblingIndex() + StrokesGroups.Count;

        DebugExtension.Blip();
        ScoreManager.instance.AddPlayer("PLAYER " + index, index,true, false);
        
    }

    public void OnAddPlayer(string name, int index)
    {
        DebugExtension.Blip();
        var go = Instantiate(StrokesGroups[0].gameObject, StrokesGroups[0].transform.parent, true) as GameObject;
        go.transform.SetSiblingIndex(index);
        var row = go.GetComponent<ScoreBoardStrokeRow>();
        row.SetName(name);
        StrokesGroups.Add(row);
        
        if (!editable)
        {
            HideAllButtons();
        }
        else
        {
            row.ShowRemovePlayerButton();
        }
    }

    private void OnPlayerRenamed(string name, int index)
    {
        StrokesGroups[index].SetName(name);
    }

    public void RemovePlayerPressed(ScoreBoardStrokeRow row)
    {
        ScoreManager.instance.RemovePlayer(StrokesGroups.IndexOf(row));
        StrokesGroups.Remove(row);
        Destroy(row.gameObject);
    }

    public void OnRemovePlayer(int rowID)
    {
        var row = StrokesGroups[rowID];
        StrokesGroups.Remove(row);
        Destroy(row.gameObject);
    }

    public void UpdatePlayerName(ScoreBoardStrokeRow row, string name)
    {
        if (!editable) return;
        ScoreManager.instance.SetPlayerName(StrokesGroups.IndexOf(row), name);
    }


    public void DisableAllEdits()
    {
        foreach (var item in StrokesGroups)
        {
            item.EditNameDone();
        }
    }

    public void HideAllButtons()
    {
        foreach (var item in StrokesGroups)
        {
            item.HideAddPlayerbutton();
            item.HideRemovePlayerbutton();
        }
    }

    public int GetStrokeRowID(ScoreBoardStrokeRow row)
    {
        return StrokesGroups.IndexOf(row);
    }
}
