﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ScorePlayer  {
    [SerializeField]
    private int[] _strokes;

    public int[] strokes
    {
        get
        {
            if (_strokes == null)
            {
                InitStrokes();
            }
            return _strokes;
        }
    }

    public string playerName;

    public void InitStrokes()
    {
        _strokes = new int[0];
        if (CourseManager.IsInstantiated() && CourseManager.instance.CurrentCourse != null)
        {
            _strokes = new int[CourseManager.instance.CurrentCourse.Holes.Length];
        }
    }
}
