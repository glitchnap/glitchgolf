﻿using UnityEngine;
using System.Collections;

[MonoSingleton(false)]
public class GlobalUIManager : MonoSingleton<GlobalUIManager>
{
    public InGameMenu inGameMenu;
    public MainMenu mainMenu;
    public ConnectionMenu connectionMenu;
    public TutorialMenu tutorialMenu;

    public bool EnableTutorial;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        GlitchConnect.Commands.MenuCommand.CommandReceived += OnMenuCommandReceived;

        connectionMenu.Show();
        inGameMenu.Hide();
        mainMenu.Hide();
        tutorialMenu.Hide();
    }

    new void OnDestroy()
    {
        base.OnDestroy();
        GlitchConnect.Commands.MenuCommand.CommandReceived -= OnMenuCommandReceived;
    }

    void OnMenuCommandReceived(GlitchConnect.Commands.MenuCommand.Actions action)
    {
        if (action == GlitchConnect.Commands.MenuCommand.Actions.GotoInGame)
            GlobalUIManager.instance.GotoInGame();
        if (action == GlitchConnect.Commands.MenuCommand.Actions.GotoMainMenu)
            GlobalUIManager.instance.GotoMainMenu();
    }

    public void ShowConnectionMenu()
    {
        connectionMenu.Show(true, GlitchUITransitionTypes.SlideLeft);
    }

    public void GotoMainMenu()
    {
        connectionMenu.Hide();
        inGameMenu.Hide();
        mainMenu.Show();

        GameplayManager.instance.StopGame();

    }

    public void GotoInGame()
    {
        StartGame();
        connectionMenu.Hide();
        mainMenu.Hide();
        inGameMenu.Show();
    }

    void StartGame()
    {
        GameplayManager.instance.StartGame();
    }

    public void ShowTutorial()
    {
        DebugExtension.Blip();
        tutorialMenu.Show();
    }
}
