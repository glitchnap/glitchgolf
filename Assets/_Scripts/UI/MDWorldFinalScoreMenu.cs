﻿using UnityEngine;
using System.Collections;
using GlitchConnect.Commands;

public class MDWorldFinalScoreMenu : GlitchUIElement
{
    public ScoreBoardUI scoreBoardUI;


    void Start()
    {
        MenuCommand.CommandReceived += OnMenuCommandReceived;
        CourseManager.OnCourseChanged += OnCourseChanged;
    }

    void OnDestroy()
    {
        MenuCommand.CommandReceived -= OnMenuCommandReceived;
        CourseManager.OnCourseChanged -= OnCourseChanged;
    }

    public override void OnShow()
    {
        GolfCameraFollow.instance.EnableFlyOver();
        ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.FinalScoreShow));
        
    }

    public override void OnHide()
    {
        ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.FinalScoreHide));
    }


    public void PlayAgainPressed()
    {
        if (isShowing)
        {
            LoadCourse(CourseManager.instance.CurrentCourseIndex);
        }
    }

    public void NextCoursePressed()
    {
        if (isShowing)
        {
            var nextcourseID = CourseManager.instance.CurrentCourseIndex + 1;
            if (nextcourseID >= CourseManager.instance.Courses.Length) nextcourseID = 0;

            LoadCourse(nextcourseID);
        }
    }

    void LoadCourse(int id)
    {
        CourseManager.instance.ChangeCourse(id);
    }

    void OnCourseChanged()
    {
        GlobalUIManager.instance.GotoInGame();
    }

    void OnMenuCommandReceived(MenuCommand.Actions action)
    {
        switch (action)
        {
            case MenuCommand.Actions.FinalScorePlayAgain:
                PlayAgainPressed();
                break;
            case MenuCommand.Actions.FinalScorePlayNext:
                NextCoursePressed();
                break;
            default:
                break;
        }
    }
}
