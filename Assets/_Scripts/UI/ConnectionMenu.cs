using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Holoville.HOTween;

public class ConnectionMenu : GlitchUIElement
{

    public GlitchUIElement SplashGroup;
    public GlitchUIElement ConnectingGroup;
    public GlitchUIElement ConnectedGroup;

    public GlitchUIElement RoleWorldGroup;
    public GlitchUIElement RolePutterGroup;
    public GlitchUIButton RoleSwapButton;

    public ConnectingDotsLoop dots;
    
    public TMPro.TextMeshProUGUI connectingText;


    void Start()
    {
        ConnectionManager.instance.fsm.Changed += fsm_Changed;
        RoleManager.RoleChanged += OnRoleChanged;
    }

    void OnDestroy()
    {
        if (ConnectionManager.IsInstantiated())
            ConnectionManager.instance.fsm.Changed -= fsm_Changed;
        
        RoleManager.RoleChanged -= OnRoleChanged;
    }

    void fsm_Changed(ConnectionManager.States state)
    {
        switch (state)
        {
            case ConnectionManager.States.Idle:
                GlobalUIManager.instance.ShowConnectionMenu();
                SplashGroup.Show(true, GlitchUITransitionTypes.SlideLeft);
                ConnectingGroup.Hide(false);
                ConnectedGroup.Hide(false);
                //CoursePreviewImage.GetComponent<GlitchUIElement>().Hide(false);
                break;
            case ConnectionManager.States.WaitForConnection:
                ShowConnectingGroup();
                break;
            case ConnectionManager.States.WaitForRole:
                ShowWaitForRoleGroup();
                break;
            case ConnectionManager.States.WaitForStartPlaying:
                ShowConnectedGroup();
                // TODO: make this better
                if (RoleManager.instance.IsSingleDevice())
                    ConnectedPlayPressed();
                break;
            case ConnectionManager.States.Playing:
                GlobalUIManager.instance.GotoMainMenu();
                break;
            default:
                break;
        }
    }

    public void ShowSplashGroup(bool animate = true)
    {
        SplashGroup.Show(animate, GlitchUITransitionTypes.Fade);
    }

    public void ShowConnectingGroup(bool animate = true)
    {
        if (ConnectionManager.instance.fsm.LastState == ConnectionManager.States.Idle)
        {
            SplashGroup.Hide(animate, GlitchUITransitionTypes.SlideLeft);
            ConnectingGroup.Show(animate, GlitchUITransitionTypes.SlideRight);
        }
        else
        {
            ConnectedGroup.Hide(animate, GlitchUITransitionTypes.SlideRight);
            ConnectingGroup.Show(animate, GlitchUITransitionTypes.SlideLeft);
        }

        connectingText.text = "Connecting";
        dots.Toggle(true);
    }

    public void ShowWaitForRoleGroup(bool animate = true)
    {
        connectingText.text = "Detecting devices";
    }

    public void ShowConnectedGroup(bool animate = true)
    {
        ConnectedGroup.Show(animate, GlitchUITransitionTypes.SlideRight);
        ConnectingGroup.Hide(animate, GlitchUITransitionTypes.SlideLeft);
        RefreshRoleGroup();
    }

    public void SplashPlayPressed()
    {
        ConnectionManager.instance.UseUNet();
        ConnectionManager.instance.StartConnecting();
    }

    public void SplashPlayMultipeerPressed()
    {
        #if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
        ConnectionManager.instance.UseMultipeer();
        #else
        ConnectionManager.instance.UseUNet();
        Debug.LogWarning("Trying to use multipeer, from an unsupported system, using UNet instead!");
        #endif
        ConnectionManager.instance.StartConnecting();
    }

    public void SingleDevicePressed()
    {
        ConnectionManager.instance.StartSingleDevice();
        //this.Hide();
    }

    public void ConnectedSwitchPressed()
    {
        ConnectionManager.instance.SwapRoles();
    }

    public void ConnectedPlayPressed()
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            ConnectionManager.instance.PlaySingle();
        }
        else
        {
            ConnectionManager.instance.Play();
        }
    }


    void RefreshRoleGroup()
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            RolePutterGroup.Show();
            RoleWorldGroup.Hide();
            RoleSwapButton.Hide();
        }
        else if (RoleManager.instance.IsWorld())
        {
            RoleWorldGroup.Show();
            RolePutterGroup.Hide();
        }
        else
        {
            RoleWorldGroup.Hide();
            RolePutterGroup.Show();
            RoleSwapButton.Show();
        }
    }

    void OnRoleChanged()
    {
        RefreshRoleGroup();
    }
}
