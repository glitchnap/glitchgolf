﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using TMPro;
using DG.Tweening;

public class ScoreUIManager : GlitchUIElement
{
    public TMPro.TextMeshProUGUI strokes;
    public TMPro.TextMeshProUGUI hole;
    public TMPro.TextMeshProUGUI par;
    public TMPro.TextMeshProUGUI playername;
    public TMPro.TextMeshProUGUI warningText;

    void Start()
    {
        ScoreManager.instance.OnScoreChanged += UpdateValues;
        GameplayManager.instance.onWarningMessage += ShowWarning;
        warningText.alpha = 0;
    }

    void OnDestroy()
    {
        if(ScoreManager.IsInstantiated())
            ScoreManager.instance.OnScoreChanged -= UpdateValues;
    }

    void UpdateValues()
    {
        hole.text = (CourseManager.instance.CurrentHoleIndex + 1) + "";
        strokes.text = "STROKE  " + ScoreManager.instance.strokeCounter + "";
        par.text = "PAR  " + (CourseManager.instance.CurrentCourse == null? 0 : CourseManager.instance.GetCurrentHole().par);

        var currentPlayerName = ScoreManager.instance.GetPlayers()[ScoreManager.instance.currentPlayerID].playerName;
        if (ScoreManager.instance.GetPlayers().Count == 1 && currentPlayerName.ToLower() == ScoreManager.instance.defaultName.ToLower())
            playername.text = "";
        else
            playername.text = currentPlayerName;
    }

    public void ShowWarning(string Message)
    {
        warningText.alpha = 1;
        warningText.text = Message;
        
        DOTween.To(
            () => warningText.alpha, 
            (x) => warningText.alpha = x, 
            0, 
            1).SetDelay(1);
    }
}
