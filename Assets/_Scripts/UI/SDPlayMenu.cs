using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;

public class SDPlayMenu : GlitchUIElement
{
    public ScoreBoardUI scoreBoardUI;

    public void RestartPressed()
    {
        //CourseManager.instance.Reset();
        CourseManager.instance.ChangeHole(0);
        this.Hide();
    }

    public void QuitPressed()
    {
        GlobalUIManager.instance.GotoMainMenu();
    }

    public void NextHolePressed()
    {
        CourseManager.instance.GoToNextLevel();
    }

    public void PrevHolePressed()
    {
        CourseManager.instance.GoToPreviousLevel();
    }

    public void TutorialPressed()
    {
        GlobalUIManager.instance.ShowTutorial();
    }
}
