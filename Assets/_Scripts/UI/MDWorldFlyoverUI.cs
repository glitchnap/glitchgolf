using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;
using GlitchConnect.Commands;
using System;

public class MDWorldFlyoverUI : GlitchUIElement
{
    public TextMeshProUGUI HoleText;
    public TextMeshProUGUI ParText;

    void Start()
    {
        CourseManager.OnHoleChanged += OnHolechanged;
    }

    private void OnHolechanged(int obj)
    {
        Refresh();
    }

    public override void OnShow()
    {
        Refresh();
    }

    public void Refresh()
    {
        HoleText.text = "HOLE " + (CourseManager.instance.CurrentHoleIndex + 1);
        ParText.text = "PAR " + (CourseManager.instance.GetCurrentHole().par);
    }
}
