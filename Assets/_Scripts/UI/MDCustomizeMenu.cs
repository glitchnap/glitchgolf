using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;
using GlitchConnect.Commands;

public class MDCustomizeMenu : GlitchUIElement
{
    public TextMeshProUGUI ballName;
    public TextMeshProUGUI putterName;

    void Start()
    {
        UpdateSettings();
    }

    public void BackPressed()
    {
        UpdateSettings();
        this.Hide();
    }

    public void UpdateSettings()
    {
        ConnectionManager.instance.SendCommand(new CustomizePutterAndBallCommand(PutterAndBallManager.instance.BallID, PutterAndBallManager.instance.PutterID));
        RefreshLabels();
    }


    public void RefreshLabels()
    {
        ballName.text = PutterAndBallManager.instance.GetCurrentBall().GetComponent<BallSettings>().ballNiceName;
        putterName.text = PutterAndBallManager.instance.GetCurrentPutter().GetComponent<PutterPrefab>().putterNiceName;
    }

    public void GoToNextBall()
    {
        PutterAndBallManager.instance.GoToNextBall();
        UpdateSettings();
    }
    public void GoToPrevBall()
    {
        PutterAndBallManager.instance.GoToPrevBall();
        UpdateSettings();
    }
    public void GoToNextPutter()
    {
        PutterAndBallManager.instance.GoToNextPutter();
        UpdateSettings();
    }
    public void GoToPrevPutter()
    {
        PutterAndBallManager.instance.GoToPrevPutter();
        UpdateSettings();
    }

    public void ShadowsTogglePressed()
    {
        ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.ToggleShadows));
    }
}
