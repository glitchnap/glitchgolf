﻿using UnityEngine;
using System.Collections;
using System;

public class SDFinalScore : GlitchUIElement
{
    public ScoreBoardUI scoreBoardUI;

    void Start()
    {
        CourseManager.OnCourseChanged += OnCourseChanged;
    }

    void OnDestroy()
    {
        CourseManager.OnCourseChanged -= OnCourseChanged;
    }

    public override void OnShow()
    {
        GolfCameraFollow.instance.EnableFlyOver();
    }

    public override void OnHide()
    {
        GolfCameraFollow.instance.DisableFlyOver();
    }

    public void PlayAgainPressed()
    {
        LoadCourse(CourseManager.instance.CurrentCourseIndex);
    }

    public void NextCoursePressed()
    {
        var nextcourseID = CourseManager.instance.CurrentCourseIndex + 1;
        if (nextcourseID >= CourseManager.instance.Courses.Length) nextcourseID = 0;

        LoadCourse(nextcourseID);
    }

    void LoadCourse(int id)
    {
        CourseManager.instance.ChangeCourse(id);
    }

    void OnCourseChanged()
    {
        GlobalUIManager.instance.GotoInGame();
    }
}
