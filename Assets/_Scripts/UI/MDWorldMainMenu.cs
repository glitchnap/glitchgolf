using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;

public class MDWorldMainMenu : GlitchUIElement
{
    public Image coursePreviewImage;
    public TMPro.TextMeshProUGUI courseNameText;

    private int currentCourseID;

    void Start()
    {
        currentCourseID = 0;
        GlitchConnect.Commands.CoursePreviewCommand.ChanseCourseReceived += OnChangeCourseReceived;
        RefreshCourseImage();
    }

    public override void OnShow()
    {
        currentCourseID = 0;
        RefreshCourseImage();
    }

    void OnChangeCourseReceived(int index)
    {
        currentCourseID = index;
        RefreshCourseImage();
    }

    void RefreshCourseImage()
    {
        if (currentCourseID >= CourseManager.instance.Courses.Length) currentCourseID = 0;
        if (currentCourseID < 0) currentCourseID = CourseManager.instance.Courses.Length-1;

        courseNameText.text = CourseManager.instance.Courses[currentCourseID].niceName;
        coursePreviewImage.sprite = CourseManager.instance.Courses[currentCourseID].smallCoursePreview;
    }
}
