using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;

public class SDCustomizeMenu : GlitchUIElement
{
    public Toggle flipDirection;
    public Toggle leftHanded;
    public Toggle maxStrokes;
    public Toggle gyroRotation;
    public Toggle tiltRotation;
    public Toggle shadowToggle;
    public TextMeshProUGUI ballName;
    public TextMeshProUGUI putterName;

    void Start()
    {
        flipDirection.isOn = SDInputManager.instance.gyroFlip;
        leftHanded.isOn = SDInputManager.instance.leftHanded;
        maxStrokes.isOn = (ScoreManager.instance.maxStrokes > 0);
        gyroRotation.isOn = SDInputManager.instance.gyroRotate;
        tiltRotation.isOn = SDInputManager.instance.tiltRotate;
        RefreshLabels();
    }

    public void BackPressed()
    {
        UpdateSettings();
        this.Hide();
    }

    public void UpdateSettings()
    {
        GameplayManager.instance.SetBall(PutterAndBallManager.instance.BallID);
        GameplayManager.instance.SetPutter(PutterAndBallManager.instance.PutterID);

        SDInputManager.instance.gyroFlip = flipDirection.isOn;
        SDInputManager.instance.leftHanded = leftHanded.isOn;

        ScoreManager.instance.maxStrokes = (maxStrokes ? 8 : -1);

        GameplayManager.instance.ResetPutterAndBall();

    }

    public override void OnShow()
    {
        shadowToggle.isOn = QualitySettings.GetQualityLevel() == 1;
    }

    public void ToggleGyroRotation(bool value)
    {
        if (value)
        {
            SDInputManager.instance.tiltRotate = tiltRotation.isOn = false;
            SDInputManager.instance.gyroRotate = true;
        }
        else
        {
            SDInputManager.instance.gyroRotate = false;
        }
    }

    public void ToggleTiltRotation(bool value)
    {
        if (value)
        {
            SDInputManager.instance.gyroRotate = gyroRotation.isOn = false;
            SDInputManager.instance.tiltRotate = true;
        }
        else
        {
            SDInputManager.instance.tiltRotate = false;
        }
    }

    public void RefreshLabels()
    {
        ballName.text = PutterAndBallManager.instance.GetCurrentBall().GetComponent<BallSettings>().ballNiceName;
        putterName.text = PutterAndBallManager.instance.GetCurrentPutter().GetComponent<PutterPrefab>().putterNiceName;
    }

    public void GoToNextBall()
    {
        PutterAndBallManager.instance.GoToNextBall();
        RefreshLabels();
    }

    public void GoToPrevBall()
    {
        PutterAndBallManager.instance.GoToPrevBall();
        RefreshLabels();
    }

    public void GoToNextPutter()
    {
        PutterAndBallManager.instance.GoToNextPutter();
        RefreshLabels();
    }

    public void GoToPrevPutter()
    {
        PutterAndBallManager.instance.GoToPrevPutter();
        RefreshLabels();
    }

    public void ShadowTogglePressed()
    {
        var qualityLevel = 0;
        if (shadowToggle.isOn) qualityLevel = 1;
        QualitySettings.SetQualityLevel(qualityLevel);
        DebugExtension.Blip("Setting quality level " + qualityLevel);
    }
}
