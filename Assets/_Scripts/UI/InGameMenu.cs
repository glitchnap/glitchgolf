using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;
using System;
using GlitchConnect.Commands;

public class InGameMenu : GlitchUIElement
{
    public TMPro.TextMeshProUGUI text;

    public GlitchUIElement SDScoreUI;
    public GlitchUIElement SDPlayUI;
    public GlitchUIElement SDPlayMenu;
    public GlitchUIButton SDPlayMenuToggle;
    public GlitchUIElement SDCustomizeMenu;
    public GlitchUIElement SDFinalScore;

    public GlitchUIElement MDPutterPlayMenu;
    public GlitchUIElement MDWorldScoreUI;
    public GlitchUIElement MDWorldPlayMenu;
    public GlitchUIElement MDCustomizeMenu;
    public GlitchUIElement MDWorldFinalScore;
    public GlitchUIElement MDPutterFinalScore;
    public GlitchUIElement MDWorldCalibrationUI;
    public GlitchUIElement MDPutterCalibrationUI;
    public GlitchUIElement MDWorldFlyoverUI;

    void Start()
    {
        MenuCommand.CommandReceived += OnMenuCommandReceived;
        GameplayManager.instance.onFinalHoleReachedEvent += OnFinalHoleReached;

        CourseManager.OnHoleChanged += OnHoleChanged;
        CourseManager.OnCourseChanged += UpdateCourseLabel;
    }
    
    void OnHoleChanged(int hole)
    {
        UpdateCourseLabel();

    }

    void UpdateCourseLabel()
    {
        if(RoleManager.instance.IsWorld())
        {
            ConnectionManager.instance.SendCommand(new CourseInfoCommand(CourseManager.instance.CurrentCourseIndex, CourseManager.instance.CurrentHoleIndex));
        }
    }

    void OnMenuCommandReceived(MenuCommand.Actions action)
    {
        switch (action)
        {
            case MenuCommand.Actions.PlayMenuShow:
                MDWorldPlayMenu.Show();
                break;
            case MenuCommand.Actions.PlayMenuHide:
                MDWorldPlayMenu.Hide();
                break;
            case MenuCommand.Actions.PlayMenuToggle:
                MDWorldPlayMenu.Toggle();
                break;
            case MenuCommand.Actions.FinalScoreShow:
                ShowFinalScoreMenu();
                break;
            case MenuCommand.Actions.FinalScoreHide:
                MDPutterHideFinalScoreMenu();
                break;
            case MenuCommand.Actions.ToggleShadows:
                var qualityLevel = 0;
                if (QualitySettings.GetQualityLevel() == 0) qualityLevel = 1;
                QualitySettings.SetQualityLevel(qualityLevel);
                break;
            default:
                break;
        }
    }

    public void Show(bool animate = true, GlitchUITransitionTypes transition = GlitchUITransitionTypes.Fade)
    {
        (this as IGlitchUIElement).Show(animate, transition);

        HideAllMenus();

        switch (RoleManager.instance.role)
        {
            case GlitchGolfRoles.none:
                break;
            case GlitchGolfRoles.putter:
                MDPutterPlayMenu.Show();
                break;
            case GlitchGolfRoles.world:
                MDWorldScoreUI.Show();
                break;
            case GlitchGolfRoles.singleDevice:
                SDScoreUI.Show();
                SDPlayMenuToggle.Show();
                SDPlayUI.Show();
                break;
            default:
                break;
        }
    }

    void HideAllMenus()
    {
        SDScoreUI.Hide();
        SDPlayUI.Hide();
        SDPlayMenu.Hide();
        SDPlayMenuToggle.Hide();
        SDCustomizeMenu.Hide();
        SDFinalScore.Hide();

        MDWorldPlayMenu.Hide();
        MDWorldScoreUI.Hide();
        MDPutterPlayMenu.Hide();
        MDCustomizeMenu.Hide();
        MDWorldFinalScore.Hide();
        MDPutterFinalScore.Hide();
        MDWorldCalibrationUI.Hide();
        MDPutterCalibrationUI.Hide();
        MDWorldFlyoverUI.Hide();
    }

    public void SDPlayMenuTogglePressed()
    {
        SDPlayMenu.Toggle();
    }

    public void SDCustomizeMenuPressed()
    {
        SDCustomizeMenu.Show();
    }

    public void MDCustomizeMenuOpenPressed()
    {
        MDCustomizeMenu.Show();
        ConnectionManager.instance.SendCommand(new GlitchConnect.Commands.MenuCommand(GlitchConnect.Commands.MenuCommand.Actions.PlayMenuToggle));
    }

    public void MDCustomizeMenuBackPressed()
    {
        MDCustomizeMenu.Hide();
        ConnectionManager.instance.SendCommand(new GlitchConnect.Commands.MenuCommand(GlitchConnect.Commands.MenuCommand.Actions.PlayMenuToggle));
    }

    private void OnFinalHoleReached()
    {
        ShowFinalScoreMenu();
    }

    void ShowFinalScoreMenu()
    {
        HideAllMenus();
        

        switch (RoleManager.instance.role)
        {
            case GlitchGolfRoles.none:
                break;
            case GlitchGolfRoles.putter:
                MDPutterFinalScore.Show();
                break;
            case GlitchGolfRoles.world:
                MDWorldFinalScore.Show();
                break;
            case GlitchGolfRoles.singleDevice:
                SDFinalScore.Show();
                break;
            default:
                break;
        }
    }

    void MDPutterHideFinalScoreMenu()
    {
        MDPutterFinalScore.Hide();
        MDPutterPlayMenu.Show();
    }

    public void CalibrateShow()
    {
        switch (RoleManager.instance.role)
        {
            case GlitchGolfRoles.putter:
                MDPutterCalibrationUI.Show();
                break;
            case GlitchGolfRoles.world:
                MDWorldCalibrationUI.Show();
                break;
            default:
                break;
        }
    }

    public void CalibrateHide()
    {
        switch (RoleManager.instance.role)
        {
            case GlitchGolfRoles.putter:
                MDPutterCalibrationUI.Hide();
                break;
            case GlitchGolfRoles.world:
                MDWorldCalibrationUI.Hide();
                break;
            default:
                break;
        }
    }

    public void FlyoverShow()
    {
        if (RoleManager.instance.IsWorld() && !MDWorldFinalScore.isShowing)
        {
            MDWorldFlyoverUI.Show();
            MDWorldScoreUI.Hide();
        }
    }

    public void FlyoverHide()
    {
        if (RoleManager.instance.IsWorld())
        {
            MDWorldFlyoverUI.Hide();
            MDWorldScoreUI.Show();
        }
    }
}
