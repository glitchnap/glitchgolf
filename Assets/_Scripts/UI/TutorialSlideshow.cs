﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

public class TutorialSlideshow : GlitchUIElement {

    public Transform pagesTransform;
    private List<GlitchUIElement> pages;
    public Image progressFill;
    private int pageCounter;
    public Action OnHideEvent;

    void Start()
    {
        pages = new List<GlitchUIElement>();
        for (int i = 0; i < pagesTransform.childCount; i++)
        {
            var page = pagesTransform.GetChild(i).GetComponent<GlitchUIElement>();
            if(page!=null)
                pages.Add(page);
        }

        MiniSwipeDetector.instance.OnSwipeEvent += OnSwipe;
    }

    private void OnSwipe(MiniSwipeDetector.SwipeDirection direction)
    {
        if(this.isShowing)
        {
            DebugExtension.Blip(direction.ToString());
            if (direction == MiniSwipeDetector.SwipeDirection.Left)
            {
                //Advance();
            }
            if (direction == MiniSwipeDetector.SwipeDirection.Right)
            {
                WhateverTheOppositeOfAdvanceIs();
                WhateverTheOppositeOfAdvanceIs();
            }
        }
    }

    public override void OnShow()
    {
        pageCounter = 0;
        UpdatePages();
    }

    public override void OnHide()
    {
        if (OnHideEvent != null) OnHideEvent.Invoke();
    }


    public void Advance()
    {
        pageCounter++;
        UpdatePages();
    }

    public void WhateverTheOppositeOfAdvanceIs()
    {
        pageCounter--;
        if (pageCounter < 0) pageCounter = 0;
        UpdatePages();
    }

    private void UpdatePages()
    {
        foreach (var item in pages)
        {
            item.Hide();
        }

        progressFill.fillAmount = (pageCounter + 1) / ((float)pages.Count);

        if (pageCounter < pages.Count)
            pages[pageCounter].Show();
        else
            this.Hide();
    }
}
