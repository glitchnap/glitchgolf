using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;

public class MainMenu : GlitchUIElement
{
    public GlitchUIElement SDMainMenu;
    public GlitchUIElement MDPutterMainMenu;
    public GlitchUIElement MDWorldMainMenu;

    void Start()
    {
    }

    public void Show(bool animate = true, GlitchUITransitionTypes transition = GlitchUITransitionTypes.Fade)
    {
        SDMainMenu.Hide();
        MDPutterMainMenu.Hide();
        MDWorldMainMenu.Hide();

        (this as IGlitchUIElement).Show(animate, transition);
        switch (RoleManager.instance.role)
        {
            case GlitchGolfRoles.none:
                break;
            case GlitchGolfRoles.putter:
                MDPutterMainMenu.Show();
                break;
            case GlitchGolfRoles.world:
                MDWorldMainMenu.Show();
                break;
            case GlitchGolfRoles.singleDevice:
                SDMainMenu.Show();
                break;
            default:
                break;
        }
    }
}
