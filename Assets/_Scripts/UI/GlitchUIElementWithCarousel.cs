﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GlitchUIElementWithCarousel : GlitchUIElement {

    public CanvasGroup[] images;
    public float time;
    private float counter;
    private int imageID;
    public float fadeLength = -1;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;
        if (counter > time) {
            counter = 0;
            Advance();
        }
	}

    public override void OnShow()
    {
        DoReset();
    }

    void DoReset()
    {
        imageID = 0;
        counter = 0;
        UpdateImages();
    }

    void Advance()
    {
        imageID++;
        if (imageID >= images.Length) DoReset();
        UpdateImages();
    }

    void UpdateImages()
    {
        foreach (var item in images)
        {
            FadeAssistant.FadeOut(item, fadeLength).Play();
        }
        FadeAssistant.FadeIn(images[imageID], fadeLength).Play();
    }
}
