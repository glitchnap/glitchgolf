﻿using UnityEngine;
using System.Collections;
using GlitchConnect.Commands;

public class MDPutterFinalScoreMenu : GlitchUIElement
{
    public void PlayAgainPressed()
    {
        ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.FinalScorePlayAgain));
    }

    public void NextCoursePressed()
    {
        ConnectionManager.instance.SendCommand(new MenuCommand(MenuCommand.Actions.FinalScorePlayNext));
    }
}
