using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Holoville.HOTween;
using TMPro;
using GlitchConnect.Commands;

public class MDWorldCalibrationUI : GlitchUIElement
{
    [ReadOnlyAttribute]
    public float lastCalibration;
    [ReadOnlyAttribute]
    public float lastLevelCalibration;

    void Start()
    {
        NavigationCommand.OnNavigationCommandReceived += NavCommandRecieved;
    }

    public override void OnShow()
    {
        MDInputManager.instance.isCalibrating = true;
        //Disable the club if the player hit calibrate mid swing...
        GameplayManager.instance.UpdateClubEnabled(false);
    }

    public override void OnHide()
    {
        MDInputManager.instance.isCalibrating = false;
    }

    void NavCommandRecieved(NavigationCommands cmd, float val)
    {
        if (RoleManager.instance.IsWorld())
        {
            switch (cmd)
            {
                case NavigationCommands.StartCalibrate:
                    if (!isShowing)
                    {
                        GlobalUIManager.instance.inGameMenu.CalibrateShow();
                        lastCalibration = NavigationManager.instance.putterGhostCalibrate.eulerAngles.y;
                        lastLevelCalibration = NavigationManager.instance.GetLevelCalibration();
                        NavigationManager.instance.Calibrate(0, true);
                    }
                    break;
                case NavigationCommands.Stop:
                case NavigationCommands.EndCalibrate:
                    var angle = GameplayManager.instance.GetPutterAngle2D() - 90;
                    NavigationManager.instance.DoLevelCalibration(0);
                    NavigationManager.instance.Calibrate(angle, true);
                    NavigationManager.instance.DoLevelCalibration(lastLevelCalibration);
                    GlobalUIManager.instance.inGameMenu.CalibrateHide();
                    break;
                case NavigationCommands.CancelCalibrate:
                    GlobalUIManager.instance.inGameMenu.CalibrateHide();
                    NavigationManager.instance.Calibrate(lastCalibration, true);
                    break;
                default:
                    break;
            }
        }
    }

}
