﻿using UnityEngine;
using System.Collections;
using GlitchConnect.Commands;

public class MDPutterCalibrationUI : GlitchUIElement
{

    public override void OnShow()
    {
        MDInputManager.instance.isCalibrating = true;
        ConnectionManager.instance.SendCommand(new NavigationCommand(NavigationCommands.StartCalibrate, 0));
    }

    public override void OnHide()
    {
        MDInputManager.instance.isCalibrating = false;
    }

    public void CalibratePressed()
    {
        GlobalUIManager.instance.inGameMenu.CalibrateHide();
        //ConnectionManager.instance.SendCommand(new NavigationCommand(NavigationCommands.StartCalibrate, InputHelper.GetLocalDelta().x));
        ConnectionManager.instance.SendCommand(new NavigationCommand(NavigationCommands.EndCalibrate, 0));
    }

    public void CancelPressed()
    {
        GlobalUIManager.instance.inGameMenu.CalibrateHide();
        ConnectionManager.instance.SendCommand(new NavigationCommand(NavigationCommands.CancelCalibrate, 0));
    }
}
