﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;


public class FunFeedbackManager : MonoSingleton<FunFeedbackManager> {

    public Transform cam;
    public Transform HoleInOne;
    public Transform[] HoleInOneStars;
    public Transform OutOfBounds;
    public Transform Par;
    public Transform[] ParStars;
    public Transform Star;
    public Transform[] StarStars;
    public Transform Generic;
    public Transform[] GenericStars;

    public TMPro.TextMeshPro BigText;
    public TMPro.TextMeshPro SmallText;


    private Sequence HoleInOneTweener;
    private Sequence OutOfBoundsTweener;
    private Sequence ParTweener;
    private Sequence StarTweener;
    private Sequence GenericTweener;

    private float starZ;
    private float startCamZ;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        starZ = HoleInOneStars[0].localScale.z;
        startCamZ = cam.transform.localPosition.z;

        HideAll();
        cam.transform.localPosition = cam.transform.localPosition.withZ(-1000);
    }

    void Update()
    {
        /*
        if(Input.GetKeyDown(KeyCode.X))
        {
            ShowHoleInOne();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            ShowOutOfBounds();
        }*/
    }

    public void HideAll()
    {
        HoleInOne.gameObject.SetActive(false);
        OutOfBounds.gameObject.SetActive(false);
        Par.gameObject.SetActive(false);
        Star.gameObject.SetActive(false);
        Generic.gameObject.SetActive(false);

        KillTweener(HoleInOneTweener);
        KillTweener(OutOfBoundsTweener);
        KillTweener(ParTweener);
        KillTweener(StarTweener);
        KillTweener(GenericTweener);
    }

    public void ShowHoleInOne()
    {
        if (HoleInOne.gameObject.activeSelf) return;

        HoleInOneTweener = GenericShow(HoleInOne);

        for (int i = 0; i < HoleInOneStars.Length; i++)
        {
            var star = HoleInOneStars[i];
            star.localScale = star.localScale.withZ(0.01f);
            HoleInOneTweener.Insert(0.5f + 0.11f*i, HOTween.To(
                star, 0.3f, new TweenParms()
                    .Prop("localScale", star.localScale.withZ(starZ))
                )
            );
        }

        HoleInOneTweener.Play();
    }

    public void ShowPar()
    {
        if (Par.gameObject.activeSelf) return;

        ParTweener = GenericShow(Par);

        for (int i = 0; i < ParStars.Length; i++)
        {
            var star = ParStars[i];
            star.localScale = star.localScale.withZ(0.01f);
            ParTweener.Insert(0.5f + 0.11f * i, HOTween.To(
                star, 0.3f, new TweenParms()
                    .Prop("localScale", star.localScale.withZ(starZ))
                )
            );
        }

        ParTweener.Play();
    }

    public void ShowGeneric(int strokes, int par)
    {
        var underOver = "under";
        if (strokes > par) underOver = "over";
        var amount = strokes - par;
        var absamount = Mathf.Abs(amount);

        SmallText.text = absamount + " " + underOver + " par";

        var big = "";
        if (amount == -1) big = "birdie";
        else if (amount == -2) big = "eagle";
        else if (amount == -3) big = "ALBATROSS";
        else if (amount == -4) big = "CONDOR";
        else if (amount < -4) big = "WHAT?";
        else if (amount == 1) big = "bogey";
        else if (amount > 1) big = "bogey +" + Mathf.Min(amount,5);

        BigText.text = big;

        if (Generic.gameObject.activeSelf) return;

        GenericTweener = GenericShow(Generic);

        for (int i = 0; i < GenericStars.Length; i++)
        {
            bool shouldShowStar = amount < i;
            GenericStars[i].gameObject.SetActive(shouldShowStar);

            var star = GenericStars[i];
            star.localScale = star.localScale.withZ(0.01f);
            GenericTweener.Insert(0.5f + 0.11f * i, HOTween.To(
                star, 0.3f, new TweenParms()
                    .Prop("localScale", star.localScale.withZ(starZ))
                )
            );
        }

        GenericTweener.Play();
    }



    public void ShowStar()
    {
        if (Star.gameObject.activeSelf) return;

        StarTweener= GenericShow(Star,1.75f);

        for (int i = 0; i < StarStars.Length; i++)
        {
            var star = StarStars[i];
            star.localScale = star.localScale.withZ(0.01f);
            StarTweener.Insert(0.5f + 0.11f * i, HOTween.To(
                star, 0.3f, new TweenParms()
                    .Prop("localScale", star.localScale.withZ(starZ))
                )
            );
        }

        StarTweener.Play();
    }

    public void ShowOutOfBounds()
    {
        if (OutOfBounds.gameObject.activeSelf) return;

        OutOfBoundsTweener = GenericShow(OutOfBounds);
        OutOfBoundsTweener.Play();
    }

    Sequence GenericShow(Transform t, float delay=2)
    {
        HideAll();
        t.gameObject.SetActive(true);

        var seq = new Sequence(new SequenceParms().OnComplete(() => {
            t.gameObject.SetActive(false);
        }));

        seq.Append(HOTween.To(cam.transform, 0.5f, new TweenParms()
            .Prop("localPosition", cam.transform.localPosition.withZ(startCamZ))
            .Ease(EaseType.EaseOutSine)
        ));

        seq.Insert(delay, HOTween.To(cam.transform, 0.23f, new TweenParms()
            .Prop("localPosition", cam.transform.localPosition.withZ(-1000))
            .Ease(EaseType.EaseInCirc)
        ));

        return seq;
    }


    void KillTweener(Sequence toKill)
    {
        if (toKill != null && !toKill.isComplete)
        {
            toKill.Kill();
        }
    }
}
