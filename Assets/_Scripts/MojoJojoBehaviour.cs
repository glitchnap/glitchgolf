﻿using UnityEngine;
using System.Collections;

public class MojoJojoBehaviour : MonoBehaviour {

    public float openDelay;
    public float openAnimationDelay;
    public float openTime;
    public float closeDelay;
    
    public float closeTime;
    public float angle;
    public Animator animator;

    private Trapdoor[] trapdoors;

    Coroutine loopCoroutine;
	void Start () {
        trapdoors = transform.parent.GetComponentsInChildren<Trapdoor>();

        loopCoroutine = StartCoroutine(loop());
	}

    void OnDestroy()
    {
        if(loopCoroutine != null)
            StopCoroutine(loopCoroutine);
    }

    IEnumerator loop()
    {
        while (true)
        {
            yield return new WaitForSeconds(openDelay);

            animator.SetTrigger("do");

            yield return new WaitForSeconds(openAnimationDelay);

            foreach (var item in trapdoors)
            {
                item.Open(openTime, angle);
            }

            yield return new WaitForSeconds(closeDelay);

            foreach (var item in trapdoors)
            {
                item.Close(openTime);
            }
        }
    }

}
