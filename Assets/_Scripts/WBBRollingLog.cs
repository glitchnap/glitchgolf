﻿using UnityEngine;
using System.Collections;

public class WBBRollingLog : MonoBehaviour {

    public AnimationCurve curve;
    public float speed;
    public float distance;
    public float rotationSpeed;
    public float timeOffset;
    public Vector3 direction;
    private Rigidbody rb;
    private Vector3 origin;
    private Quaternion originRot;
    public Vector3 transformedDirection;
    void Start () {
        rb = GetComponent<Rigidbody>();
        origin = transform.position;
        transformedDirection = transform.TransformPoint(direction) - transform.position;
    }
	
	void Update () {
        var t = curve.Evaluate(speed * (Time.time + timeOffset));
        transform.localRotation = Quaternion.Euler(0,0,t * rotationSpeed);
        rb.MovePosition(origin + transformedDirection * t * distance);
        
    }
}
