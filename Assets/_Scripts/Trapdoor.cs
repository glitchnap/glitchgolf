﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Trapdoor : MonoBehaviour {

    private Transform partA;
    private Transform partB;

    void Start()
    {
        partA = transform.GetChild(0);
        partB = transform.GetChild(1);
    }

    public void Open(float t, float angle)
    {
        HOTween.To(partA, t, new TweenParms()
            .Prop("localRotation", Vector3.zero.withZ(-angle))
        );
        HOTween.To(partB, t, new TweenParms()
            .Prop("localRotation", Vector3.zero.withZ(angle))
        );
    }

    public void Close(float t)
    {
        HOTween.To(partA, t, new TweenParms()
            .Prop("localRotation", Vector3.zero)
        );
        HOTween.To(partB, t, new TweenParms()
            .Prop("localRotation", Vector3.zero)
        );
    }
}
