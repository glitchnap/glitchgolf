﻿using UnityEngine;
using System.Collections;

public class Spin3D : MonoBehaviour
{

    public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(0,Time.time * speed,0));
	}
}
