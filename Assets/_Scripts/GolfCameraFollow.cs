using UnityEngine;
using System.Collections;
using DG.Tweening;

[System.Serializable]
public struct SingleDeviceCameraSettings
{
    public float lerpSpeed;
    public float angleSpeed;
    public float autoRotationLerpSpeed;

    public Vector3 additionalRotation;
    public Vector3 additionalPosition;

    public  float yOffset;
    [ReadOnlyAttribute]
    public float yOffsetStart;

    public bool angleAlongClub;
    public bool rotateWithClub;

    public bool cameraTiltEnabled;
    public bool cameraZoomEnabled;
    public bool cameraAutoTurnEnabled;
}

[System.Serializable]
public struct MultiDeviceCameraSettings
{
    public float lerpSpeed;
    public float angleSpeed;

    public  float yOffset;
    [ReadOnlyAttribute]
    public float yOffsetStart;

    public bool panAlongClubDirection;
    public float panAlongClubDeadZone;
    public float panAlongClubParentMaxDist;
    public float panDistance;
    public bool panAlongClubNoTilt;

    public bool cameraTiltEnabled;
    public bool cameraZoomEnabled;
}

public class GolfCameraFollow : MonoBehaviour
{
    public static GolfCameraFollow instance;

    [SerializeField, ReadOnly]
    private bool doFlyOver;
    [SerializeField, ReadOnly]
    private bool doingFlyOverTransition;

    public float flyOverLerpSpeed;
    public float flyOverRotateSpeed;
    public float flyOverMovementRadius;
    public bool flyOverRotation;
    public float flyOverRotationHeight;

    [InspectorNoteAttribute("Single Device Settings", NoteColor.pink)]
    public SingleDeviceCameraSettings singleDeviceSettings;

    [InspectorNoteAttribute("Multi Device Settings", NoteColor.yellow)]
    public MultiDeviceCameraSettings multiDeviceSettings;

    [ReadOnly]
    public float lastAutoRotationAngle;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        singleDeviceSettings.yOffsetStart = singleDeviceSettings.yOffset;
        multiDeviceSettings.yOffsetStart = multiDeviceSettings.yOffset;
    }

    void Update()
    {
        if (doFlyOver)
        {
            if (!doingFlyOverTransition)
            {
                DoFlyOver();    
            }
            return;
        }

        if (RoleManager.instance.IsSingleDevice())
        {
            UpdateSingleDevice();
        }
        else
        {
            UpdateMultiDevice();
        }
    }

    private void UpdateSingleDevice()
    {
        // find ball
        var ballTarget = GameplayManager.instance.ball.transform;

        // move parent onto ball
        var parentDestination = new Vector3(ballTarget.position.x, ballTarget.position.y, ballTarget.position.z);
        transform.parent.position = Vector3.Lerp(transform.parent.position, parentDestination, singleDeviceSettings.lerpSpeed * Time.deltaTime);

        // do auto rotating while the ball is rolling
        if (singleDeviceSettings.cameraAutoTurnEnabled && !GameplayManager.instance.ball.WasJustReset() && !GameplayManager.instance.ball.IsSleeping())
        {
            transform.parent.rotation = transform.parent.rotation * Quaternion.Inverse(Quaternion.Euler(89.96f, 0, 0));
            transform.parent.LookAt2D(ballTarget.position, Vector3.up, singleDeviceSettings.autoRotationLerpSpeed * Time.deltaTime);
            lastAutoRotationAngle = transform.parent.rotation.eulerAngles.y;
            transform.parent.rotation = transform.parent.rotation * Quaternion.Euler(89.96f, 0, 0);
        }
        else
        {
            // rotate parent with calibration
            if (singleDeviceSettings.rotateWithClub)
            {
                // support club on both left and right hand side
                if (SDInputManager.IsInstantiated() && SDInputManager.instance.leftHanded)
                {
                    transform.parent.rotation = Quaternion.Lerp(transform.parent.rotation, Quaternion.Euler(89.96f, 90f + GameplayManager.instance.putterGhostCalibrate.eulerAngles.y, 0), singleDeviceSettings.autoRotationLerpSpeed * Time.deltaTime);
                }
                else
                {
                    transform.parent.rotation = Quaternion.Lerp(transform.parent.rotation, Quaternion.Euler(89.96f, -90f + GameplayManager.instance.putterGhostCalibrate.eulerAngles.y, 0), singleDeviceSettings.autoRotationLerpSpeed * Time.deltaTime);    
                }
            }
            else
            {
                transform.parent.rotation = Quaternion.Lerp(transform.parent.rotation, Quaternion.Euler(89.96f, 0, 0), singleDeviceSettings.autoRotationLerpSpeed * Time.deltaTime);
            }
        }

        // zoom based on current zoom setting
        var zoomDistance = singleDeviceSettings.yOffset;

        // zoom based on distance to ball
        if (singleDeviceSettings.cameraZoomEnabled && !GameplayManager.instance.ball.IsSleeping())
        {
            var parentToBallDistance = Vector3.Distance(parentDestination, transform.parent.position);
            zoomDistance = Mathf.Abs(zoomDistance) > Mathf.Abs(singleDeviceSettings.yOffsetStart - parentToBallDistance) ? zoomDistance : (singleDeviceSettings.yOffsetStart - parentToBallDistance);
        }

        // move camera over ball
        var cameraDestination = new Vector3(0, 0, zoomDistance);

        // rotate camera to look 'ahead'
        var cameraDestinationRot = Quaternion.identity;

        if (singleDeviceSettings.angleAlongClub)
        {
            cameraDestinationRot = Quaternion.Euler(singleDeviceSettings.additionalRotation);

            // recalculate position
            cameraDestination = singleDeviceSettings.additionalPosition + singleDeviceSettings.yOffsetStart * Vector3.forward;
            // and zooming
            cameraDestination -= (singleDeviceSettings.yOffsetStart - zoomDistance) * Vector3.up + (singleDeviceSettings.yOffsetStart - zoomDistance) * Vector3.forward * 0.5f;
        }

        // lerp position and rotation to projected destinations
        transform.localPosition = Vector3.Lerp(transform.localPosition, cameraDestination, singleDeviceSettings.lerpSpeed * Time.deltaTime);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, cameraDestinationRot, singleDeviceSettings.angleSpeed * Time.deltaTime);

        // tilt camera to always look at ball (basically avoid ball moving out of the camera frustrum)
        if (singleDeviceSettings.cameraTiltEnabled)
        {
            //NOTE: this should be lerped, but we'd have to implmement our own LookAt3D...
            transform.LookAt(ballTarget.position.addY(0.5f)); //Look a little bit above the ball...
        }
    }

    private void UpdateMultiDevice()
    {
        // find ball
        var ballTarget = GameplayManager.instance.ball.transform;

        // move parent onto ball
        var parentDestination = new Vector3(ballTarget.position.x, ballTarget.position.y, ballTarget.position.z);
        transform.parent.position = Vector3.Lerp(transform.parent.position, parentDestination, multiDeviceSettings.lerpSpeed * Time.deltaTime);

        // zoom based on current zoom setting
        var zoomDistance = multiDeviceSettings.yOffset;

        // calculate distance to ball
        var parentToBallDistance = Vector3.Distance(parentDestination, transform.parent.position);

        // zoom based on distance to ball
        if (multiDeviceSettings.cameraZoomEnabled && !GameplayManager.instance.ball.IsSleeping())
        {
            zoomDistance = Mathf.Abs(zoomDistance) > Mathf.Abs(multiDeviceSettings.yOffsetStart - parentToBallDistance) ? zoomDistance : (multiDeviceSettings.yOffsetStart - parentToBallDistance);
        }

        // disallow tilting while calibrating
        var allowTilt = !MDInputManager.instance.isCalibrating;
        // disallow panning, during calibration, swinging, and while putter/ball is moving
        var allowPan = !MDInputManager.instance.isCalibrating &&
                       !GameplayManager.instance.clubEnabled &&
                       GameplayManager.instance.hasFinishedMovingPutter &&
                       GameplayManager.instance.ball.IsSleeping();

        // set camera destination over ball
        Vector3 cameraDestination = new Vector3(0, 0, zoomDistance);

        // pan means look around using the remote pointing direction
        if (multiDeviceSettings.panAlongClubDirection && allowPan)
        {
            // make sure putter is close to ball and that it's above the pan deadzone
            var putterToBallVector = ballTarget.position - GameplayManager.instance.putter.footCollider.transform.position;
            if (Mathf.Pow(multiDeviceSettings.panAlongClubDeadZone, 2f) < putterToBallVector.sqrMagnitude && parentToBallDistance < multiDeviceSettings.panAlongClubParentMaxDist)
            {
                // calculate pan amount
                var panAddition = new Vector3(putterToBallVector.x, putterToBallVector.z, 0).normalized * (putterToBallVector.sqrMagnitude - Mathf.Pow(multiDeviceSettings.panAlongClubDeadZone, 2f)) * multiDeviceSettings.panDistance;

                //Take the level rotation into account.
                panAddition = Quaternion.AngleAxis(NavigationManager.instance.GetLevelCalibration(), Vector3.forward) * panAddition;

                // add pan amount if tilting, and subtract if not
                if (multiDeviceSettings.panAlongClubNoTilt)
                {
                    // override tilting while panning, and no tilt is enabled
                    allowTilt = false;
                    cameraDestination -= panAddition;
                }
                else
                {
                    cameraDestination += panAddition;
                }

                // zoom out while panning, based on the pan amount, zoom out 3x faster than you "pan out"
                var panZoomAddition = Mathf.Clamp(panAddition.magnitude * 3f, 0, flyOverRotationHeight - Mathf.Abs(zoomDistance));
                cameraDestination = cameraDestination.addZ(-panZoomAddition);
            }
        }

        // lerp position and rotation to projected destinations
        transform.localPosition = Vector3.Lerp(transform.localPosition, cameraDestination, multiDeviceSettings.lerpSpeed * Time.deltaTime);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, multiDeviceSettings.angleSpeed * Time.deltaTime);

        // tilt camera to always look at ball (basically avoid ball moving out of the camera frustrum)
        if (multiDeviceSettings.cameraTiltEnabled && allowTilt && !GameplayManager.instance.ball.IsSleeping())
        {
            //NOTE: this should be lerped, but we'd have to implmement our own LookAt3D...
            //transform.rotation = Quaternion.Lerp(transform.localRotation, Quaternion.LookRotation(ballTarget.position - transform.position, transform.up), angleSpeed * Time.deltaTime);
            transform.LookAt(ballTarget, transform.up); 
        }
    }

    public void EnableFlyOver()
    {
        if (doFlyOver || doingFlyOverTransition)
        {
            return;
        }

        doFlyOver = true;
        doingFlyOverTransition = true;

        var rotationTarget = transform.parent.eulerAngles.withY(0);
        var positionTarget = transform.parent.position;

        Vector3 cameraDestination = new Vector3(0, flyOverMovementRadius, -flyOverRotationHeight);

        if (CourseManager.instance.CurrentCourse != null)
        {
            var hole = CourseManager.instance.GetCurrentHole();
            rotationTarget = Quaternion.LookRotation(Vector3.down, hole.StartPosition.forward).eulerAngles;
            positionTarget = hole.FlyOverCenterPosition;
        }
        else
        {
            //This should not happen
            Debug.LogError("YOU'RE CALLING ENABLE FLYOVER TOO EARLY!");
        }

        transform.DOLocalMove(cameraDestination, 1f);
        transform.DOLocalRotateQuaternion(Quaternion.identity, 1f);

        transform.parent.DOMove(positionTarget, 1f);
        transform.parent.DORotate(rotationTarget, 1f).OnComplete(() =>
            {
                doingFlyOverTransition = false;
            });
    }

    private void DoFlyOver()
    {
        if (CourseManager.instance.CurrentCourse != null)
        {
            var hole = CourseManager.instance.GetCurrentHole();
            var parentTarget = hole.FlyOverCenterPosition;

            transform.parent.position = Vector3.Slerp(transform.parent.position, parentTarget, flyOverLerpSpeed * Time.deltaTime);
            transform.parent.Rotate(Vector3.back, flyOverRotateSpeed * Time.deltaTime, Space.Self);

            if (!flyOverRotation)
            {
                transform.rotation = Quaternion.LookRotation(Vector3.down, hole.StartPosition.forward);
            }
        }
    }

    public void DisableFlyOver()
    {
        if (!doFlyOver || doingFlyOverTransition)
        {
            return;
        }

        doingFlyOverTransition = true;

        var rotationTarget = transform.parent.eulerAngles.withY(0);
        var positionTarget = transform.parent.position;
        var lookRotation = Quaternion.identity;

        var zoomDistance = RoleManager.instance.IsSingleDevice() ? singleDeviceSettings.yOffset : multiDeviceSettings.yOffset;
        Vector3 cameraDestination = new Vector3(0, 0, zoomDistance);

        if (CourseManager.instance.CurrentCourse != null)
        {
            var hole = CourseManager.instance.GetCurrentHole();
            positionTarget = hole.StartPosition.position;

            rotationTarget = Quaternion.LookRotation(Vector3.down, hole.StartPosition.forward).eulerAngles;
            NavigationManager.instance.DoLevelCalibration(hole.StartPosition.eulerAngles.y);
        }
        else
        {
            //This should not happen
            Debug.LogError("YOU'RE CALLING ENABLE FLYOVER TOO EARLY!");
        }

        transform.DOLocalMove(cameraDestination, 1f);
        transform.DOLocalRotateQuaternion(lookRotation, 1f);

        transform.parent.DOMove(positionTarget, 1f);
        transform.parent.DORotate(rotationTarget, 1f).OnComplete(() =>
            {
                doFlyOver = false;
                doingFlyOverTransition = false;
            });
    }

    public void SetCameraHeight(float amount)
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            singleDeviceSettings.yOffset = -amount;
        }
        else
        {
            multiDeviceSettings.yOffset = -amount;
        }
    }

    public float GetCameraHeight()
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            return -singleDeviceSettings.yOffset;    
        }
        else
        {
            return -multiDeviceSettings.yOffset;
        }

    }

    public void ResetZoom()
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            singleDeviceSettings.yOffset = singleDeviceSettings.yOffsetStart;    
        }
        else
        {
            multiDeviceSettings.yOffset = multiDeviceSettings.yOffsetStart;
        }
    }

    public void SetupCameraZoomIn()
    {
        //TODO: make this work!
//        transform.parent.position = transform.parent.position.withY(200f);
//        var cam = transform.GetComponent<Camera>();
//        cam.fieldOfView = 10;
//        DOTween.To(() => cam.fieldOfView, (x) => cam.fieldOfView = x, 60, 1f);
//        doFlyOver = true;
    }
}
