﻿using UnityEngine;
using System.Collections;

public class FinnBehaviour : MonoBehaviour {

    public Renderer rnd;

    public void OnTriggerEnter(Collider col)
    {
        rnd.materials[1].SetTextureOffset("_MainTex", new Vector2(0.5f, 0));
        Timer.DelayedExecute(() => { rnd.materials[1].SetTextureOffset("_MainTex", new Vector2(0, 0)); }, 1);
    }
}
