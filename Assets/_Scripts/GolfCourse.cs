using UnityEngine;
using System.Collections;

public class GolfCourse : MonoBehaviour
{
    public Sprite BigCoursePreview;
    public GolfHole[] Holes;
}
