using UnityEngine;
using System.Collections;
using GlitchConnect;
using GlitchConnect.Commands;

public class GlitchCommandTestBed : MonoBehaviour
{

    public string SerializedPackage;

    void Start()
    {
        GlitchConnector.instance.StartConnecting(GlitchConnector.ConnectorTypes.GlitchMultipeer, 0, "test");
        
    }

    void Update()
    {
	
    }

    void OnGUI()
    {
        if (GUILayout.Button("serialize command"))
        {
            DebugExtension.Blip(this, "SERIALIZING COMMAND ....", Color.green);
            SerializedPackage = GlitchCommandHandler.Serialize(new TimestampCommand(100, 100, false));
            Debug.Log(SerializedPackage);
        }
        if (GUILayout.Button("send command"))
        {
            DebugExtension.Blip(this, "SENDING COMMAND ....", Color.cyan);
            GlitchConnector.instance.SendCommand(new TimestampCommand(0, 0, false));
        }
        if (GUILayout.Button("receive command (from public string)"))
        {
            DebugExtension.Blip(this, "RECEIVING COMMAND ....", Color.magenta);
            GlitchConnector.instance.DebugOnReceiveCommand(new GlitchData("me", SerializedPackage));
        }
    }
}
