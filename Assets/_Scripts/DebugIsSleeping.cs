using UnityEngine;
using System.Collections;

public class DebugIsSleeping : MonoBehaviour {

    public float size;
    private Rigidbody rb;

		void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	void OnDrawGizmos()
    {
        if(rb!=null)
        {
            if(rb.IsSleeping())
            {
                // IS ASLEEP
                Gizmos.color = Color.yellow.SetA(0.5f);
            }
            else
            {
                // IS AWAKE
                Gizmos.color = Color.blue.SetA(0.5f);
            }
            Gizmos.DrawSphere(transform.position, size);
        }
    }

}
