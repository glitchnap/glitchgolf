﻿using UnityEngine;
using System.Collections;

public class RunAlongPath : MonoBehaviour
{
    public BezierCurve curve;
    public float speed;
    private float t;

    void Update()
    {
        t += Time.deltaTime;
        transform.position = curve.GetPointAtDistance(t * speed);

        var lookatTarget = curve.GetPointAtDistance((t + Time.deltaTime) * speed);
        transform.LookAt(new Vector3(lookatTarget.x, transform.position.y, lookatTarget.z));
    }
}
