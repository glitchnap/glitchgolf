﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Star : MonoBehaviour {

    public bool pickedUp;
    private Vector3 startScale;

    void Start()
    {
        startScale = transform.localScale;
        pickedUp = false; 
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag=="Ball")
        {
            Pickup();
            ScoreManager.instance.PropagateUpdateScore();
        }
    }

    public void Spawn()
    {
        pickedUp = false;
        gameObject.SetActive(true);
        HOTween.To(transform, 0.5f, new TweenParms()
            .Prop("localScale",startScale)
        );
        
    }

    public void Pickup()
    {
        if (!pickedUp)
        {
            pickedUp = true;
            HOTween.To(transform, 0.5f, new TweenParms()
                .Prop("localScale", startScale * 0.01f)
                .OnComplete(() => { gameObject.SetActive(false); })
            );
            FunFeedbackManager.instance.ShowStar();
        }
    }
}
