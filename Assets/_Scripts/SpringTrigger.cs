﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpringTrigger : MonoBehaviour
{
    public Transform movingElement;
    public float force;



    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Ball")
        {
            //Debug.Break();
            var rb = col.GetComponent<Rigidbody>();
            rb.AddForce(0, 0.2f * force, force);
            movingElement.DOLocalMoveZ(0.8f, 0.3f).SetEase(Ease.OutExpo).SetLoops(2, LoopType.Yoyo);
            //movingElement.AddForce(force, movingElement.transform.position.addZ(-0.5f), 1f);
        }
    }
}
