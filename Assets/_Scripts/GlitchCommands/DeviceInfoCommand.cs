using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace GlitchConnect.Commands
{
    public class DeviceInfoCommand : IGlitchCommand
    {
        [Serializable]
        public struct DeviceInfoContainer
        {
            public string deviceID;
            public string deviceType;
            public int deviceScreenWidth;
            public int deviceScreenHeight;
            public float deviceScreenSize;
            public RuntimePlatform deviceOS;
            #if UNITY_IOS
            public UnityEngine.iOS.DeviceGeneration? deviceTypeiOS;
            #endif
        }

        public static DeviceInfoContainer GetLocalDeviceInfo()
        {
            DeviceInfoContainer container = new DeviceInfoContainer();
            container.deviceID = SystemInfo.deviceUniqueIdentifier;
            if (Application.isEditor) container.deviceID += UnityEngine.Random.Range(0, 100);
            container.deviceOS = Application.platform;
            container.deviceType = SystemInfo.deviceModel;
            container.deviceScreenHeight = Screen.height;
            container.deviceScreenWidth = Screen.width;
            container.deviceScreenSize = Mathf.Sqrt(Mathf.Pow(Screen.height, 2) + Mathf.Pow(Screen.width, 2)) / Screen.dpi;

#if UNITY_IOS
            container.deviceTypeiOS = UnityEngine.iOS.Device.generation;
#endif
            return container;
        }

        public DeviceInfoContainer deviceInfo;

        /// <summary>
        /// Default constructor will set current device info automatically
        /// </summary>
        public DeviceInfoCommand()
        {
            deviceInfo = GetLocalDeviceInfo();
        }

        public void Execute()
        {
            ConnectionManager.instance.otherDeviceInfo = deviceInfo;
        }

        public static bool isOtherDeviceBiggest(DeviceInfoContainer otherDeviceInfo)
        {
            var localDeviceInfo = GetLocalDeviceInfo();

            return otherDeviceInfo.deviceScreenSize > localDeviceInfo.deviceScreenSize;
        }
    }
}