using System;

namespace GlitchConnect.Commands
{
    public class SetRoleCommand : IGlitchCommand
    {
        public GlitchGolfRoles role;

        public static Action<GlitchGolfRoles> RoleChanged;

        public SetRoleCommand(GlitchGolfRoles role)
        {
            this.role = role;
        }

        public void Execute()
        {
            if (RoleChanged != null)
            {
                RoleChanged.Invoke(role);
            }
        }
    }
}