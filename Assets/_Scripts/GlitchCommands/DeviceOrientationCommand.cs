using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GlitchConnect.Commands
{
    [Serializable]
    public class DeviceOrientationCommand : IGlitchCommand
    {
        public static Action<bool, Vector3, Vector3> DeviceOrientationReceived;

        [SerializeField]
        private Vector3 o;

        public Vector3 Orientation { get { return o; } set { o = value; } }

        [SerializeField]
        private bool p;

        public bool IsPressed { get { return p; } set { p = value; } }

        [SerializeField]
        private Vector3 a;

        public Vector3 Acceleration { get { return a; } set { a = value; } }

        public DeviceOrientationCommand(bool isPressed, Vector3 orientation)
        {
            Orientation = orientation;
            IsPressed = isPressed;
        }

        public DeviceOrientationCommand(bool isPressed, Vector3 orientation, Vector3 acceleration)
        {
            Orientation = orientation;
            IsPressed = isPressed;
            Acceleration = acceleration;
        }

        public void Execute()
        {
            if (DeviceOrientationReceived != null)
            {
                DeviceOrientationReceived.Invoke(IsPressed, Orientation, Acceleration);
            }
        }
    }
}
