using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GlitchConnect.Commands
{
    public enum NavigationCommands
    {
        StartCalibrate,
        Zoom,
        Stop,
        EndCalibrate,
        CancelCalibrate
    }

    public class NavigationCommand : IGlitchCommand
    {
        public static Action<NavigationCommands, float> OnNavigationCommandReceived;

        public NavigationCommands command;
        public float value;

        public NavigationCommand(NavigationCommands command, float value)
        {
            this.command = command;
            this.value = value;
        }

        public void Execute()
        {
            if (OnNavigationCommandReceived != null)
            {
                OnNavigationCommandReceived.Invoke(command, value);
            }
        }
    }
}