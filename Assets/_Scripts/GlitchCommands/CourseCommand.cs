using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GlitchConnect.Commands
{
    public class CourseCommand : IGlitchCommand
    {
        public enum Actions
        {
            ResetHole,
            ChangeHole,
            ChangeCourse,
            CourseLoaded,
        }

        public static Action ResetHoleReceived;
        public static Action<int> ChangeHoleReceived;
        public static Action<int> ChangeCourseReceived;
        public static Action<int> CourseLoadedReceived;

        public Actions action;
        public int index;

        public CourseCommand(Actions action, int index = -1)
        {
            this.action = action;
            this.index = index;
        }

        public void Execute()
        {
            switch (action)
            {
                case Actions.ResetHole:
                    if (ResetHoleReceived != null)
                    {
                        ResetHoleReceived.Invoke();
                    }
                    break;
                case Actions.ChangeHole:
                    if (ChangeHoleReceived != null)
                    {
                        ChangeHoleReceived.Invoke(index);
                    }
                    break;
                case Actions.ChangeCourse:
                    if (ChangeCourseReceived != null)
                    {
                        ChangeCourseReceived.Invoke(index);
                    }
                    break;
                case Actions.CourseLoaded:
                    if (CourseLoadedReceived != null)
                    {
                        CourseLoadedReceived.Invoke(index);
                    }
                    break;
            }
        }
    }
}