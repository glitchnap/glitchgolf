﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GlitchConnect.Commands
{
    [Serializable]
    public class CompassCalibrationCommand : IGlitchCommand
    {
        [SerializeField]
        private float h;

        public float Heading { get { return h; } set { h = value; } }

        [SerializeField]
        private float a;

        public float Accuracy { get { return a; } set { a = value; } }

        public CompassCalibrationCommand(float heading, float accuracy)
        {
            Heading = heading;
            Accuracy = accuracy;
        }

        public void Execute()
        {
            var localHeading = Input.compass.magneticHeading;
            var compassDiff = Heading - localHeading;

            var currentPutterHeading = GameplayManager.instance.putterGhost.rotation.eulerAngles.y;

            var adjustment = (currentPutterHeading - compassDiff) * Accuracy; //NOTE: should work if Accuracy = 1f, means 100% accurate.
            NavigationManager.instance.Calibrate(adjustment);
        }
    }
}

