using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GlitchConnect.Commands
{
    public class CoursePreviewCommand : IGlitchCommand
    {
        public static Action<int> ChanseCourseReceived;

        public int index;

        public CoursePreviewCommand(int index = -1)
        {
            this.index = index;
        }

        public void Execute()
        {
            if (ChanseCourseReceived != null)
            {
                ChanseCourseReceived.Invoke(index);
            }
        }
    }
}