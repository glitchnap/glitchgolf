using System;

namespace GlitchConnect.Commands
{
    public class CustomizePutterAndBallCommand : IGlitchCommand
    {
        public int ballID;
        public int putterID;

        public static Action<GlitchGolfRoles> PutterAndBallChanged;

        public CustomizePutterAndBallCommand(int ballID, int putterID)
        {
            this.ballID = ballID;
            this.putterID = putterID;
        }

        public void Execute()
        {
            GameplayManager.instance.SetBall(ballID);
            GameplayManager.instance.SetPutter(putterID);
            GameplayManager.instance.ResetPutterAndBall();
        }
    }
}