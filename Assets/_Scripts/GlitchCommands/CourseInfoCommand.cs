using System;

namespace GlitchConnect.Commands
{
    public class CourseInfoCommand : IGlitchCommand
    {
        public static Action<int,int> CommandReceived;

        public int courseID;
        public int holeID;

        public CourseInfoCommand(int courseID, int holeID)
        {
            this.courseID = courseID;
            this.holeID = holeID;

        }

        public void Execute()
        {
            if (CommandReceived != null)
            {
                CommandReceived.Invoke(courseID, holeID);
            }
        }
   }
}