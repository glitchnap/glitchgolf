using System;

namespace GlitchConnect.Commands
{
    public class MenuCommand : IGlitchCommand
    {
        public enum Actions
        {
            Ok,
            Cancel,
            Start,
            GotoInGame,
            GotoMainMenu,
            PlayMenuShow,
            PlayMenuHide,
            PlayMenuToggle,
            FinalScoreShow,
            FinalScoreHide,
            FinalScorePlayAgain,
            FinalScorePlayNext,
            MDTutorialShow,
            MDTutorialHide,
            ToggleShadows
        }

        public static Action StartCommandReceived;
        public static Action OkCommandReceived;
        public static Action CancelCommandReceived;
        public static Action<Actions> CommandReceived;

        public Actions action;

        public MenuCommand(Actions command)
        {
            this.action = command;
        }

        public void Execute()
        {
            if (CommandReceived != null)
            {
                CommandReceived.Invoke(action);
            }

            switch (action)
            {
                case Actions.Start:
                    if (StartCommandReceived != null)
                    {
                        StartCommandReceived.Invoke();
                    }
                    break;
                case Actions.Ok:
                    if (OkCommandReceived != null)
                    {
                        OkCommandReceived.Invoke();
                    }
                    break;
                case Actions.Cancel:
                    if (CancelCommandReceived != null)
                    {
                        CancelCommandReceived.Invoke();
                    }
                    break;
                default:
                    break;
            }

        }
    }
}