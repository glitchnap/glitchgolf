﻿using UnityEngine;
using System.Collections;
using Gvr.Internal;

public class GoogleVRGyroController : MonoBehaviour
{
    // The VR device that will be providing input data.
    private static BaseVRDevice device;

    // Use this for initialization
    void Start()
    {
        device = BaseVRDevice.GetDevice();
        #if UNITY_EDITOR
        if (device is EditorDevice)
        {
            //Use our implementation instead, it works better for the glitchGolf usecase!
            device = new GoogleVREditorDevice();
        }
        #endif
        device.Init();
        device.SetNeckModelScale(0);
        device.SetVRModeEnabled(false);
        device.Recenter();
    }
	
    // Update is called once per frame

    void LateUpdate()
    {
        // there's no more use for local gyro mapping (we're juts using one of the axis)
        if (!RoleManager.instance.IsPutter()) return;

        device.UpdateState();
        var orientation = device.GetHeadPose().Orientation;

        #if UNITY_IOS && !UNITY_EDITOR
        //NOTE: this is test code, to figure out iOS.
        //var attitude = Input.gyro.attitude;
        //attitude = new Quaternion(attitude.x, attitude.y, -attitude.z, -attitude.w);
        //attitude = Quaternion.Euler(90, 0, 0) * attitude;

        //NOTE: A 90 rotation around the z axis needs to be added, on iOS
        orientation = orientation * Quaternion.Euler(0, 0, 90);

        //NOTE: After I figured out that, it was actually just a rotation around the Z axis that I needed, the orientation values were flipped, in my Debug.Log below.
        //NOTE: Interesting fact about quaternions, negating all components, does not change the rotation, so this can be omitted (I tested both with and without).
        //orientation = new Quaternion(-orientation.x, -orientation.y, -orientation.z, -orientation.w);
        //Debug.Log("Attitude vs Orientation: " + attitude + " vs " + orientation);
        #endif

        transform.localRotation = orientation;
    }

    public Quaternion GetDeviceOrientation()
    {
        if (device == null) return Quaternion.identity;
        device.UpdateState();

        var orientation = device.GetHeadPose().Orientation;

        #if UNITY_IOS && !UNITY_EDITOR
        //NOTE: A 90 rotation around the z axis needs to be added, on iOS
        orientation = orientation * Quaternion.Euler(0, 0, 90);
        #endif

        return orientation;
    }

    // Quaternion.EulerAngles.x can be unreliable sometimes for upside down values, so this should fix it
    public float GetSafeEulerX()
    {
        var rot = GetDeviceOrientation();
        var upsidedown = Vector3.Dot(rot * Vector3.up, Vector3.down) > 0;
        return (upsidedown ? 180 - rot.eulerAngles.x : rot.eulerAngles.x);
    }
}
