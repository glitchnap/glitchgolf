using UnityEngine;
using System.Collections;
using Holoville.HOTween;

[RequireComponent(typeof(Rigidbody))]
public class BumperObstacle : MonoBehaviour
{

    private Vector3 startpos;
    public Vector3 direction;
    public float force = 100;
    public float maxTimer = 1;

    private float timer;

    public bool useCollisionDirection;
    public bool useCollisionNormal;


    void Start()
    {
        startpos = transform.position;
        timer = 0;
    }

    void FixedUpdate()
    {
        if (timer > 0.9f * maxTimer)
        {
            var amount = 1 - ((timer - 0.9f * maxTimer) / 0.1f);
            GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(startpos, startpos - ((transform.rotation * direction).normalized * force), amount));
        }
        else if (timer > 0)
        {
            var amount = 1 - ((timer - 0.1f * maxTimer) / 0.9f);
            GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(transform.position, startpos, amount));
        }
        timer -= Time.fixedDeltaTime;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Ball")
        {
            if (useCollisionDirection)
            {
                direction = startpos - col.collider.transform.position;
            }
            if (useCollisionNormal)
            {
                direction = col.contacts[0].normal;
            }
            timer = maxTimer;   
        }
    }
}
