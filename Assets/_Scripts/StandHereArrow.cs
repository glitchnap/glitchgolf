﻿using UnityEngine;
using System.Collections;

public class StandHereArrow : MonoBehaviour
{
    public Transform ballPosition;

    public Vector3 directionRotation;

    private bool isShowing;

    void Start()
    {
        isShowing = true;
        Hide();
    }

    void LateUpdate()
    {
        if (ballPosition == null || !ballPosition.GetComponent<Rigidbody>().IsSleeping())
        {
            Hide();
            return;
        }
        
        if (!RoleManager.IsInstantiated() || !RoleManager.instance.IsWorld())
        {
            Hide();
            return;
        }

        if (!CourseManager.IsInstantiated() || CourseManager.instance.CurrentCourse == null)
        {
            Hide();
            return;
        }

        if (!ScoreManager.IsInstantiated() || ScoreManager.instance.strokeCounter > 0)
        {
            Hide();
            return;
        }

        Show();

        var hole = CourseManager.instance.GetCurrentHole();

        transform.position = ballPosition.position;
        transform.rotation = Quaternion.Euler(directionRotation) * Quaternion.LookRotation(hole.StartPosition.forward, Vector3.up);
    }

    private void Show()
    {
        if (isShowing) return;

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }

        isShowing = true;
    }

    private void Hide()
    {
        if (!isShowing) return;

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }

        isShowing = false;
    }
}
