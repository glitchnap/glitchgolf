using UnityEngine;
using System.Collections;

public class ConnectingDotsLoop : MonoBehaviour {
    public int maxDots;
    public float maxTime;

    private float counter;
    private int dots;
    private bool on;
    private TMPro.TextMeshProUGUI text;

	void Start () {
        dots = maxDots;
        counter = maxTime;
        text = GetComponent<TMPro.TextMeshProUGUI>();
	}
	
	void Update () 
    {
        if(on)
        {
            counter -= Time.deltaTime;
            if(counter <= 0)
            {
                counter = maxTime;
                dots--;
                if(dots < 0)
                {
                    dots = maxDots;
                }

                var s = "";
                for (int i = 0; i < maxDots-dots; i++)
                {
                    s += ".";
                }
                text.text = s;
            }
        }
	}

    public void Toggle(bool on)
    {
        this.on = on;
        if(on)
        {
            dots = maxDots;
            counter = maxTime;
        }
    }
}
