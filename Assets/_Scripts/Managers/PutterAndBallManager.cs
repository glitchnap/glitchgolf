using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using GlitchConnect.Commands;

public enum BallTypes
{
    Standard,
    Bouncy,
    Sticky,
    Heavy,
    Light,
    Cube
}

[System.Serializable]
public struct SharedBallSettings
{
    public bool useFakeSleep;
    public float fakeSleepThreshold;
    public float fakeVelocityReductionRate;
    public float fakeVelocityCutoff;
    public bool limitVelocity;
    public float maxVelocity;
    public bool limitAngularVelocity;
    public float maxAngularVelocity;
    public bool useHoleMagnetism;
    public float magnetismRadius;
    public float magnetismAmount;
    public float magnetismApproachAngle;
    public float magnetismMaxTime;
}

[System.Serializable]
public class BallTypeSettings
{
    public BallTypes type;
    [Range(0.01f, 10)]
    public float mass;
    [Range(0, 10)]
    public float drag;
    [Range(0, 10)]
    public float angularDrag;
    public float staticFriction;
    public float dynamicFriction;
    [Range(0, 1)]
    public float bounciness;
    public PhysicMaterialCombine frictionCombine;
    public PhysicMaterialCombine bounceCombine;
    public Vector3 customGravity;
}

public class PutterAndBallManager : MonoSingleton<PutterAndBallManager>
{
    public GameObject[] putters;
    public GameObject[] balls;
    public SharedBallSettings sharedBallSettings;
    public List<BallTypeSettings> ballPhysicsSettings;

    public float lerpSpeed;
    public Camera BallCamera;
    public Transform BallSpawn;
    public int BallID;
    public float BallSpacing;
    public Camera PutterCamera;
    public Transform PutterSpawn;
    public Vector2 PutterCameraOffset;
    public int PutterID;
    public float PutterSpacing;

    public Material ballOcclusionMaterial;

    void Awake()
    {
        instance = this;
    }

    void Start()
    { 
        for (int i = 0; i < putters.Length; i++)
        {
            var go = Instantiate(putters[i], PutterSpawn) as GameObject;
            go.transform.localPosition = Vector3.right * PutterSpacing * i;
            go.SetLayer(go.transform.parent.gameObject.layer);

            go.transform.localRotation = Quaternion.Euler(GameplayManager.instance.putterTilt, 90, 0);

            Destroy(go.GetComponentInChildren<Putter>());
            Destroy(go.GetComponentInChildren<Rigidbody>());
            foreach (var item in go.GetComponentsInChildren<Collider>())
            {
                Destroy(item);
            }
            
        }

        for (int i = 0; i < balls.Length; i++)
        {
            var go = Instantiate(balls[i], BallSpawn) as GameObject;
            go.transform.localPosition = Vector3.right * BallSpacing * i;
            go.SetLayer(go.transform.parent.gameObject.layer);

            Destroy(go.GetComponentInChildren<BallSettings>());
            Destroy(go.GetComponentInChildren<Ball>());
            Destroy(go.GetComponentInChildren<BallRollingPhysics>());
            Destroy(go.GetComponentInChildren<Rigidbody>());
            foreach (var item in go.GetComponentsInChildren<Collider>())
            {
                Destroy(item);
            }
        }
    }

    void Update()
    {
        BallCamera.transform.localPosition = Vector3.Lerp(BallCamera.transform.localPosition, BallCamera.transform.localPosition.withX(BallSpacing * BallID), lerpSpeed * Time.deltaTime);
        PutterCamera.transform.localPosition = Vector3.Lerp(
            PutterCamera.transform.localPosition, 
            PutterCamera.transform.localPosition
                .withX(PutterSpacing * PutterID + PutterCameraOffset.x)
                .withY(PutterCameraOffset.y), lerpSpeed * Time.deltaTime);
    }

    public void GoToNextBall()
    {
        BallID++;
        if (BallID >= balls.Length) BallID = balls.Length - 1;

    }

    public void GoToPrevBall()
    {
        BallID--;
        if (BallID < 0) BallID = 0;
    }

    public void GoToNextPutter()
    {
        PutterID++;
        if (PutterID >= putters.Length) PutterID = putters.Length - 1;
    }

    public void GoToPrevPutter()
    {
        PutterID--;
        if (PutterID < 0) PutterID = 0;
    }

    public GameObject GetCurrentPutter()
    {
        return putters[PutterID];
    }

    public GameObject GetCurrentBall()
    {
        return balls[BallID];
    }

}