using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.StateMachine;
using System;
using System.Linq;
using GlitchConnect.Commands;
using UnityEngine.SceneManagement;

public class CourseManager : MonoSingleton<CourseManager>
{
    public int CurrentHoleIndex;
    public GolfCourse CurrentCourse;
    public int CurrentCourseIndex;
    private string currentCourseScene;
    private int currentCourseLength;

    public GolfLevelPack[] Courses;

    public static Action<int> OnHoleChanged;
    public static Action<int> OnHoleReset;
    public static Action OnCourseChanged;

    public bool DebugAutoStartSD = true;

    void Awake()
    {
        instance = this;
        CourseCommand.ResetHoleReceived += Reset;
        CourseCommand.ChangeCourseReceived += ChangeCourse;
        CourseCommand.ChangeHoleReceived += ChangeHole;
        CourseCommand.CourseLoadedReceived += OnCourseLoaded;

    }

    void Start()
    {
        CurrentHoleIndex = 0;
        
        // Detect other course scenes
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            var scene = SceneManager.GetSceneAt(i);
            if (scene.path.Contains("Courses"))
            {

#if UNITY_EDITOR
                // Start single device with the course scene that was still open...
                // For debugging only, skips all UI...
                var course = Courses[0];
                foreach (var item in Courses)
                {
                    if(item.sceneName == scene.name)
                    {
                        course = item;
                    }
                }

                SceneManager.UnloadScene(scene);
                Debug.Log("Unloaded: " + scene.name);

                if (DebugAutoStartSD)
                {

                    ConnectionManager.instance.UseUNet();
                    ConnectionManager.instance.StartConnecting();
                    Timer.DelayedExecute(ConnectionManager.instance.StartSingleDevice, 0.1f);
                    Timer.DelayedExecute(ConnectionManager.instance.PlaySingle, 0.2f);

                    Timer.DelayedExecute(() =>
                    {
                        ChangeCourse(Courses.ToList().IndexOf(course));
                        GlobalUIManager.instance.GotoInGame();
                    }, 0.3f);

                    Debug.Log("Starting game cause a course was open...");

                    //GlobalUIManager.instance.GotoInGame();
                    return;
                }
#else
                Debug.Log("Unloaded: " + scene.name);
                SceneManager.UnloadScene(scene);
#endif
            }
        }
    }
     
    private void OnCourseLoaded(int numberOfHoles)
    {
        currentCourseLength = numberOfHoles;
    }

    public void ChangeCourse(int index)
    {
        if (RoleManager.instance.IsPutter())
        {
            ConnectionManager.instance.SendCommand(new CourseCommand(CourseCommand.Actions.ChangeCourse, index));
        }

        CurrentCourseIndex = index;
        ChangeCourse(Courses[index]);
    }

    private void ChangeCourse(GolfLevelPack course)
    {
        UnloadCurrentCourse();

        StartCoroutine(LoadCourse(course));
    }

    public void UnloadCurrentCourse()
    {
        if (!String.IsNullOrEmpty(currentCourseScene))
        {
            SceneManager.UnloadScene(currentCourseScene);   
        }

        CurrentCourse = null;
        currentCourseScene = "";
        currentCourseLength = 0;
    }

    private IEnumerator LoadCourse(GolfLevelPack levelPack)
    {
        currentCourseScene = levelPack.sceneName;

        if (!RoleManager.instance.IsPutter())
        {
            DebugExtension.Blip("Begin async loading scene");
            var asy = SceneManager.LoadSceneAsync(currentCourseScene, LoadSceneMode.Additive);
            asy.allowSceneActivation = true;

            yield return new WaitUntil(() => asy.isDone);
            yield return new WaitForEndOfFrame();

            var scene = SceneManager.GetSceneByName(currentCourseScene);

            DebugExtension.Blip("Begin loading course");
            for (int i = 0; i < scene.rootCount; i++)
            {
                CurrentCourse = scene.GetRootGameObjects()[i].GetComponent<GolfCourse>();
                if (CurrentCourse != null) break;
            }

            if (CurrentCourse == null)
                DebugExtension.Blip("Couldn't load course :(");
            else
                DebugExtension.Blip("Loaded course: " + CurrentCourse.name);
            currentCourseLength = CurrentCourse.Holes.Length;

            if (!RoleManager.instance.IsSingleDevice())
            {
                ConnectionManager.instance.SendCommand(new CourseCommand(CourseCommand.Actions.CourseLoaded, currentCourseLength));
            }
        }
       
        if (OnCourseChanged != null)
        {
            OnCourseChanged.Invoke();
        }

        CurrentHoleIndex = 0;
        PushHoleChanged();
    }

    public void GoToNextLevel()
    {
        var next = CurrentHoleIndex + 1;
        if (next >= currentCourseLength)
        {
            next = 0;
        }

        ChangeHole(next);
    }

    public void GoToPreviousLevel()
    {
        var prev = CurrentHoleIndex - 1;
        if (prev < 0)
        {
            prev = currentCourseLength - 1;
        }
            
        ChangeHole(prev);
    }

    public void ChangeHole(int index)
    {
        var LastHole = CurrentHoleIndex;
        CurrentHoleIndex = index;

        if (RoleManager.instance.IsPutter())
        {
            ConnectionManager.instance.SendCommand(new CourseCommand(CourseCommand.Actions.ChangeHole, index));
        }

        if (LastHole == index)
        {
            PushHoleReset();
        }
        else
        {
            PushHoleChanged();
        }
    }

    public GolfHole GetCurrentHole()
    {
        if (CurrentCourse != null)
        {
            if (CurrentHoleIndex >= CurrentCourse.Holes.Length) CurrentHoleIndex = 0;
            return CurrentCourse.Holes[CurrentHoleIndex];    
        }

        Debug.LogError("Failed to get current Hole, because CurrentCourse wasn't set!", gameObject);
        return null;
    }

    public void Reset()
    {
        if (RoleManager.instance.IsPutter())
        {
            ConnectionManager.instance.SendCommand(new CourseCommand(CourseCommand.Actions.ResetHole));
        }
            
        ChangeHole(CurrentHoleIndex);
    }

    private void PushHoleChanged()
    {
        if (OnHoleChanged != null)
        {
            OnHoleChanged.Invoke(CurrentHoleIndex);
        }
    }

    private void PushHoleReset()
    {
        if (OnHoleReset!= null)
        {
            OnHoleReset.Invoke(CurrentHoleIndex);
        }
    }

    [ContextMenu("Sort these")]
    public void Sort()
    {
        var list = Courses.ToList<GolfLevelPack>();
        list.Sort((a, b) => a.sortOrder.CompareTo(b.sortOrder));
        Courses = list.ToArray();
    }

    public int GetStarCountForHole(int holeIndex)
    {
        var stars = CurrentCourse.Holes[holeIndex].Stars;
        int count = 0;
        for (int i = 0; i < stars.Length; i++)
        {
            if (stars[i].pickedUp) count++;
        }
        return count;
        
    }
}
