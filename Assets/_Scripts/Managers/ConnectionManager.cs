using UnityEngine;
using System.Collections;
using MonsterLove.StateMachine;
using GlitchConnect;
using GlitchConnect.Commands;

public class ConnectionManager : MonoSingleton<ConnectionManager>
{

    public const int ConnectionsRequired = 1;
    public const string AppName = "GlitchGolf";

    public float RecommendedSendRate
    {
        get
        {
            return GlitchConnector.instance.RecommendedSendRate;
        }
    }

    public float RecommendedSendIntervalsInSeconds
    {
        get
        {
            if (GlitchConnector.instance.RecommendedSendRate > 0)
            {
                return 1f / GlitchConnector.instance.RecommendedSendRate;    
            }
            else
            {
                return 1f;
            }
        }
    }

    public enum States
    {
        Idle,
        WaitForConnection,
        WaitForRole,
        WaitForStartPlaying,
        Playing
    }

    public StateMachine<States> fsm;
    public DeviceInfoCommand.DeviceInfoContainer otherDeviceInfo;

    private GlitchConnector.ConnectorTypes ConnectorType = GlitchConnector.ConnectorTypes.GlitchUNet;

    private Coroutine rotationLoop;

    [ReadOnly]
    public States currentState;

    void Awake()
    {
        instance = this;
        fsm = StateMachine<States>.Initialize(this, States.Idle);
    }

    void Start()
    {
        GlitchConnector.instance.OnConnectionChanged += OnConnectionChanged;

        SetRoleCommand.RoleChanged += OnRoleChanged;
        MenuCommand.StartCommandReceived += OnStartCommandReceived;
    }

    new void OnDestroy()
    {
        if (GlitchConnector.IsInstantiated())
        {
            GlitchConnector.instance.OnConnectionChanged -= OnConnectionChanged;
        }

        SetRoleCommand.RoleChanged -= OnRoleChanged;
        MenuCommand.StartCommandReceived -= OnStartCommandReceived;
        base.OnDestroy();
    }

    void Update()
    {
        currentState = fsm.State;
    }

    #region FSM

    public void WaitForRole_Enter()
    {
        GlitchConnector.instance.SendCommand(new DeviceInfoCommand());
    }

    public void WaitForRole_Update()
    {
        if (otherDeviceInfo.deviceID != null)
        {
            // if I have the alphabetically larger UDID, I pick!
            if (otherDeviceInfo.deviceID.CompareTo(DeviceInfoCommand.GetLocalDeviceInfo().deviceID) > 0)
            {
                DecideRole();
            }
        }
    }

    #region FSM Helpers

    private void DecideRole()
    {
        GlitchDebug.Log("I AM DECIDING ROLES !");

        if (DeviceInfoCommand.isOtherDeviceBiggest(otherDeviceInfo))
        {
            RoleManager.instance.SetRole(GlitchGolfRoles.putter);
            GlitchConnector.instance.SendCommand(new SetRoleCommand(GlitchGolfRoles.world));
        }
        else
        {
            RoleManager.instance.SetRole(GlitchGolfRoles.world);
            GlitchConnector.instance.SendCommand(new SetRoleCommand(GlitchGolfRoles.putter));
        }

        fsm.ChangeState(States.WaitForStartPlaying);
    }

    #endregion

    #endregion


    #region Notification Handlers

    private void OnStartCommandReceived()
    {
        if (fsm.State == States.WaitForStartPlaying)
        {
            fsm.ChangeState(States.Playing);
        }
    }

    private void OnRoleChanged(GlitchGolfRoles role)
    {
        if (fsm.State == States.WaitForRole)
        {
            fsm.ChangeState(States.WaitForStartPlaying);
        }
    }

    private void OnConnectionChanged(GlitchConnectionEvent obj)
    {
        if (obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.Connected)
        {
            fsm.ChangeState(States.WaitForRole);
        }
        else if (obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost || obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.PeerDisconnected)
        {
            GlitchDebug.Log("Connection lost, opening main menu again", GlitchDebug.IMPORTANT);
            if (rotationLoop != null)
            {
                StopCoroutine(rotationLoop);
            }

            GlitchConnector.instance.StopTimeStampLoop();
            if (obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.PeerDisconnected)
            {
                GlitchConnector.instance.Disconnect();
            }
            fsm.ChangeState(States.Idle);
        }
    }

    #endregion

    #region public menu helpers

    public void UseMultipeer()
    {
        ConnectorType = GlitchConnector.ConnectorTypes.GlitchMultipeer;
    }

    public void UseUNet()
    {
        ConnectorType = GlitchConnector.ConnectorTypes.GlitchUNet;
    }

    public void StartConnecting()
    {
        fsm.ChangeState(States.WaitForConnection);
        GlitchConnector.instance.Disconnect();
        GlitchConnector.instance.StartConnecting(ConnectorType, ConnectionsRequired, AppName);
    }

    public void Disconnect()
    {
        GlitchConnector.instance.Disconnect();
        RoleManager.instance.SetRole(GlitchGolfRoles.none);
        fsm.ChangeState(States.Idle);
    }

    public void SwapRoles()
    {
        RoleManager.instance.SwapRoles();
    }

    public void Play()
    {
        GlitchConnector.instance.SendCommand(new MenuCommand(MenuCommand.Actions.Start));
        fsm.ChangeState(States.Playing);
    }

    public void PlaySingle()
    {
        fsm.ChangeState(States.Playing);
    }

    public void StartSingleDevice()
    {
        GlitchConnector.instance.Disconnect();
        RoleManager.instance.SetRole(GlitchGolfRoles.singleDevice);
        fsm.ChangeState(States.WaitForStartPlaying);
        /*Disconnect();
        
        Play();*/
    }

    #region private menu helpers

    #endregion

    #endregion

    #region Public connection methods

    public void SendCommand(IGlitchCommand command, bool unreliably = false)
    {
        if (unreliably)
        {
            GlitchConnector.instance.SendCommandUnreliably(command);
        }
        else
        {
            GlitchConnector.instance.SendCommand(command);
        }
    }

    public void StartTimeStampLoop()
    {
        GlitchConnector.instance.StartTimeStampLoop();
    }

    public void StopTimeStampLoop()
    {
        GlitchConnector.instance.StopTimeStampLoop();
    }

    public void StartRotationLoop()
    {
        rotationLoop = StartCoroutine(SendRotationLoop());
    }

    public void StopRotationLoop()
    {
        if (rotationLoop != null)
        {
            StopCoroutine(rotationLoop);
        }
    }

    public IEnumerator SendRotationLoop()
    {
        while (true)
        {
            if (fsm.State == States.Playing)
            {
                GlitchConnector.instance.SendCommandUnreliably(
                    new DeviceOrientationCommand(MDInputManager.instance.isPressing, GameplayManager.instance.GetPutterAngle(), Input.gyro.userAcceleration));
            }
            yield return new WaitForSeconds(RecommendedSendIntervalsInSeconds);
        }
    }

    #endregion

    #region debug

    void OnGUI()
    {
        /*
        if (Application.isEditor)
        {
            switch (fsm.State)
            {
                case States.Idle:
                    break;
                case States.WaitForConnection:
                    if (GUILayout.Button("Fake Connect"))
                    {
                        fsm.ChangeState(States.WaitForRole);
                    }
                    break;
                case States.WaitForRole:

                    if (GUILayout.Button("Fake Get Role"))
                    {
                        DebugExtension.BlipError(this, "NEEDS TO BE REIMPLEMENTED");
                        DecideRole();
                    }
                    break;
                case States.WaitForStartPlaying:
                    if (GUILayout.Button("Swap Role"))
                    {
                        SwapRoles();
                    }
                    if (GUILayout.Button("Disconnect"))
                    {
                        fsm.ChangeState(States.WaitForConnection);
                    }
                    break;
                case States.Playing:
                    break;
                default:
                    break;
            }
        }
        */
    }

    #endregion
}
