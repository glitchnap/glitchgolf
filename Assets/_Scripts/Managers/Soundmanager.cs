using UnityEngine;
using System.Collections;

public class Soundmanager : MonoSingleton<Soundmanager>
{ 
    public AudioClip ballHitClub;
    public AudioClip ballHitWall;
    public AudioClip ballHitGoal;

    private AudioSource source;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void Update()
    {
	
    }

    public void Play(AudioClip clip)
    {
        source.PlayOneShot(clip);
    }

    public void PlayOnBall(AudioClip clip)
    {
        GameplayManager.instance.ball.GetComponent<AudioSource>().PlayOneShot(clip);
    }
}
