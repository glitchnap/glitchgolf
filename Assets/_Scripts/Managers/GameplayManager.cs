using UnityEngine;
using System.Collections;
using MonsterLove.StateMachine;
using System;
using GlitchConnect.Commands;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameplayManager : MonoSingleton<GameplayManager>
{
    [InspectorNote("Global Settings", NoteColor.pink)]
    public bool _global;
    public float lerpSpeed;
    public float lerpSpeedInCalibration;
    public float minPutterDistanceToAllowEnable;
    public float outOfBoundsTimeOut = 1;
    public float fallingTimeOut = 2;

    [InspectorNote("Runtime Settings", NoteColor.pink)]
    public bool _runtime;
    public bool clubEnabled;
    public float putterTilt = 100;
    public float putterDistanceFromBall = 10;
    public Vector3 ballLastPos;
    public float ballLastAngle;
    public bool ballLastSet;

    [InspectorNote("References", NoteColor.pink)]
    public bool _references;

    public Ball ball;
    public Putter putter;
    public Transform putterGhost;
    public Transform putterGhostCalibrate;
    public GameObject PutterModelParent;
    public SimpleConstraints ballArrow;

    // events
    public Action onHoleChangedEvent;
    public Action onBallHitEvent;
    public Action onBallOutOfBoundsEvent;
    public Action onHoleResetEvent;
    public Action onHoleReachedEvent;
    public Action onFinalHoleReachedEvent;
    public Action<string> onWarningMessage;

    // private stuff
    private Tweener clubGhostRotationTween;
    private Coroutine hitGoalDelay;
    [ReadOnly]
    [SerializeField]
    private bool goalWasHit = false;
    [ReadOnly]
    [SerializeField]
    private bool isOutOfBounds = false;
    [ReadOnly]
    [SerializeField]
    private float outOfBoundsTimer;
    [ReadOnly]
    [SerializeField]
    private float fallingTimer;

    bool hasSnappedBall;
    public bool hasFinishedMovingPutter;
    public bool isPlaying;

    void Awake()
    {
        instance = this;

        // record the initial offset, so that you can layout the club-to-ball distance in the editor
        
    }

    void Start()
    {
        clubEnabled = true;
        DisableClub();
        SnapClubToBall();

        CourseManager.OnHoleChanged += OnHoleChanged;
        CourseManager.OnHoleReset += OnHoleReset;
        CourseManager.OnCourseChanged += OnCourseChanged;

        DeviceOrientationCommand.DeviceOrientationReceived += OnDeviceOrientationReceived;
    }

    public void StartGame()
    {
        isPlaying = true;
    }

    public void OnCourseChanged()
    {
        ScoreManager.instance.DoReset();
        GolfCameraFollow.instance.SetupCameraZoomIn();
    }

    public void StopGame()
    {
        isPlaying = false;
    }

    void Update()
    {


    }

    void FixedUpdate()
    {
        if (!isPlaying) return;
        // Check if out of bounds too long
        isOutOfBounds = ball.IsOnlyTouchingBottomFloor();

        if (isOutOfBounds)
        {
            outOfBoundsTimer += Time.fixedDeltaTime;
            if (outOfBoundsTimer > outOfBoundsTimeOut)
            {
                SetBallToLast();
                if (onBallOutOfBoundsEvent != null) onBallOutOfBoundsEvent.Invoke();
                fallingTimer = 0;
                outOfBoundsTimer = 0;
            }
            else if (outOfBoundsTimer > outOfBoundsTimeOut / 2f)
            {
                FunFeedbackManager.instance.ShowOutOfBounds();
            }
        }
        else if (ball.IsFalling())
        {
            fallingTimer += Time.fixedDeltaTime;
            if (fallingTimer > fallingTimeOut)
            {
                SetBallToLast();
                if (onBallOutOfBoundsEvent != null) onBallOutOfBoundsEvent.Invoke();
                fallingTimer = 0;
                outOfBoundsTimer = 0;
            }
            else if (fallingTimer > fallingTimeOut / 2f)
            {
                //ShowWarning("Out of bounds");
                FunFeedbackManager.instance.ShowOutOfBounds();
            }
        }
        else
        {
            fallingTimer = 0;
            outOfBoundsTimer = 0;
        }
        
        // When ball falls asleep...
        if (ball.IsSleeping() && !ball.WasSleeping())
        {
            // don't do anything if we hit the goal...
            if (!goalWasHit)
            {
                // figure out of we're at max strokes
                if (ScoreManager.instance.isCurrentlyLastStroke())
                {
                    BallHitGoalOrMaxStrokes();
                }
                else
                {
                    // Snap to ball
                    if (!hasSnappedBall)
                    {
                        FixClubAimDirection(GolfCameraFollow.instance.lastAutoRotationAngle);
                        SnapClubToBall();
                        hasSnappedBall = true;
                    }
                }
            }
        }
        else
        {
            // we're not asleep so just reset some values
            if (!ball.IsSleeping())
            {
                hasFinishedMovingPutter = false;
            }
            hasSnappedBall = false;
        }


        // Have local Golf club follow the rotation of the client
        if (RoleManager.instance.IsWorld() || RoleManager.instance.IsSingleDevice())
        {
            // Don't use LOCALROTATION here, because we want to actual rotation, including the Calibrate ghost rotation
            var destination = Quaternion.Euler(putterGhost.rotation.eulerAngles);
            if (RoleManager.instance.IsWorld() && MDInputManager.instance.isCalibrating)
                putter.DoMoveRotation(destination, lerpSpeedInCalibration);
            else
                putter.DoMoveRotation(destination, lerpSpeed);
            // rigid.MovePosition(putter.localPosition + (putterGhost.localPosition * ConnectionManager.instance.RecommendedSendIntervalsInSeconds) * Time.fixedDeltaTime);
        }
    }

    public void SnapClubToBall(bool instant = false)
    {
        putter.DoMovePosition(ball.transform.position, instant);
    }

    public void SetBallToStart()
    {
        ballLastSet = false;
        if (RoleManager.instance.IsSingleDevice())
        {
            SDInputManager.instance.CancelSwing();
        }
        var startPos = CourseManager.instance.GetCurrentHole().StartPosition;
        ball.Reset();
        ball.transform.position = startPos.position.addY(ball.radius);
        SnapClubToBall();
        // place the putter a small distance away from the ball
        if (RoleManager.instance.IsSingleDevice())
        {
            FixClubAimDirection(startPos.eulerAngles.y);
            FixClubTilt();
        }
    }

    public void SetBallToLast()
    {
        if (!ballLastSet)
        {
            SetBallToStart();
            return;
        }
        DisableClub();
        if (RoleManager.instance.IsSingleDevice())
        {
            SDInputManager.instance.CancelSwing();
        }

        var startPos = ballLastPos;
        var startRot = ballLastAngle - 90;
        //var startPos = ballLastPos;

        ball.Reset();
        ball.transform.position = startPos;
        SnapClubToBall();

        // place the putter a small distance away from the ball
        if (RoleManager.instance.IsSingleDevice())
        {
            FixClubAimDirection(startRot);
            FixClubTilt();
        }
    }


    public void FixClubAimDirection(float angle)
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            GolfCameraFollow.instance.lastAutoRotationAngle = angle;

            if (SDInputManager.instance.leftHanded)
            {
                SDInputManager.instance.gyroOffset = -GyroManager.instance.gyroController.GetDeviceOrientation().eulerAngles.y - 90;
                SDInputManager.instance.SetAimRotation(angle - 90);
            }
            else
            {
                SDInputManager.instance.gyroOffset = -GyroManager.instance.gyroController.GetDeviceOrientation().eulerAngles.y + 90;
                SDInputManager.instance.SetAimRotation(angle + 90);
            }
        }
    }

    public void FixClubTilt()
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            if (SDInputManager.instance.leftHanded)
            {
                putterGhost.localRotation = Quaternion.Euler(0, 0, -putterDistanceFromBall) * Quaternion.Euler(putterTilt, 0, 0);
            }
            else
            {
                putterGhost.localRotation = Quaternion.Euler(0, 0, putterDistanceFromBall) * Quaternion.Euler(putterTilt, 0, 0);    
            }
        }
    }


    public void UpdateClubEnabled(bool isPressed)
    {
        if (isPressed)
        {
            EnableClub();
        }
        else
        {
            DisableClub();
        }
    }

    public void EnableClub(bool forceEnable = false)
    {
        GolfCameraFollow.instance.DisableFlyOver();
        GlobalUIManager.instance.inGameMenu.FlyoverHide();

        if (!clubEnabled || forceEnable)
        {
            bool tooclose = (putter.footCollider.transform.position - ball.transform.position).magnitude <= minPutterDistanceToAllowEnable;

            if (!tooclose || forceEnable)
            {
                clubEnabled = true;

                PutterModelParent.GetComponentInParent<Rigidbody>().WakeUp();
                putter.VisuallyEnable();

                //                foreach (var item in PutterEnabled.GetComponentsInChildren<Collider>())
                //                {
                //                    item.isTrigger = false;
                //                }
                putter.footCollider.isTrigger = false;
            }
            else if (tooclose)
            {
                ShowWarning("Too close to ball");
            }
        }
    }

    public void DisableClub()
    {
        if (clubEnabled)
        {
            clubEnabled = false;
            //PutterEnabled.SetActive(false);
            
            putter.VisuallyDisable();

            //            foreach (var item in PutterEnabled.GetComponentsInChildren<Collider>())
            //            {
            //                item.isTrigger = true;
            //            }
            putter.footCollider.isTrigger = true;
        }
    }

    public void SnapClubToGhostInstant()
    {
        putter.DoMoveRotation(putterGhost.rotation, 1);
    }

    public void OnHoleChanged(int index)
    {
        isOutOfBounds = false;
        outOfBoundsTimer = 0;
        fallingTimer = 0;
        goalWasHit = false;

        if (onHoleChangedEvent != null) onHoleChangedEvent.Invoke();
        if (!RoleManager.instance.IsPutter())
        {
            ResetPutterAndBall();
            SetBallToStart();
            SnapClubToBall();
            if (!RoleManager.instance.IsSingleDevice())
            {
                GolfCameraFollow.instance.EnableFlyOver();
                GlobalUIManager.instance.inGameMenu.FlyoverShow(); 
            }
        }
    }

    public void OnHoleReset(int index)
    {
        isOutOfBounds = false;
        outOfBoundsTimer = 0;
        fallingTimer = 0;
        goalWasHit = false;

        if (onHoleResetEvent != null) onHoleResetEvent.Invoke();
        if (!RoleManager.instance.IsPutter())
        {
            ResetPutterAndBall();
            SetBallToStart();
            SnapClubToBall();
        }
    }


    private void OnDeviceOrientationReceived(bool isPressed, Vector3 orientation, Vector3 acceleration)
    { 
        if (!isPlaying) return;

        if (RoleManager.instance.IsSingleDevice() || !MDInputManager.instance.isCalibrating)
        {
            UpdateClubEnabled(isPressed);
        }

        UpdateClubRotation(Quaternion.Euler(orientation));
    }

    public void UpdateClubRotation(Quaternion newRotation)
    {
        //NOTE: Maybe do a lerp using the 1f/recommendedSendRate as the amount of time it should take...
        //putterGhost.localRotation = newRotation;

        if (clubGhostRotationTween != null)
        {
            clubGhostRotationTween.Kill();
        }

        clubGhostRotationTween = putterGhost.DOLocalRotate(newRotation.eulerAngles, ConnectionManager.instance.RecommendedSendIntervalsInSeconds * 4f);
    }


    public void BallHitGoal(Collider collider)
    {
        if (ScoreManager.instance.strokeCounter == 1)
        {
            FunFeedbackManager.instance.ShowHoleInOne();
        }
        else if (ScoreManager.instance.strokeCounter == CourseManager.instance.GetCurrentHole().par)
        {
            FunFeedbackManager.instance.ShowPar();
        }
        else
        {
            FunFeedbackManager.instance.ShowGeneric(ScoreManager.instance.strokeCounter, CourseManager.instance.GetCurrentHole().par);
        }

        goalWasHit = true;
        if (hitGoalDelay != null)
        {
            Timer.KillTimer(hitGoalDelay);
        }
        hitGoalDelay = Timer.DelayedExecute(() =>
            {
                var level = collider.transform.GetComponentInParent<GolfHole>();
                if (level == CourseManager.instance.GetCurrentHole())
                {
                    BallHitGoalOrMaxStrokes();
                }
                else
                {
                    // we hit another hole, lol, plz just reset
                    CourseManager.instance.ChangeHole(CourseManager.instance.CurrentHoleIndex);
                }
            }, 1.5f);
        Soundmanager.instance.PlayOnBall(Soundmanager.instance.ballHitGoal);

    }

    public void BallHitGoalOrMaxStrokes()
    {
        // check if this is the last player...
        if (ScoreManager.instance.isCurrentlyLastPlayer())
        {
            // check if last hole in course
            if (CourseManager.instance.CurrentHoleIndex == CourseManager.instance.CurrentCourse.Holes.Length - 1)
            {
                StopGame();
                if (onHoleReachedEvent != null) onHoleReachedEvent.Invoke();
                if (onFinalHoleReachedEvent != null) onFinalHoleReachedEvent.Invoke();
            }
            else
            {
                if (onHoleReachedEvent != null) onHoleReachedEvent.Invoke();
                CourseManager.instance.GoToNextLevel();
            }
        }
        else
        {
            if (onHoleReachedEvent != null) onHoleReachedEvent.Invoke();
            CourseManager.instance.ChangeHole(CourseManager.instance.CurrentHoleIndex);
        }
    }

    public void BallHitBottomFloor()
    {
        if (!goalWasHit)
        {
            Soundmanager.instance.PlayOnBall(Soundmanager.instance.ballHitWall);
        }
    }

    public void BallHitWall()
    {
        Soundmanager.instance.PlayOnBall(Soundmanager.instance.ballHitWall);
    }

    public void BallHitPutter()
    {
        if (ball.WasSleeping())
        {
            if (onBallHitEvent != null) onBallHitEvent.Invoke();
            Soundmanager.instance.PlayOnBall(Soundmanager.instance.ballHitClub);

            if (Vector3.Distance(ball.transform.position, CourseManager.instance.GetCurrentHole().StartPosition.position) > 0.1f)
            {
                ballLastSet = true;
                ballLastPos = ball.transform.position;
                ballLastAngle = putter.putterKinematic.transform.eulerAngles.y;
            }
        }
    }

    public float GetPutterAngle2D()
    {
        var angle = Mathf.Atan2(putterGhost.transform.up.z, putterGhost.transform.up.x) * Mathf.Rad2Deg;
        return angle;
    }

    public Vector3 GetPutterAngle()
    {
        return putterGhost.rotation.eulerAngles;
    }

    public void ResetPutterAndBall()
    {
        SetBallToStart();
    }


    public void SetPutter(int putterID)
    {
        PutterModelParent.transform.Clear();
        var newPutter = Instantiate(PutterAndBallManager.instance.putters[putterID], PutterModelParent.transform) as GameObject;
        newPutter.transform.localPosition = Vector3.zero;
        newPutter.transform.localRotation = Quaternion.identity;

        putter.RefreshFootCollider();

        //Disable the putter
        clubEnabled = true;
        DisableClub();

    }

    public void SetBall(int ballID)
    {
        var newBall = Instantiate(PutterAndBallManager.instance.balls[ballID], ball.transform.parent) as GameObject;
        newBall.transform.localPosition = Vector3.zero;
        newBall.transform.localRotation = Quaternion.identity;
        Destroy(ball.gameObject);
        ball = newBall.GetComponent<Ball>();
        ballArrow.targetPosition = ball.transform;
    }

    public void ShowWarning(string warning)
    {
        if (onWarningMessage != null)
        {
            onWarningMessage.Invoke(warning);
        }
    }
}
