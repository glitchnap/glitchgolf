using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.StateMachine;
using System;
using GlitchConnect.Commands;
using UnityEngine.SceneManagement;
using DG.Tweening;

[MonoSingleton(false)]
public class ScoreManager : MonoSingleton<ScoreManager>
{
    [InspectorNote("Global Settings", NoteColor.pink)]
    public bool _global;

    public string defaultName = "Player 1";
    public int outOfBoundsPenalty;
    public int maxStrokes;
    // -1 = no max
    public bool defaultEnableMaxStrokes;

    [InspectorNote("Runtime Settings", NoteColor.pink)]
    public bool _runtime;

    public int currentPlayerID;
    public int strokeCounter;
    public float strokeHitCooldown;
    private float lastStrokeTime;


    [InspectorNote("References", NoteColor.pink)]
    public bool _references;

    public List<ScorePlayer> players;
    public Action OnScoreChanged;
    public Action<String,int> OnPlayerAddedEvent;
    public Action<int> OnPlayerRemovedEvent;
    public Action<String> OnSetFirstPlayerEvent;
    public Action<String,int> OnPlayerRenamedEvent;

    void Awake()
    {
        instance = this;

        // default setup
        players = new List<ScorePlayer>();
        
        if (!defaultEnableMaxStrokes)
        {
            maxStrokes = -1;
        }
    }

    void Start()
    {
        GameplayManager.instance.onBallHitEvent += OnStroke;
        GameplayManager.instance.onBallOutOfBoundsEvent += OnOutOfBounds;
        GameplayManager.instance.onHoleReachedEvent += OnReachHole;
        GameplayManager.instance.onHoleChangedEvent += OnChangeHole;

        AddPlayer(defaultName, 0, true, true);

    }

    new void OnDestroy()
    {
        if (GameplayManager.IsInstantiated())
        {
            GameplayManager.instance.onBallHitEvent -= OnStroke;
            GameplayManager.instance.onBallOutOfBoundsEvent -= OnOutOfBounds;
            GameplayManager.instance.onHoleChangedEvent -= OnChangeHole;
        }
    }

    void OnStroke()
    {
        if ((Time.unscaledTime - strokeHitCooldown) < lastStrokeTime)
        {
            //Ignore multiple hits within the cooldown period...
            return;
        }
            
        strokeCounter++;

        var currentHole = CourseManager.instance.CurrentHoleIndex;

        players[currentPlayerID].strokes[(int)currentHole] = strokeCounter;

        PropagateUpdateScore();

        lastStrokeTime = Time.unscaledTime;
    }

    void UpdatePlayerName(ScoreBoardStrokeRow scoreBoardStrokeRow, string text)
    {
        
    }

    public void PropagateUpdateScore()
    {
        if (OnScoreChanged != null) OnScoreChanged.Invoke();
    }

    void OnChangeHole()
    {
        currentPlayerID = 0;
        strokeCounter = 0;

        PropagateUpdateScore();
    }


    void OnReachHole()
    {


        currentPlayerID++;
        if (currentPlayerID >= players.Count) currentPlayerID = 0;
        strokeCounter = 0;

        PropagateUpdateScore();
    }

    void OnOutOfBounds()
    {
        strokeCounter += outOfBoundsPenalty;
        PropagateUpdateScore();
    }

    public void DoReset()
    {
        foreach (var item in players)
        {
            item.InitStrokes();
        }
        PropagateUpdateScore();
    }

    public int[] GetCurrentPlayerStrokes()
    {
        return players[currentPlayerID].strokes;
    }

    public bool isCurrentlyLastPlayer()
    {
        if (currentPlayerID + 1 >= players.Count)
        {
            return true;
        }
        return false;
    }

    public bool isCurrentlyLastStroke()
    {
        if (maxStrokes >= 0 && strokeCounter >= maxStrokes)
        {
            return true;
        }
        return false;
    }

    public void RemovePlayer(int ID)
    {
        players.RemoveAt(ID);
        PropagateUpdateScore();

        if (OnPlayerRemovedEvent != null) OnPlayerRemovedEvent(ID);
    }

    public void AddPlayer(string name, int ID, bool propagate, bool first)
    {
        // throw events first, so they can create necessary UI components.
        if (first)
        {
            if (OnSetFirstPlayerEvent != null) OnSetFirstPlayerEvent(name);
        }
        else
        {
            if (OnPlayerAddedEvent != null) OnPlayerAddedEvent(name, ID);
        }

        var player = new ScorePlayer();
        player.playerName = name;
        if (ID >= players.Count)
            players.Add(player);
        else
            players.Insert(ID, player);

        if (propagate)
        {
            PropagateUpdateScore();
        }

        DebugExtension.Blip();


     
    }

    public void SetPlayerName(int ID, string name)
    {
        players[ID].playerName = name;
        PropagateUpdateScore();

        if (OnPlayerRenamedEvent != null) OnPlayerRenamedEvent(name, ID);
    }

    public List<ScorePlayer> GetPlayers()
    {
        return players;
    }
}




