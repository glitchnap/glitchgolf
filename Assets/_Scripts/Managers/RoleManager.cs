using UnityEngine;
using System.Collections;
using System;
using GlitchConnect.Commands;


public enum GlitchGolfRoles
{
    none,
    putter,
    world,
    singleDevice
}

public class RoleManager : MonoSingleton<RoleManager>
{
    public static Action RoleChanged;

    public GlitchGolfRoles role;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        role = GlitchGolfRoles.none;
        SetRoleCommand.RoleChanged += OnRoleChangedCommand;
    }

    private void OnRoleChangedCommand(GlitchGolfRoles role)
    {
        SetRole(role);
    }

    public void SetRole(GlitchGolfRoles role)
    {
        this.role = role;
        switch (role)
        {
            case GlitchGolfRoles.none:
                break;
            case GlitchGolfRoles.putter:
                SetPutter();
                break;
            case GlitchGolfRoles.world:
                SetWorld();
                break;
            case GlitchGolfRoles.singleDevice:
                SetSingleDevice();
                break;
            default:
                break;
        }

        if (RoleChanged != null)
        {
            RoleChanged.Invoke();   
        }
    }

    private void SetPutter()
    {
        GlitchDebug.Log("BECAME PUTTER");

        GyroManager.instance.EnableGyro();
        SDInputManager.instance.Disable(); // in case it was running from a previous session

        ConnectionManager.instance.StartRotationLoop();
        ConnectionManager.instance.StartTimeStampLoop();
    }

    private void SetWorld()
    {
        GlitchDebug.Log("BECAME WORLD");

        GyroManager.instance.DisableGyro();
        SDInputManager.instance.Disable(); // in case it was running from a previous session

        ConnectionManager.instance.StopRotationLoop();
        ConnectionManager.instance.StopTimeStampLoop();
    }

    private void SetSingleDevice()
    {
        GlitchDebug.Log("BECAME SINGLE DEVICE");

        // stop looking, you'll never find love
        //ConnectionManager.instance.Disconnect();

        // this should enable the 'fake' gyro
        GyroManager.instance.EnableGyro();

        // this enables all custom Input stuff
        SDInputManager.instance.Enable();
    }


    public void SwapRoles()
    {
        if (IsWorld())
        {
            SetRole(GlitchGolfRoles.putter);
            ConnectionManager.instance.SendCommand(new SetRoleCommand(GlitchGolfRoles.world));
        }
        else
        {
            SetRole(GlitchGolfRoles.world);
            ConnectionManager.instance.SendCommand(new SetRoleCommand(GlitchGolfRoles.putter));
        }
    }

    public GlitchGolfRoles GetRole()
    {
        return role;
    }

    public bool IsPutter()
    {
        return role == GlitchGolfRoles.putter;
    }

    public bool IsWorld()
    {
        return role == GlitchGolfRoles.world;
    }

    public bool IsSingleDevice()
    {
        return role == GlitchGolfRoles.singleDevice;
    }

    public bool IsMultiDevice()
    {
        return role == GlitchGolfRoles.putter || role == GlitchGolfRoles.world;
    }
}
