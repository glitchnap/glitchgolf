using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.StateMachine;
using System;
using GlitchConnect.Commands;
using UnityEngine.SceneManagement;
using DG.Tweening;
using GlitchConnect;

public class MDInputManager : MonoSingleton<MDInputManager>
{
    public bool isPressing;
    public bool isCalibrating;
    private Vector2 lastReceivedDelta;

    void Awake()
    {
        instance = this;
    }

    void LateUpdate()
    {
        isPressing = false;
    }

    
}



