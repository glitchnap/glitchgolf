using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonsterLove.StateMachine;
using System;
using GlitchConnect.Commands;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class SDInputManager : MonoSingleton<SDInputManager>
{
    public bool gyroFlip;
    public float baseGyroTilt = 30;
    // this is in degrees. You don't want it to be 0, cause then you need to hold phone 90 degrees vertical for the putter to be 'in' the ball.
    public Renderer directionArrow;
    public bool leftHanded;

    public bool gyroRotate;
    public float gyroOffset;
    public bool tiltRotate;
    public float tiltDeadzone;

    private List<Quaternion> recordedRotations;
    private int recordingIterator;


    public enum States
    {
        Disabled,
        Navigate,
        Swing,
        Ballmoving,
        ClubSnapping
    }

    public StateMachine<States> fsm;

    void Awake()
    {
        instance = this;
        fsm = StateMachine<States>.Initialize(this, States.Disabled);
        directionArrow.enabled = false;
    }

    void Update()
    {
        if (RoleManager.instance.IsSingleDevice())
        {
            //GameplayManager.instance.UpdateClubEnabled(Input.GetMouseButton(0));

            // Debug keys
            if (Input.GetKeyDown(KeyCode.R))
            {
                CourseManager.instance.ChangeHole(CourseManager.instance.CurrentHoleIndex);
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                CourseManager.instance.GoToNextLevel();
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                CourseManager.instance.GoToPreviousLevel();
            }
        }

        

    }

    public void Navigate_Enter()
    {
        //GyroManager.instance.gyroController.enabled = false;
        GameplayManager.instance.DisableClub();

        directionArrow.enabled = true;

        GameplayManager.instance.FixClubTilt();
        UpdateSwing();
    }

    public void Navigate_Update()
    {
        UpdateAimRotation();
    }

    public void Swing_Enter()
    {
        //GyroManager.instance.gyroController.enabled = true;
        var gyroAngle = GyroManager.instance.gyroController.GetSafeEulerX();
        if (gyroFlip) gyroAngle = -gyroAngle;

        baseGyroTilt = gyroAngle + GameplayManager.instance.putterDistanceFromBall;    

        #if UNITY_EDITOR || UNITY_STANDALONE
        baseGyroTilt = Input.mousePosition.y / Screen.height * 180 + 90 + 30; //30 is initial base gyro tilt  
        #endif

        GameplayManager.instance.FixClubTilt();

        GameplayManager.instance.SnapClubToGhostInstant();
        GameplayManager.instance.SnapClubToBall(true);
        GameplayManager.instance.EnableClub(true);
    }

    public void Swing_Update()
    {
        //UpdateAimRotation();
        UpdateSwing();

        if (!GameplayManager.instance.ball.IsSleeping())
        {
            fsm.ChangeState(States.Ballmoving);
        }

        

        // still allow spinning in swing mode cause why not

    }

    public void Swing_Exit()
    {
        directionArrow.enabled = false;
    }

    private float rotateDelay;

    public void Ballmoving_Enter()
    {
        rotateDelay = 0.5f;
    }

    public void Ballmoving_Update()
    {
        rotateDelay -= Time.deltaTime;

        if (rotateDelay <= 0)
        {
            UpdateAimRotation();    
        }

        UpdateSwing();
        if (GameplayManager.instance.ball.IsSleeping())
        {
            fsm.ChangeState(States.Navigate);
        }
    }

    public void Ballmoving_Exit()
    {
        UpdateSwing();
        GameplayManager.instance.DisableClub();
    }

    public void Enable()
    {
        fsm.ChangeState(States.Navigate);
    }

    public void Disable()
    {
        fsm.ChangeState(States.Disabled);
    }

    public void Swing()
    {
        fsm.ChangeState(States.Swing);
    }


    public void CancelSwing()
    {
        if (fsm.State == States.Swing)
            fsm.ChangeState(States.Navigate);
        else
            GameplayManager.instance.DisableClub();
    }

    void UpdateAimRotation()
    {
        if (gyroRotate)
        {
            NavigationManager.instance.Calibrate(GolfCameraFollow.instance.lastAutoRotationAngle + gyroOffset + GyroManager.instance.gyroController.GetDeviceOrientation().eulerAngles.y, true);
        }
        else if (tiltRotate)
        {
            if (Mathf.Abs(Input.acceleration.x) > tiltDeadzone)
            {
                NavigationManager.instance.Calibrate(-Input.acceleration.x * Time.deltaTime);
            }
        }

        NavigationManager.instance.Calibrate(InputHelper.GetLocalDelta().x);
    }

    public void UpdateSwing()
    {
        var destination = Quaternion.identity;

        // First tilt the putter
        destination = Quaternion.Euler(GameplayManager.instance.putterTilt, 0, 0) * destination;

        // Then add gyro-based swing 
        if (instance.fsm.State == States.Swing || fsm.State == States.Ballmoving)
        {
            var deviceXaxisTilt = GyroManager.instance.gyroController.GetSafeEulerX();

#if UNITY_EDITOR || UNITY_WINDOWS || UNITY_OSX
            deviceXaxisTilt = Input.mousePosition.y / Screen.height * 180 + 90;
            //deviceXaxisTilt = Input.mousePosition.y / Screen.height *360;
#endif
            if (gyroFlip) deviceXaxisTilt = -deviceXaxisTilt;

            if (leftHanded)
            {
                destination = Quaternion.Euler(0, 0, deviceXaxisTilt - baseGyroTilt) * destination;
            }
            else
            {
                destination = Quaternion.Euler(0, 0, -deviceXaxisTilt + baseGyroTilt) * destination;
            }

        }
        else
        {
            if (!leftHanded)
            {
                destination = Quaternion.Euler(0, 0, GameplayManager.instance.putterDistanceFromBall) * destination;
            }
            else
            {
                destination = Quaternion.Euler(0, 0, -GameplayManager.instance.putterDistanceFromBall) * destination;
            }
        }

        // lerp that sucker
        GameplayManager.instance.putterGhost.localRotation = destination;
    }

    public void SetAimRotation(float angle)
    {
        NavigationManager.instance.Calibrate(angle, true);
    }


}
