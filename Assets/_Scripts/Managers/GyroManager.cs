using UnityEngine;
using System.Collections;

public class GyroManager : MonoSingleton<GyroManager>
{
    public GoogleVRGyroController gyroController;

    void Awake()
    {
        instance = this;
    }

    public void EnableGyro()
    {
        var gyroController = GameplayManager.instance.putterGhost.GetComponent<GoogleVRGyroController>();

        if (gyroController == null)
        {
            gyroController = GameplayManager.instance.putterGhost.gameObject.AddComponent<GoogleVRGyroController>();    
        }
        gyroController.enabled = true;

        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
        }

        this.gyroController = gyroController;
    }

    public void DisableGyro()
    {
        var gyroController = GameplayManager.instance.putterGhost.GetComponent<GoogleVRGyroController>();    

        if (gyroController != null)
        {
            Destroy(gyroController);   
        }

        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = false;
        }
    }
}
