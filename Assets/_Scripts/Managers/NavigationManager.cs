using UnityEngine;
using System.Collections;
using DG.Tweening;

//using Holoville.HOTween;
using System.Collections.Generic;
using System.Linq;
using GlitchConnect.Commands;

public class NavigationManager : MonoSingleton<NavigationManager>
{
    [InspectorNote("Global Settings", NoteColor.pink)]
    public bool _global;
    public float maxCameraHeight;
    public float minCameraHeight;
    public float maxAngledCameraHeight;
    public float minAngledCameraHeight;
    public float cameraZoomScale;
    public float calibrateScaleMD;
    public float calibrateScaleSD;

    [InspectorNote("Runtime Settings", NoteColor.pink)]
    public bool _runtime;


    [InspectorNote("References", NoteColor.pink)]
    public bool _references;

    public Transform putterGhostCalibrate;

    private float levelCalibrationOffset;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        NavigationCommand.OnNavigationCommandReceived += OnNavigationCommandReceived;
    }

    public void DoLevelCalibration(float offsetAmount)
    {
        if (!RoleManager.instance.IsSingleDevice())
        {
            Calibrate(-levelCalibrationOffset + putterGhostCalibrate.eulerAngles.y, true); //Remove old calibration
            levelCalibrationOffset = offsetAmount;
            Calibrate(levelCalibrationOffset + putterGhostCalibrate.eulerAngles.y, true); //Add new calibration
        }
    }

    public void Calibrate(float amount, bool absolute = false)
    {
        //Camera.main.transform.Rotate(0, 0, amount * calibrateScale);
        if (!absolute)
        {
            var calibrateScale = calibrateScaleMD;
            if (RoleManager.instance.IsSingleDevice())
            {
                calibrateScale = calibrateScaleSD;
            }
            putterGhostCalibrate.Rotate(0, amount * calibrateScale * Time.deltaTime, 0);
        }
        else
            putterGhostCalibrate.rotation = Quaternion.Euler(putterGhostCalibrate.eulerAngles.x, amount, putterGhostCalibrate.eulerAngles.z);
    }

    public void Zoom(float amount)
    {
        //var y = GolfCameraFollow.instance.GetCameraHeight();
        
        var y = amount * cameraZoomScale;

        if (RoleManager.instance.IsWorld())
        {
            y = Mathf.Clamp(y, minCameraHeight, maxCameraHeight);
        }
        else
        {
            y = Mathf.Clamp(y, minAngledCameraHeight, maxAngledCameraHeight);
        }
        GolfCameraFollow.instance.SetCameraHeight(y);
    }

    public float GetCameraHeight()
    {
        return GolfCameraFollow.instance.GetCameraHeight();
    }

    public float GetLevelCalibration()
    {
        return levelCalibrationOffset;
    }

    public void OnNavigationCommandReceived(NavigationCommands command, float value)
    {
        switch (command)
        {
//            case NavigationCommands.StartCalibrate:
//                Calibrate(value);
//                break;
            case NavigationCommands.Zoom:
                var actualZoomValue = (maxCameraHeight - minCameraHeight) * value + minCameraHeight;
                Zoom(actualZoomValue);
                break;
            default:
                break;
        }
    }
}