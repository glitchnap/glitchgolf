using UnityEngine;
using System.Collections;

public class SpinWithPhysics : MonoBehaviour {

    public Vector3 direction;

    private Rigidbody rb;
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
    void FixedUpdate () {
        rb.AddTorque(direction);
	}
}
