using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class AssignPhysicsMaterialToAll : MonoBehaviour {
    public PhysicMaterial mat;
	void OnEnable()
    {
        foreach (var item in transform.GetComponentsInChildren<Collider>())
        {
            item.material = mat;
            //item.sharedMesh = item.transform.GetComponent<MeshFilter>().sharedMesh;
        }
        enabled=false;
    }
}
