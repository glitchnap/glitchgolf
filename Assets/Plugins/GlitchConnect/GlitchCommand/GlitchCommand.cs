using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlitchConnect.Commands
{
    public interface IGlitchCommand
    {
        void Execute();
    }
}