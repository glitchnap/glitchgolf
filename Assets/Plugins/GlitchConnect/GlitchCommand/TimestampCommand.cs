using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GlitchConnect.Commands
{
    [Serializable]
    public class TimestampCommand : IGlitchCommand
    {
    
        [SerializeField]
        private float s;

        public float SendRate { get { return s; } set { s = value; } }

        [SerializeField]
        private float t;

        public float TimeStamp { get { return t; } set { t = value; } }

        [SerializeField]
        private bool r;

        public bool IsResponse { get { return r; } set { r = value; } }

        public TimestampCommand(float sendRate, float timeStamp, bool isResponse)
        {
            SendRate = sendRate;
            TimeStamp = timeStamp;
            IsResponse = isResponse;
        }

        public void Execute()
        {
            if (!IsResponse)
            {
                // basically sending 'a timestamp' as a roundtrip value, so that the sender can evaluate it
                IsResponse = true;
                GlitchConnector.instance.RecommendedSendRate = SendRate;
                GlitchConnector.instance.SendCommandUnreliably(this);
            }
            else
            {
                var roundTrip = Time.unscaledTime - TimeStamp;
                SendRate = GlitchConnector.instance.RecommendedSendRate;
                if ((1f / (roundTrip / 2f)) < SendRate) //Reduce sendrate, if roundtrip is longer than expected
                {
                    SendRate -= 0.5f;
                }
                else if ((1f / (roundTrip / 2f)) > SendRate) //Increase sendrate, if roundtrip is shorter than expected
                {
                    SendRate += 0.5f;
                }
//                SendRate = 1f / (roundTrip / 2f);
    
                SendRate = Mathf.Clamp(SendRate, 5, 60);
                GlitchConnector.instance.RecommendedSendRate = SendRate;
            }
            //DebugExtension.Blip(null, "I WAS EXECUTED HOORAY " + SendRate + " " + IsResponse + " " + TimeStamp, Color.red);
        }
    }
}
