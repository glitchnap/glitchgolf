using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class GlitchConnectionEvent
{
    public GlitchConnectionEvent() { }
    public GlitchConnectionEvent(GlitchConnectionEventType type) { Type = type; }

    public enum GlitchConnectionEventType
    {
        Connected,
        ConnectionLost,
        PeerDisconnected,
    }
	
    public GlitchConnectionEventType Type;
}

