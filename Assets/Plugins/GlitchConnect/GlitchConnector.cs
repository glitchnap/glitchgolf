#if UNITY_IOS
#define USE_MULTIPEER // Use Multipeer
#else
#undef USE_MULTIPEER // Always Disable Multipeer for Non-IOS platforms
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MonsterLove.StateMachine;
using GlitchConnect.Commands;

namespace GlitchConnect
{
    public class GlitchConnector : MonoSingleton<GlitchConnector>
    {
        public enum ConnectorTypes
        {
            GlitchUNet,
            GlitchMultipeer
        }

        public enum States
        {
            Disconnected,
            Connecting,
            Connected
        }

        public StateMachine<States> fsm;

        public event Action<GlitchConnectionEvent> OnConnectionChanged;
        public event Action<GlitchData> OnReceiveData;
        public event Action<IGlitchCommand> OnReceiveCommand;
    
        // The received or calculated SendRate
        public float RecommendedSendRate = 5;
        public float TimeStampLoopSendRate = 50;

        public bool shouldAutoStartTimestampLoop;

        private IConnector currentConnector { get; set; }

        private Coroutine timestampLoop;

        void Awake()
        {
            instance = this;
            fsm = StateMachine<States>.Initialize(this, States.Disconnected);

        }

        public void StartConnecting(ConnectorTypes type, int minPeerConnections, string gameName)
        {
            GlitchDebug.Log("[GC] Start Connecting: ### " + type.ToString().ToUpper() + " ###", "Glitchconnect");

            if (currentConnector != null)
            {
                currentConnector.Disconnect();
            }

            switch (type)
            {
                case ConnectorTypes.GlitchUNet:
                    currentConnector = new UNet.GlitchUNetConnector();
                    break;
                case ConnectorTypes.GlitchMultipeer:
                    currentConnector = new GlitchMultipeerConnector();
                    break;
            }

            if (currentConnector != null)
            {
                StartListening();
                currentConnector.Connect(minPeerConnections, gameName);
                fsm.ChangeState(States.Connecting);
            }
        }

        public void Disconnect()
        {
            GlitchDebug.Log("[GC] Disconnect", "Glitchconnect");
            StopListening();
            fsm.ChangeState(States.Disconnected);
            if (currentConnector != null)
            {
                currentConnector.Disconnect();
            }
        }

        private void StartListening()
        {
            GlitchDebug.Log("[GC] StartListening", "Glitchconnect");

            if (currentConnector != null)
            {
                currentConnector.OnConnectionChanged += connector_OnConnectionChanged;
                currentConnector.OnReceiveData += connector_OnReceiveData;
                currentConnector.OnReceiveData += connector_OnReceiveCommand;
            }
        }

        private void StopListening()
        {
            GlitchDebug.Log("[GC] StopListening", "Glitchconnect");

            if (currentConnector != null)
            {
                currentConnector.OnConnectionChanged -= connector_OnConnectionChanged;
                currentConnector.OnReceiveData -= connector_OnReceiveData;
                currentConnector.OnReceiveData -= connector_OnReceiveCommand;
            }
        }

        public void SendData(string data)
        {
            if (currentConnector != null)
            {
                currentConnector.SendData(data);
                //Debug.Log("[GC] Sending Data Reliably: " + data);
            }
            else
            {
                GlitchDebug.Log("[GC] Tried to send Data without a connector", "ERROR");
            }
        }

        public void SendCommand(IGlitchCommand command)
        {
            if (currentConnector != null)
            {
                currentConnector.SendData(GlitchCommandHandler.Serialize(command));
                GlitchDebug.Log("[GC] Sending Data Reliably: " + command.GetType().ToString() + " " + GlitchCommandHandler.Serialize(command));
            }
            else
            {
                GlitchDebug.Log("[GC] Tried to send Data without a connector", "ERROR");
            }
        }

        public void SendCommandUnreliably(IGlitchCommand command)
        {
            if (currentConnector != null)
            {
                currentConnector.SendDataUnreliably(GlitchCommandHandler.Serialize(command));
                //Debug.Log("[GC] Sending Data Unreliably: " + GlitchCommandHandler.Serialize(command));
            }
            else
            {
                GlitchDebug.Log("[GC] Tried to send Data without a connector", "ERROR");
            }
        }

        public void SendDataUnreliably(string data)
        {
            if (currentConnector != null)
            {
                currentConnector.SendDataUnreliably(data);
                //Debug.Log("[GC] Sending Data Unreliably: " + data);
            }
            else
            {
                GlitchDebug.Log("[GC] Tried to send Data without a connector", "ERROR");
            }
        }

        public void SendDataStream(string data)
        {
            if (currentConnector != null)
            {
                currentConnector.SendDataStream(data);
            }
            else
            {
                GlitchDebug.Log("[GC] Tried to send Data without a connector", "ERROR");
            }
        }

        public IEnumerable<GlitchPeer> PeerList
        {
            get
            {
                if (currentConnector != null)
                {
                    return currentConnector.PeerList;
                }
                return new List<GlitchPeer>();
            }
        }

    
        void connector_OnConnectionChanged(GlitchConnectionEvent obj)
        {
            GlitchDebug.Log("[GC] Connection changed: " + obj.Type.ToString(), "GlitchConnect");
            if (obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost ||
                obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.PeerDisconnected)
            {
                StopListening();
                StopTimeStampLoop();
                fsm.ChangeState(States.Disconnected);
            }
            else if (obj.Type == GlitchConnectionEvent.GlitchConnectionEventType.Connected)
            {
                if (shouldAutoStartTimestampLoop)
                {
                    StartTimeStampLoop();
                }
                fsm.ChangeState(States.Connected);
            }

            // Propagate conn changed event...
            if (OnConnectionChanged != null)
            {
                OnConnectionChanged(obj);
            }
        }

        void connector_OnReceiveData(GlitchData obj)
        {
            if (OnReceiveData != null)
            {
                OnReceiveData(obj);
                Debug.Log("[GC] Received data: " + obj.Data);
            }
        }

        void connector_OnReceiveCommand(GlitchData obj)
        {   
       
            IGlitchCommand command = GlitchCommandHandler.Deserialize(obj.Data);

            //Debug.Log("[GC] Received command : " + command.GetType().Name + " " + obj.Data);

            command.Execute();
            if (OnReceiveCommand != null) OnReceiveCommand(command);
            

        }

        public void DebugOnReceiveCommand(GlitchData obj)
        {
            connector_OnReceiveCommand(obj);
        }

        public void StartTimeStampLoop()
        {
            timestampLoop = StartCoroutine(SendTimestampLoop());
        }

        public void StopTimeStampLoop()
        {
            if (timestampLoop != null)
            {
                StopCoroutine(timestampLoop);    
            }
        }

        public IEnumerator SendTimestampLoop()
        {
            while (fsm.State == States.Connected)
            {
                GlitchConnector.instance.SendCommandUnreliably(new TimestampCommand(RecommendedSendRate, Time.unscaledTime, false));

                yield return new WaitForSeconds(1f / TimeStampLoopSendRate);

                //Adjust the timestamp loop down to a reasonable once a second, after a little while.
                if (TimeStampLoopSendRate > 1f)
                {
                    TimeStampLoopSendRate -= 0.1f;
                }
            }
        }

    }
}
