using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


// This class is based off of
//    public class NetworkDiscovery : MonoBehaviour
// From the Unet NetworkDiscovery source:
// https://bitbucket.org/Unity-Technologies/networking/raw/78ca8544bbf4e87c310ce2a9a3fc33cdad2f9bb1/Runtime/NetworkDiscovery.cs
// It was overwritten because it doesn't allow running server/client at the same time for no discernable reason.

namespace GlitchConnect.UNet
{
    public struct NetworkBroadcastResult
    {
        public string serverAddress;
        public byte[] broadcastData;
    }

    public class GlitchUNetDiscovery : MonoSingleton<GlitchUNetDiscovery>
    {
        const int k_MaxBroadcastMsgSize = 1024;

        // config data
        [SerializeField]
        int m_BroadcastPort = 47777;

        [SerializeField]
        int m_BroadcastKey = 2222;

        [SerializeField]
        int m_BroadcastVersion = 1;

        [SerializeField]
        int m_BroadcastSubVersion = 1;

        [SerializeField]
        int m_BroadcastInterval = 1000;

        [SerializeField]
        bool m_UseNetworkManager = true;

        [SerializeField]
        string m_BroadcastData = "HELLO";

        [SerializeField]
        int m_OffsetX;

        [SerializeField]
        int m_OffsetY;

        // runtime data
        int m_HostIdClient = -1;
        int m_HostIdServer = -1;

        bool m_Running;

        bool m_IsServer;
        bool m_IsClient;

        byte[] m_MsgOutBuffer;
        byte[] m_MsgInBuffer;
        HostTopology m_DefaultTopology;
        Dictionary<string, NetworkBroadcastResult> m_BroadcastsReceived;

        public Action<string, int> OnBroadcastReceived;

        public int broadcastPort
        {
            get { return m_BroadcastPort; }
            set { m_BroadcastPort = value; }
        }

        public int broadcastKey
        {
            get { return m_BroadcastKey; }
            set { m_BroadcastKey = value; }
        }

        public int broadcastVersion
        {
            get { return m_BroadcastVersion; }
            set { m_BroadcastVersion = value; }
        }

        public int broadcastSubVersion
        {
            get { return m_BroadcastSubVersion; }
            set { m_BroadcastSubVersion = value; }
        }

        public int broadcastInterval
        {
            get { return m_BroadcastInterval; }
            set { m_BroadcastInterval = value; }
        }

        public bool useNetworkManager
        {
            get { return m_UseNetworkManager; }
            set { m_UseNetworkManager = value; }
        }

        public string broadcastData
        {
            get { return m_BroadcastData; }
            set
            {
                m_BroadcastData = value;
                m_MsgOutBuffer = StringToBytes(m_BroadcastData);
                if (m_UseNetworkManager)
                {
                    if (LogFilter.logWarn)
                    {
                        Debug.LogWarning("NetworkDiscovery broadcast data changed while using NetworkManager. This can prevent clients from finding the server. The format of the broadcast data must be 'NetworkManager:IPAddress:Port'.");
                    }
                }
            }
        }

        public int offsetX
        {
            get { return m_OffsetX; }
            set { m_OffsetX = value; }
        }

        public int offsetY
        {
            get { return m_OffsetY; }
            set { m_OffsetY = value; }
        }

        public int hostIdClient
        {
            get { return m_HostIdClient; }
            set { m_HostIdClient = value; }
        }

        public int hostIdServer
        {
            get { return m_HostIdServer; }
            set { m_HostIdServer = value; }
        }

        public bool running
        {
            get { return m_Running; }
            set { m_Running = value; }
        }

        public bool isServer
        {
            get { return m_IsServer; }
            set { m_IsServer = value; }
        }

        public bool isClient
        {
            get { return m_IsClient; }
            set { m_IsClient = value; }
        }

        public Dictionary<string, NetworkBroadcastResult> broadcastsReceived
        {
            get { return m_BroadcastsReceived; }
        }

        static byte[] StringToBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string BytesToString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        void Awake()
        {
            instance = this;
        }

        public bool Initialize()
        {
            if (m_BroadcastData.Length >= k_MaxBroadcastMsgSize)
            {
                if (LogFilter.logError)
                {
                    Debug.LogError("NetworkDiscovery Initialize - data too large. max is " + k_MaxBroadcastMsgSize);
                }
                return false;
            }

            if (!NetworkTransport.IsStarted)
            {
                NetworkTransport.Init();
            }

            if (m_UseNetworkManager && NetworkManager.singleton != null)
            {
                m_BroadcastData = "NetworkManager:" + NetworkManager.singleton.networkAddress + ":" + NetworkManager.singleton.networkPort;
                if (LogFilter.logInfo)
                {
                    Debug.Log("NetworkDiscovery set broadcast data to:" + m_BroadcastData);
                }
            }

            m_MsgOutBuffer = StringToBytes(m_BroadcastData);
            m_MsgInBuffer = new byte[k_MaxBroadcastMsgSize];
            m_BroadcastsReceived = new Dictionary<string, NetworkBroadcastResult>();

            ConnectionConfig cc = new ConnectionConfig();
            cc.AddChannel(QosType.Unreliable);
            m_DefaultTopology = new HostTopology(cc, 1);

            if (m_IsServer)
                StartAsServer();

            if (m_IsClient)
                StartAsClient();

            return true;
        }

        public bool StartAsClient()
        {
            if (m_HostIdClient != -1 || m_IsClient)
            {
                if (LogFilter.logWarn)
                {
                    Debug.LogWarning("NetworkDiscovery StartAsClient already started");
                }
                return false;
            }

            m_HostIdClient = NetworkTransport.AddHost(m_DefaultTopology, m_BroadcastPort);
            if (m_HostIdClient == -1)
            {
                if (LogFilter.logError)
                {
                    Debug.LogError("NetworkDiscovery StartAsClient - addHost failed");
                }
                return false;
            }

            byte error;
            NetworkTransport.SetBroadcastCredentials(m_HostIdClient, m_BroadcastKey, m_BroadcastVersion, m_BroadcastSubVersion, out error);

            m_Running = true;
            m_IsClient = true;
            if (LogFilter.logDebug)
            {
                Debug.Log("StartAsClient Discovery listening");
            }
            return true;
        }

        public bool StartAsServer()
        {
            if (m_HostIdServer != -1 || m_IsServer)
            {
                if (LogFilter.logWarn)
                {
                    Debug.LogWarning("NetworkDiscovery StartAsServer already started");
                }
                return false;
            }

            m_HostIdServer = NetworkTransport.AddHost(m_DefaultTopology, 0);
            if (m_HostIdServer == -1)
            {
                if (LogFilter.logError)
                {
                    Debug.LogError("NetworkDiscovery StartAsServer - addHost failed");
                }
                return false;
            }

            byte err;
            if (!NetworkTransport.StartBroadcastDiscovery(m_HostIdServer, m_BroadcastPort, m_BroadcastKey, m_BroadcastVersion, m_BroadcastSubVersion, m_MsgOutBuffer, m_MsgOutBuffer.Length, m_BroadcastInterval, out err))
            {
                if (LogFilter.logError)
                {
                    Debug.LogError("NetworkDiscovery StartBroadcast failed err: " + err);
                }
                return false;
            }

            m_Running = true;
            m_IsServer = true;
            if (LogFilter.logDebug)
            {
                Debug.Log("StartAsServer Discovery broadcasting");
            }
            DontDestroyOnLoad(gameObject);
            return true;
        }

        public void StopDiscovery()
        {
            try
            {
                if (m_IsServer)
                {
                    NetworkTransport.StopBroadcastDiscovery();
                    NetworkTransport.RemoveHost(m_HostIdServer);
                }
                if (m_IsClient)
                {
                    NetworkTransport.RemoveHost(m_HostIdClient);
                }
            }
            catch (Exception)
            {
                Debug.Log("Weird bug during Stop Discovery");
            }
            m_HostIdClient = -1;
            m_HostIdServer = -1;
            m_Running = false;
            m_IsServer = false;
            m_IsClient = false;
            m_MsgInBuffer = null;
            m_BroadcastsReceived = null;
            Debug.Log("Stopped Discovery broadcasting & listening");
        }

        void Update()
        {
            if (m_IsServer)
                UpdateServer();
            if (m_IsClient)
                UpdateClient();
        }

        void UpdateClient()
        {
            NetworkEventType networkEvent;
            do
            {
                int connectionId;
                int channelId;
                int receivedSize;
                byte error;
                networkEvent = NetworkTransport.ReceiveFromHost(m_HostIdClient, out connectionId, out channelId, m_MsgInBuffer, k_MaxBroadcastMsgSize, out receivedSize, out error);

                if (networkEvent == NetworkEventType.BroadcastEvent)
                {
                    NetworkTransport.GetBroadcastConnectionMessage(m_HostIdClient, m_MsgInBuffer, k_MaxBroadcastMsgSize, out receivedSize, out error);

                    string senderAddr;
                    int senderPort;
                    NetworkTransport.GetBroadcastConnectionInfo(m_HostIdClient, out senderAddr, out senderPort, out error);

                    var recv = new NetworkBroadcastResult();
                    recv.serverAddress = senderAddr;
                    recv.broadcastData = new byte[receivedSize];
                    Buffer.BlockCopy(m_MsgInBuffer, 0, recv.broadcastData, 0, receivedSize);
                    m_BroadcastsReceived[senderAddr] = recv;

                    string dataString = BytesToString(recv.broadcastData);
                    var items = dataString.Split(':');
                    var networkAddress = items[1];
                    var networkPort = Convert.ToInt32(items[2]);

                    // makes ure we're not discovering this class' server...
                    if (networkAddress != NetworkManager.singleton.networkAddress
                        || networkPort != NetworkManager.singleton.networkPort)
                    {
                        ReceivedBroadcast(recv.serverAddress, networkPort);
                    }
                }
            }
            while (networkEvent != NetworkEventType.Nothing && m_HostIdClient > -1);
        }

        void UpdateServer()
        {
            // Do nothing
        }

        new void OnDestroy()
        {
            if (m_IsServer && m_Running && m_HostIdServer != -1)
            {
                NetworkTransport.StopBroadcastDiscovery();
                NetworkTransport.RemoveHost(m_HostIdServer);
            }

            if (m_IsClient && m_Running && m_HostIdClient != -1)
            {
                NetworkTransport.RemoveHost(m_HostIdClient);
            }
            base.OnDestroy();
        }

        public virtual void ReceivedBroadcast(string networkAddress, int networkPort)
        {
            if (OnBroadcastReceived != null) OnBroadcastReceived.Invoke(networkAddress, networkPort);
        }

        void OnGUI()
        {
            /*GUI.color = Color.black;
            GUILayout.Label("ISSERVER: " + isServer);
            GUILayout.Label("ISCLIENT: " + isClient);
            GUILayout.Label("IsClientConnected: " + GlitchUNetManager.instance.IsClientConnected());
            GUILayout.Label("Port: " + GlitchUNetManager.instance.networkPort);*/
        }
    }
}
