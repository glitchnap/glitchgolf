using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace GlitchConnect.UNet
{
    public class GlitchUNetConnector : IConnector
    {
        private GameObject GlitchUnetManagerObject;

        public void Connect(int minPeerConnections, string gameName)
        {
            // Network Manager StartServer...
            GlitchUnetManagerObject = new GameObject("GlitchUnetManager");
            GlitchUnetManagerObject.AddComponent<GlitchUNetManager>();

            GlitchUNetManager.instance.Init(this);

            GlitchUNetManager.instance.networkPort = Random.Range(7999, 9999);

            GlitchUNetDiscovery.instance.Initialize();
            bool clientSuccess = GlitchUNetDiscovery.instance.StartAsClient();

            if(!clientSuccess)
            {
                Debug.Log("Failed to start Client, will default server to port 7000");
                GlitchUNetManager.instance.networkPort = 7000;
                GlitchUNetDiscovery.instance.Initialize();
            }

            GlitchUNetManager.instance.StartServer();

            bool serverSuccess = GlitchUNetDiscovery.instance.StartAsServer();
            if(!serverSuccess)
            {
                Debug.Log("Failed to start Server...");
            }

            GlitchUNetDiscovery.instance.OnBroadcastReceived += OnBroadcastReceived;
        }

        private void OnBroadcastReceived(string networkAddress, int networkPort)
        {
            Debug.Log("RECEIVED BROADCAST " + networkAddress + " " + networkPort + "(my port: " + GlitchUNetManager.singleton.networkPort + ", platform: " + Application.platform.ToString()+")");

            // NOTE: This will not work for > 2 devices
            // Editor will always connect because it has a forced low port
            if (networkPort < GlitchUNetManager.singleton.networkPort)
            {
                Debug.Log("CONNECTING YO");
                DoActualConnect(networkAddress, networkPort);
            }
        }

        private void DoActualConnect(string networkAddress, int networkPort)
        {
            GlitchUNetManager.instance.networkAddress = networkAddress;
            GlitchUNetManager.instance.networkPort = networkPort;
            //var client = 
                GlitchUNetManager.instance.StartClient();
            GlitchUNetDiscovery.instance.StopDiscovery();

            // TODO implement client not started
        }

        public void Disconnect()
        {
            GlitchUNetDiscovery.instance.StopDiscovery();
            GlitchUNetManager.instance.StopServer();
            GameObject.Destroy(GlitchUnetManagerObject);
        }

        public void SendData(string data)
        {
            GlitchUNetManager.instance.GetLocalPlayer().SendReliable(data);
        }

        public void SendDataUnreliably(string data)
        {
            GlitchUNetManager.instance.GetLocalPlayer().SendUnreliable(data);
        }

        public void SendDataStream(string data)
        {
            GlitchUNetManager.instance.GetLocalPlayer().SendUnreliable(data);
        }

        public IEnumerable<GlitchPeer> PeerList
        {
            get { throw new System.NotImplementedException(); }
        }

        public void ChangedState(GlitchConnectionEvent e)
        {
            if(e.Type == GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost ||
                e.Type == GlitchConnectionEvent.GlitchConnectionEventType.PeerDisconnected)
            {
                GlitchUNetDiscovery.instance.StopDiscovery();
                GameObject.Destroy(GlitchUnetManagerObject);
            }
            else if (e.Type == GlitchConnectionEvent.GlitchConnectionEventType.Connected)
            {
                GlitchUNetDiscovery.instance.StopDiscovery();
            }
            OnConnectionChanged(e);
        }

        public event System.Action<GlitchConnectionEvent> OnConnectionChanged;

        public void ReceivedData(GlitchData data)
        {
            OnReceiveData(data);
        }

        public event System.Action<GlitchData> OnReceiveData;

    }
}
