using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

namespace GlitchConnect.UNet
{
    public class GlitchUNetManager : NetworkManager
    {
        public static GlitchUNetManager instance { get { return singleton as GlitchUNetManager; } }
        public GlitchUNetConnector connector;
        private GameObject serverPlayerObject;

        public bool isServerConnected;
        public bool isServerReady;
        public bool isReady
        {
            get
            {
                return (isServerConnected && isServerReady) || (IsClientConnected() && client.connection.playerControllers.Count > 0);
            }
        }

        public GlitchUNetPlayer GetLocalPlayer()
        {
            if (IsClientConnected())
                return (client.connection.playerControllers[0]).gameObject.GetComponent<GlitchUNetPlayer>();
            else
                return serverPlayerObject.GetComponent<GlitchUNetPlayer>();
        }

        public void Init(GlitchUNetConnector connector)
        {
            DebugExtension.Blip(this);
            autoCreatePlayer = true;
            playerPrefab = Resources.Load("GlitchUNetClientPlayer") as GameObject;

            // private var, is normally initialized through inspector
            // So reflected here to be able to activate the list...
            FieldInfo[] fields = typeof(NetworkManager).GetFields( BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var item in fields)
            {
                if(item.Name == "m_SpawnPrefabs")
                {
                    item.SetValue(this, new List<GameObject>());
                }
            }

            spawnPrefabs.Add(playerPrefab);
            spawnPrefabs.Add(Resources.Load("GlitchUNetServerPlayer") as GameObject);
            this.connector = connector;
        }

        public override void OnClientConnect(NetworkConnection conn)
        {
            DebugExtension.Blip(this);
            base.OnClientConnect(conn);

            Timer.WaitForCondition(() =>
            {
                connector.ChangedState(new GlitchConnectionEvent(GlitchConnectionEvent.GlitchConnectionEventType.Connected));
            },
            () => { return isReady; });
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            DebugExtension.Blip(this);
            base.OnClientDisconnect(conn);
            connector.ChangedState(new GlitchConnectionEvent(GlitchConnectionEvent.GlitchConnectionEventType.PeerDisconnected));
        }

        public override void OnClientNotReady(NetworkConnection conn)
        {
            DebugExtension.Blip(this);
            base.OnClientNotReady(conn);
        }

        public override void OnClientError(NetworkConnection conn, int errorCode)
        {
            DebugExtension.Blip(this);
            base.OnClientError(conn, errorCode);
        }

        public override void OnStopClient()
        {
            DebugExtension.Blip(this);
            base.OnStopClient();
        }

        public override void OnServerConnect(NetworkConnection conn)
        {
            DebugExtension.Blip(this);
            base.OnServerConnect(conn);
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            base.OnServerDisconnect(conn);
            isServerConnected = false;
            isServerReady = false;
            connector.ChangedState(new GlitchConnectionEvent(GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost));
            DebugExtension.Blip(this);
        }

        public override void OnServerError(NetworkConnection conn, int errorCode)
        {
            DebugExtension.Blip(this);
            base.OnServerError(conn, errorCode);
        }

        public override void OnStartClient(NetworkClient client)
        {
            DebugExtension.Blip(this);
            base.OnStartClient(client);
        }

        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {
            DebugExtension.Blip(this);
            base.OnServerAddPlayer(conn, playerControllerId);

            isServerConnected = true;
            if (serverPlayerObject == null)
            {
                serverPlayerObject = Instantiate(Resources.Load("GlitchUNetServerPlayer")) as GameObject;
                NetworkServer.Spawn(serverPlayerObject);
            }

            Timer.WaitForCondition(() =>
            {
                connector.ChangedState(new GlitchConnectionEvent(GlitchConnectionEvent.GlitchConnectionEventType.Connected));
            },
            () => { return isReady; });
        }

        public override void OnServerReady(NetworkConnection conn)
        {
            DebugExtension.Blip(this);
            base.OnServerReady(conn);
            isServerReady = true;
        }

        public override void OnStopServer()
        {
            DebugExtension.Blip(this);
            base.OnStopServer();
        }
    }
}
