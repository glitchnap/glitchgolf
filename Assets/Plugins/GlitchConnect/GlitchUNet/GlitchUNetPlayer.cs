using UnityEngine;
using UnityEngine.Networking;

namespace GlitchConnect.UNet
{
    public class GlitchUNetPlayer : NetworkBehaviour
    {
        [Command]
        void CmdReliable(string data)
        {
            GlitchUNetManager.instance.connector.ReceivedData(new GlitchData("peer", data));
        }

        [Command(channel = 1)]
        void CmdUnreliable(string data)
        {
            GlitchUNetManager.instance.connector.ReceivedData(new GlitchData("peer", data));
        }

        [ClientRpc]
        void RpcReliable(string data)
        {
            GlitchUNetManager.instance.connector.ReceivedData(new GlitchData("peer", data));
        }

        [ClientRpc(channel = 1)]
        void RpcUnreliable(string data)
        {
            GlitchUNetManager.instance.connector.ReceivedData(new GlitchData("peer", data));
        }

        public void SendReliable(string data)
        {
            if (isServer)
                RpcReliable(data);
            else
                CmdReliable(data);
        }

        public void SendUnreliable(string data)
        {
            if (isServer)
                RpcUnreliable(data);
            else
                CmdUnreliable(data);
        }
    }
}
