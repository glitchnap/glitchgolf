using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using GlitchConnect.Commands;

namespace GlitchConnect
{
    public class GlitchCommandHandler 
    {
        private static bool initialized = false;
        private static List<System.Type> Commands;
        
        static void Init()
        {
            if(!initialized)
            {
                initialized = true;
                ReflectAllCommands();
            }
        }

        static void ReflectAllCommands()
        {
            // Reflect all types
            Commands = new List<Type>();
            foreach (Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.GetInterfaces().Contains(typeof(IGlitchCommand)))
                    {
                        Commands.Add(type);
                    }
                }
            }

            Commands.Sort((a, b) => a.Name.CompareTo(b.Name));

            foreach (var item in Commands)
            {
                Debug.Log(Commands.IndexOf(item) + " : " + item.Name);
            }
        }

        public static Type GetTypeFromID(int ID)
        {
            Init();
            return Commands[ID];
        }

        public static int GetIDFromType(Type type)
        {
            Init();
            return Commands.IndexOf(type);
        }

        public static string Serialize(IGlitchCommand command)
        {
            var commandJson = JsonUtility.ToJson(command);
            GlitchCommandPackage package = new GlitchCommandPackage() { t = GetIDFromType(command.GetType()), s = commandJson };
            var packageJson = JsonUtility.ToJson(package);
            return packageJson;
        }

        public static IGlitchCommand Deserialize(string data)
        {
            var package = JsonUtility.FromJson<GlitchCommandPackage>(data);
            var type = GetTypeFromID(package.t);
            var commandObject = JsonUtility.FromJson(package.s, type);
            var command = (IGlitchCommand)Convert.ChangeType(commandObject, type);
            return command;
        }
    }

    [Serializable]
    public class GlitchCommandPackage
    {
        // t = TypeID
        public int t;
        // s = SerializedCommand
        public string s;
    }
}
