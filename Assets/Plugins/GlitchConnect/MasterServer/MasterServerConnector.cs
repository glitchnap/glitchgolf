using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using GlitchConnect;


namespace Glitchnap.MS
{
    public enum MasterServerState
    {
        Disconnected,
        Initializing,
        WaitingToBeRegistered,
        Polling,
        Joining,
        WaitingToJoin,
        ConnectedAsServerWaitForComm,
        ConnectedAsClientWaitForComm,
        ConnectedAsServer,
        ConnectedAsClient
    }


    public class MasterServerConnector : MonoBehaviour, IConnector
    {
        #region Singleton

        private static MasterServerConnector instance;

        public static MasterServerConnector Instance { get { return instance; } }

        void Awake()
        {
            if (instance != null)
            {
                forcingDestroyBecauseDuplicate = true;
                DestroyImmediate(gameObject);
                GlitchDebug.Log("[MS] Tried to create 2nd MS Connector, destroying", "MasterServer");
            }
            else
            {
                var lingeringObjects = GameObject.FindObjectsOfType(typeof(MasterServerConnector));

                foreach (MasterServerConnector obj in lingeringObjects)
                {
                    if (obj != this)
                    {
                        Destroy(obj.gameObject);
                    }
                }

                instance = this;
                DontDestroyOnLoad(gameObject);
                state = MasterServerState.Disconnected;
            }
        }

        #endregion

        public MasterServerState state;
        public float TimeOut;

        private string displayName;
        private string serviceType;
        private string peerID;

        private HostData[] hostList;
        private int port;
        private int requiredPeers = 1;
        private bool forcingDestroyBecauseDuplicate;

        private List<GameObject> objectsToDestroy;

        public void Connect(int minPeerConnections, string gameName)
        {
            if (state != MasterServerState.Disconnected)
            {
                GlitchDebug.Log("[MS] Attempting to connect from a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Connect", "MasterServer");
                
                objectsToDestroy = new List<GameObject>();
                port = Random.Range(2000, 5000);
                serviceType = gameName;
                requiredPeers = minPeerConnections;
                displayName = SystemInfo.deviceName + "_" + SystemInfo.deviceUniqueIdentifier;

                peerID = Network.player.ipAddress + "_" + Network.player.port;

                state = MasterServerState.Initializing;
                StartServer();
            }
        }

        private void StartServer()
        {
            if (state != MasterServerState.Initializing)
            {
                GlitchDebug.Log("[MS] Attempting to Start a server from a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Initializing Server", "Masterserver");

                NetworkConnectionError initializeResult = Network.InitializeServer(requiredPeers, port, false);
                if (initializeResult == NetworkConnectionError.NoError)
                {
                    
                }
                else
                {
                    GlitchDebug.Log("[MS] Initialize Server failed, timeout.", "Masterserver");
                    GlitchDebug.Log("ERROR: " + initializeResult.ToString());
                    TimeOut = 5;
                }
            }
        }

        void OnServerInitialized()
        {
            if (state != MasterServerState.Initializing)
            {
                GlitchDebug.Log("[MS] OnServerInitialized from a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                
                GlitchDebug.Log("[MS] Server Initialized, registering host", "Masterserver");
                state = MasterServerState.WaitingToBeRegistered;
                RegisterServer();
                
            }
        }

        private void RegisterServer()
        {
            if (state != MasterServerState.WaitingToBeRegistered && state != MasterServerState.Polling)
            {
                GlitchDebug.Log("[MS] Attempting to Register a server from a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Registering server", GlitchDebug.ERROR);
                MasterServer.RegisterHost(serviceType, displayName);
            }
        }

        void OnFailedToConnectToMasterServer(NetworkConnectionError info)
        {
            if (state != MasterServerState.WaitingToBeRegistered)
            {
                GlitchDebug.Log("[MS] FailedToConnectToMasterServer from a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] FailedToConnectToMasterServer, retrying, error: " + info.ToString(), GlitchDebug.ERROR);
                TimeOut = 5;
            }
        }

        void OnMasterServerEvent(MasterServerEvent msEvent)
        {
            GlitchDebug.Log("[MS] MasterServerEvent: " + msEvent.ToString(), "GCData");
            switch (msEvent)
            {
                case MasterServerEvent.RegistrationSucceeded:
                    if (state != MasterServerState.WaitingToBeRegistered && state != MasterServerState.Polling)
                    {
                        GlitchDebug.Log("[MS] RegistrationSucceeded arrived at faulty state: " + state.ToString(), "Masterserver");
                    }
                    else
                    {
                        GlitchDebug.Log("[MS] RegistrationSucceeded, nothing special " + state.ToString(), "Masterserver");
                        state = MasterServerState.Polling;
                        RefreshHostList();
                    }
                    break;
                case MasterServerEvent.RegistrationFailedGameName:
                    if (state != MasterServerState.WaitingToBeRegistered && state != MasterServerState.Polling)
                    {
                        GlitchDebug.Log("[MS] RegistrationFailedGameName. This is final.", GlitchDebug.ERROR);
                    }
                    break;
                case MasterServerEvent.RegistrationFailedGameType:
                    if (state != MasterServerState.WaitingToBeRegistered && state != MasterServerState.Polling)
                    {
                        GlitchDebug.Log("[MS] RegistrationFailedGameType. This is final.", GlitchDebug.ERROR);
                    }
                    break;
                case MasterServerEvent.RegistrationFailedNoServer:
                    if (state != MasterServerState.WaitingToBeRegistered && state != MasterServerState.Polling)
                    {
                        GlitchDebug.Log("[MS] RegistrationFailedNoServer arrived at faulty state: " + state.ToString(), GlitchDebug.ERROR);
                    }
                    else
                    {
                        GlitchDebug.Log("[MS] RegistrationFailedNoServer, back to initialize, timeout.", GlitchDebug.ERROR);
                        state = MasterServerState.Initializing;
                        TimeOut = 5;
                    }
                    break;
                case MasterServerEvent.HostListReceived:
                    if (state != MasterServerState.Polling)
                    {
                        GlitchDebug.Log("[MS] HostListReceived arrived at faulty state: " + state.ToString(), GlitchDebug.ERROR);
                    }
                    else
                    {
                        hostList = MasterServer.PollHostList();

                        foreach (HostData host in hostList)
                        {
                            GlitchDebug.Log("[MS] Host Found: " + string.Join(".", host.ip) + ":" + host.port + " (" + host.connectedPlayers + " connections)", "GCData");
                        }

                        int lowestPort = int.MaxValue;
                        HostData lowestHost = null;
                        HostData popularHost = null;
                    
                        int doubles = 0;
                        foreach (HostData host in hostList)
                        {
                            if (host.port != port)
                            {
                                if (host.connectedPlayers > 1 && requiredPeers > 1) // only relevant when looking for +2 devices
                                {
                                    GlitchDebug.Log("[MS] Found Popular Host: " + string.Join(".", host.ip) + ":" + host.port + " (" + host.connectedPlayers + " connections)", "MasterServer");
                                    popularHost = host;
                                    break;
                                }
                                if (host.port < lowestPort && host.connectedPlayers <= requiredPeers) // ignore hosts that are 'full'
                                {
                                    lowestPort = host.port;
                                    lowestHost = host;
                                }
                            }
                            else if (host.port == port)
                            {
                                doubles++;
                                if (doubles > 1)
                                {
                                    // bail, we rolled the same number...
                                    GlitchDebug.Log("[MS] Duplicate port detected, rolling a new port", "MasterServer");
                                    Network.Disconnect();
                                    MasterServer.UnregisterHost();
                                    port = Random.Range(2000, 5000);
                                    state = MasterServerState.Initializing;

                                    TimeOut = 5;
                                }
                            }
                        }

                        HostData connectTo = null;
                        if (lowestHost != null && lowestPort < port) connectTo = lowestHost;
                        if (popularHost != null) connectTo = popularHost;
                        
                        if (connectTo != null)
                        {
                            GlitchDebug.Log("[MS] Joining Host: " + string.Join(".", lowestHost.ip) + ":" + lowestHost.port + " (" + lowestHost.connectedPlayers + " connections)", "MasterServer");
                            state = MasterServerState.Joining;
                            JoinServer(connectTo);
                            return;
                        }
                        else
                        {
                            TimeOut = 1;
                        }

                    }
                    break;
            }
        }

        private void RefreshHostList()
        {
            if (state != MasterServerState.Polling)
            {
                GlitchDebug.Log("[MS] Attempting to RefreshHostListfrom a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Polling: " + serviceType, "GCData");
                MasterServer.RequestHostList(serviceType);
            }
        }

        private void JoinServer(HostData hostData)
        {
            if (state != MasterServerState.Joining)
            {
                GlitchDebug.Log("[MS] Attempting to JoinServer a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Attempting to join Server", "Masterserver");
			    
                NetworkConnectionError initializeResult = Network.Connect(hostData);
                if (initializeResult == NetworkConnectionError.NoError)
                {
                    GlitchDebug.Log("[MS] Seemingly joined Server, unregistering", "Masterserver");
                    MasterServer.UnregisterHost();
                    state = MasterServerState.WaitingToJoin;
                }
                else
                {
                    GlitchDebug.Log("[MS] Join Server failed", "Masterserver");
                    GlitchDebug.Log("ERROR: " + initializeResult.ToString());
                    state = MasterServerState.Polling;
                }
            }
        }

        void OnConnectedToServer()
        { 
            if (state != MasterServerState.WaitingToJoin)
            {
                GlitchDebug.Log("[MS] OnConnectedToServer at a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Server Joined Successfully", "Masterserver");
                state = MasterServerState.ConnectedAsClientWaitForComm;

                objectsToDestroy.Add(Network.Instantiate(Resources.Load("_MasterServerCommunicatorClient"), Vector3.zero, Quaternion.identity, 0) as GameObject);
                StartCoroutine(WaitForCommunicator());

                
                if (OnConnectionChanged != null)
                {
                    OnConnectionChanged(new GlitchConnectionEvent() { Type = GlitchConnectionEvent.GlitchConnectionEventType.Connected });
                }
            }
        }

        void OnFailedToConnect(NetworkConnectionError error)
        {
            if (state != MasterServerState.WaitingToJoin)
            {
                GlitchDebug.Log("[MS] OnFailedToConnect on a faulty state: " + state.ToString() + " error: " + error.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] OnFailedToConnect error: " + error.ToString(), GlitchDebug.ERROR);
                Network.Disconnect();
                state = MasterServerState.Initializing;
                TimeOut = 5;
            }
        }

        void OnPlayerConnected()
        {
            if (state != MasterServerState.Polling)
            {
                GlitchDebug.Log("[MS] OnPlayerConnected on a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                GlitchDebug.Log("[MS] Player Connected (" + Network.connections.Length + "/" + requiredPeers + " connections)", "Masterserver");

                if (Network.connections.Length >= requiredPeers)
                {
                    GlitchDebug.Log("[MS] Starting Communicator", "Masterserver");
                    state = MasterServerState.ConnectedAsServerWaitForComm;

                    objectsToDestroy.Add(Network.Instantiate(Resources.Load("_MasterServerCommunicator"), Vector3.zero, Quaternion.identity, 0) as GameObject);
                    StartCoroutine(WaitForCommunicator());

                    MasterServer.UnregisterHost();
                    if (OnConnectionChanged != null)
                    {
                        OnConnectionChanged(new GlitchConnectionEvent() { Type = GlitchConnectionEvent.GlitchConnectionEventType.Connected });
                    }
                }
            }
        }

        void OnDisconnectedFromServer(NetworkDisconnection info)
        {
            if (state != MasterServerState.ConnectedAsClient
                && state != MasterServerState.ConnectedAsClientWaitForComm
                && state != MasterServerState.ConnectedAsServer
                && state != MasterServerState.ConnectedAsServerWaitForComm
                && state != MasterServerState.WaitingToJoin)
            {
                GlitchDebug.Log("[MS] OnDisconnectedFromServer on a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                

                GlitchDebug.Log("[MS] OnDisconnectedFromServer, reason: " + info.ToString(), "Masterserver");
                if (OnConnectionChanged != null)
                {
                    OnConnectionChanged(new GlitchConnectionEvent() { Type = GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost });
                }
                else
                {
                    GlitchDebug.Log("[MS] (nobody listened)", "Masterserver");
                }




                if (state != MasterServerState.WaitingToJoin)
                {
                    Cleanup(false);
                    TimeOut = 5;
                }
                else
                {
                    Cleanup(true);
                }
            }
        }

        void OnPlayerDisconnected(NetworkPlayer player)
        {
            if (state != MasterServerState.ConnectedAsServer && state != MasterServerState.ConnectedAsServerWaitForComm)
            {
                GlitchDebug.Log("[MS] OnPlayerDisconnected on a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                // for now, we're just gonna disconnect... but this only makes sense if there's only 2 devices
                GlitchDebug.Log("[MS] OnPlayerDisconnected", "Masterserver");
                if (OnConnectionChanged != null)
                {
                    OnConnectionChanged(new GlitchConnectionEvent() { Type = GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost });
                }
                else
                {
                    GlitchDebug.Log("[MS] (nobody listened)", "Masterserver");
                }
                
                Cleanup(true);
                state = MasterServerState.Disconnected;
            }
        }

        IEnumerator WaitForCommunicator()
        {
            if (state != MasterServerState.ConnectedAsClientWaitForComm && state != MasterServerState.ConnectedAsServerWaitForComm)
            {
                GlitchDebug.Log("[MS] WaitForCommunicator on a faulty state: " + state.ToString(), GlitchDebug.ERROR);
            }
            else
            {
                // Wait until the Server Communicator, and all clients have created Communicators
                GlitchDebug.Log("[MS] Starting waitforcomm. loop", "masterserver");
                while (true)
                {
                    if (MasterServerCommunicator.Instance != null && MasterServerCommunicatorClient.Instances.Count >= requiredPeers)
                    {
                        MasterServerCommunicator.Instance.OnReceiveData += MasterServerCommunicator_OnReceiveData;

                        foreach (var masterServerComminucatorClient in MasterServerCommunicatorClient.Instances)
                        {
                            masterServerComminucatorClient.OnReceiveData += MasterServerCommunicator_OnReceiveData;
                        }
                        GlitchDebug.Log("[MS] Finished Initializing Communicators", "Masterserver");
                        if (state == MasterServerState.ConnectedAsServerWaitForComm)
                            state = MasterServerState.ConnectedAsServer;
                        if (state == MasterServerState.ConnectedAsClientWaitForComm)
                            state = MasterServerState.ConnectedAsClient;
                        break;
                    }
                    else if (state != MasterServerState.ConnectedAsClientWaitForComm && state != MasterServerState.ConnectedAsServerWaitForComm)
                    {
                        GlitchDebug.Log("[MS] Was waiting in comm loop, but leaving, cause state =" + state.ToString(), GlitchDebug.ERROR);
                        break;
                    }
                    GlitchDebug.Log("[MS] Waiting (" + MasterServerCommunicatorClient.Instances.Count + "/" + requiredPeers + " connections)", "Masterserver");
                    yield return new WaitForSeconds(1);
                }

            }
        }

        private void Update()
        {
            if (TimeOut > 0)
            {
                TimeOut -= Time.deltaTime;
                if (TimeOut <= 0)
                {
                    TimeOut = 0;
                    switch (state)
                    {
                        case MasterServerState.Initializing:
                            StartServer();
                            break;
                        case MasterServerState.WaitingToBeRegistered:
                            RegisterServer();
                            break;
                        case MasterServerState.Polling:
                            RefreshHostList();
                            break;
                        default:
                            GlitchDebug.Log("Timeout was set, but nothing happened at " + state.ToString(), "WARNING");
                            break;
                    }
                }
            }
        }

        void OnDestroy()
        {
            if (!forcingDestroyBecauseDuplicate)
            {
                state = MasterServerState.Disconnected;
                Cleanup(false);
            }
        }

        void Cleanup(bool throwConnectionLostEvent)
        {
            GlitchDebug.Log("[MS] Cleanup", "MasterServer");

            if (throwConnectionLostEvent)
            {
                if (OnConnectionChanged != null)
                {
                    OnConnectionChanged(new GlitchConnectionEvent() { Type = GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost });
                }
            }
            //GlitchConnector.Instance.WasDisconnected(); //make sure it doesn't do stuff on a disconnect detect
            MasterServer.UnregisterHost();

            if (MasterServerCommunicator.Instance != null)
            {
                MasterServerCommunicator.Instance.OnReceiveData -= MasterServerCommunicator_OnReceiveData;
                MasterServerCommunicator.Instance.Destroy();
            }
            if (MasterServerCommunicatorClient.Instances != null)
            {
                foreach (var masterServerCommunicatorClient in MasterServerCommunicatorClient.Instances)
                {
                    masterServerCommunicatorClient.OnReceiveData -= MasterServerCommunicator_OnReceiveData;
                    masterServerCommunicatorClient.Destroy();
                }
                MasterServerCommunicatorClient.Instances.Clear();
            }
        }

        public void Disconnect()
        {
            GlitchDebug.Log("[MS] Disconnect", "Masterserver");
            Network.Disconnect();
            Cleanup(true); 

            state = MasterServerState.Disconnected;
        }

        void MasterServerCommunicator_OnReceiveData(string obj)
        {
            if (OnReceiveData != null)
            {
                var glitchData = new GlitchData() { Data = obj, PeerID = peerID };
                GlitchDebug.Log("[MS] Data received: " + glitchData.Data, "GCData");
                OnReceiveData(glitchData);
            }
        }

        public void SendData(string data)
        {
            GlitchDebug.Log("[MS] SendData: " + data, "GCData");
            MasterServerCommunicator.Instance.SendData(data);
        }

        public void SendDataUnreliably(string data)
        {
            GlitchDebug.Log("[MS] SendDataUnreliably: " + data, "GCData");
            if (Network.isServer)
            {
                MasterServerCommunicator.Instance.StreamData(data);
            }
            else
            {
                var masterServerComminucatorClient = MasterServerCommunicatorClient.Instances.FirstOrDefault(item => item.GetComponent<NetworkView>().isMine);
                if (masterServerComminucatorClient != null)
                {
                    masterServerComminucatorClient.StreamData(data);
                }
            }
        }

        public void SendDataStream(string data)
        {
            SendDataUnreliably(data);
        }

        public System.Collections.Generic.IEnumerable<GlitchPeer> PeerList
        {
            get
            {
                return Network.connections.Select(item => new GlitchPeer(){ PeerID = item.ipAddress + "_" + item.port });
            }
        }

        public event System.Action<GlitchConnectionEvent> OnConnectionChanged;

        public event System.Action<GlitchData> OnReceiveData;

    }
}
