using System;
using System.Collections.Generic;
using UnityEngine;

namespace Glitchnap.MS
{
    public class MasterServerCommunicatorClient : MonoBehaviour
    {
        #region Singleton

        private static List<MasterServerCommunicatorClient> instances;

        public static List<MasterServerCommunicatorClient> Instances
        {
            get
            {
              if(instances == null)
              {
                  instances = new List<MasterServerCommunicatorClient>();
              }

              return instances;
            }
        }

        void Awake()
        {
			GlitchDebug.Log("[MS] Adding Client communicator to list","Masterserver");
           	Instances.Add(this);
            DontDestroyOnLoad(gameObject); 
        }

        #endregion
        
        public event System.Action<string> OnReceiveData;
       
        #region SendDataStream

        public void StreamData(string data)
        {
            //networkView.RPC("ReceiveData", RPCMode.Others, data);
            if (GetComponent<NetworkView>().isMine && isSerializing == false)
            {
               // DebugStreamer.Log("putting data stream in stream");
				currentStreamString += data + "/";
            }
        }

        public string lastStreamString = string.Empty;
        public string currentStreamString = string.Empty;
        private bool isSerializing;

        void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
        {
            isSerializing = true;
            var length = 0;

            var data = string.Empty;
            if (stream.isWriting && lastStreamString.Equals(currentStreamString) == false)
            {
                //DebugStreamer.Log("Sending data stream: "+ currentStreamString.Length+" ..... am I supposed to be in here? "+networkView.isMine);
                //First serialize length
                length = currentStreamString.Length;
                stream.Serialize(ref length);

                //then serialize data
                foreach (var c in currentStreamString.ToCharArray())
                {
                    var character = c;
                    stream.Serialize(ref character);
                }

                lastStreamString = currentStreamString;
				currentStreamString = string.Empty;
            }
            else if (stream.isReading)
            {
                //Get length
                stream.Serialize(ref length);

              
               // DebugStreamer.Log("Receiving data stream: " + length);
              

                //Create string from chars
                var charArr = new char[length];
                for (int i = 0; i < length; i++)
                {
                    char character = ' ';
                    stream.Serialize(ref character);
                    charArr[i] = character;
                }

                data = new String(charArr);

                currentStreamString = data;
               // DebugStreamer.Log("Data Received: " + data);

                //Propagate event.
                if (OnReceiveData != null)
                {
					var commands = data.Split('/');
					
					foreach (var item in commands) {
						if(String.IsNullOrEmpty(item) == false)
						{
							OnReceiveData(item);
						}
					}
                }

            }
            isSerializing = false;
        }
        #endregion

        public void Destroy()
        {
            //Instance = null;
            GlitchDebug.Log("Destroying MasterServerCommunicatorClient", "WARNING");
            if (gameObject != null)
            {
                Network.Destroy(gameObject);
                DestroyImmediate(gameObject);
            }
            Network.RemoveRPCs(Network.player);
            
        }
    }
}
