using System;
using UnityEngine;

namespace Glitchnap.MS
{
    public class MasterServerCommunicator : MonoBehaviour
    {
        #region Singleton

        public static MasterServerCommunicator Instance { get; private set; }

        void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject); 
        }

        #endregion

        #region SendData Reliably
        public void SendData(string data)
        {
            //GetComponent<NetworkView>().RPC("ReceiveData", RPCMode.Others, data);
        }

        //[RPC]
        private void ReceiveData(string data)
        {
            if (OnReceiveData != null)
            {
                OnReceiveData(data);
            }
        }

        public event System.Action<string> OnReceiveData;
        #endregion

        #region SendDataStream

        public void StreamData(string data)
        {
            //networkView.RPC("ReceiveData", RPCMode.Others, data);
            
            if (isSerializing == false)
            {
               // DebugStreamer.Log("putting data stream in stream");
				currentStreamString += data + "/";
            }
        }

        public string lastStreamString = string.Empty;
        public string currentStreamString = string.Empty;
        private bool isSerializing;

        void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
        {
            isSerializing = true;
            int length = 0;

            string data = string.Empty;
            if (stream.isWriting && lastStreamString.Equals(currentStreamString) == false)
            {
               // DebugStreamer.Log("Sending data stream");
                //First serialize length
                length = currentStreamString.Length;
                stream.Serialize(ref length);

                //then serialize data
                foreach(var c in currentStreamString.ToCharArray())
                {
                    var character = c;
                    stream.Serialize(ref character);
                }

                lastStreamString = currentStreamString;
				currentStreamString = string.Empty;
            }
            else if(stream.isReading)
            {
                //DebugStreamer.Log("Receiving data stream");
                //Get length
                stream.Serialize(ref length);

                //Create string from chars
                var charArr = new char[length];
                for(var i = 0; i < length; i++)
                {
                    var character = ' '; 
                    stream.Serialize(ref character);
                    charArr[i] = character;
                }
                
                data = new String(charArr);
                currentStreamString = data;

                //Propagate event.
                if (OnReceiveData != null)
                {
					var commands = data.Split('/');

					foreach (var item in commands) {
						if(String.IsNullOrEmpty(item) == false)
						{
							OnReceiveData(item);
						}
					}
                }
            }
            isSerializing = false;
        }
        #endregion

        public void Destroy()
        {
            GlitchDebug.Log("Destroying MasterServerCommunicator", "WARNING");
            Instance = null;
            if (gameObject != null)
            {
                Network.Destroy(gameObject);
                DestroyImmediate(gameObject);
            }
            Network.RemoveRPCs(Network.player);
            
        }
    }
}
