using UnityEngine;
using System.Collections;

// We need this one for importing our IOS functions
using System.Runtime.InteropServices;

namespace GlitchConnect
{
    public class GlitchMultipeerConnector : IConnector
    {
        public event System.Action<GlitchConnectionEvent> OnConnectionChanged;
        public event System.Action<GlitchData> OnReceiveData;

        public void Connect(int minPeerConnections, string gameName)
        {
#if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            // hook up callbacks
            var callbackGO = GameObject.Find("GlitchMultipeerCallbacks");
            if (callbackGO == null)
            {
                callbackGO = new GameObject("GlitchMultipeerCallbacks");
                callbackGO.AddComponent<GlitchMultipeerCallbacks>();
                GameObject.DontDestroyOnLoad(callbackGO);
            }

            callbackGO.GetComponent<GlitchMultipeerCallbacks>().connector = this;

            // actually connect
            GlitchMultipeerWrapper.GlitchMultipeerConnect(gameName);
#endif
        }

        public void Disconnect()
        {
            #if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            GlitchMultipeerWrapper.GlitchMultipeerDisconnect();
            ChangedState(new GlitchConnectionEvent() { Type = GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost });
#endif
        }

        public void SendData(string data)
        {
            #if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            GlitchMultipeerWrapper.SendDataToAllConnectedPeers(data, true);
#endif
        }

        public void SendDataUnreliably(string data)
        {
            #if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            GlitchMultipeerWrapper.SendDataToAllConnectedPeers(data, false);
            #endif
        }

        public void SendDataStream(string data)
        {
            #if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            SendDataUnreliably(data);
            #endif
        }

        public void ReceivedData(GlitchData data)
        {
            if (OnReceiveData != null)
                OnReceiveData(data);
        }

        public void ChangedState(GlitchConnectionEvent e)
        {
            if (OnConnectionChanged != null)
                OnConnectionChanged(e);
        }

        public System.Collections.Generic.IEnumerable<GlitchPeer> PeerList
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }
    }
}