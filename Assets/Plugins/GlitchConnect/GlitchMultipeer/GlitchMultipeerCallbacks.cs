using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GlitchConnect
{
    public class GlitchMultipeerCallbacks : MonoBehaviour
    {
        #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX

        internal struct GlitchCallbackMessage
        {
            public string method;
            public string data;
        }

        internal static GlitchCallbackMessage[] callbackMessages;
        internal static List<GlitchCallbackMessage> callbackMessageBuffer;
        internal static object messageLock = new object();

        #endif

        public GlitchMultipeerConnector connector;

        void Awake()
        {
            #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX

            callbackMessageBuffer = new List<GlitchCallbackMessage>();

            GlitchMultipeerWrapper.ConnectCallback(((commandName, commandData) =>
                {
                    string command = System.Runtime.InteropServices.Marshal.PtrToStringAuto(commandName);
                    string data = System.Runtime.InteropServices.Marshal.PtrToStringAuto(commandData);

                    if (callbackMessageBuffer != null)
                    {
                        lock (messageLock)
                        {
                            callbackMessageBuffer.Add(new GlitchCallbackMessage{ method = command, data = data });    
                        }
                    }
                }));

            #endif
        }

        void OnDestroy()
        {
            GlitchDebug.Log("WRAPPER DESTROIED!", GlitchDebug.ERROR);
            #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            GlitchMultipeerWrapper.ConnectCallback(null);
            #endif
        }

        void Update()
        {
            #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX

            if (callbackMessageBuffer != null)
            {
                lock (messageLock)
                {
                    callbackMessages = callbackMessageBuffer.ToArray();
                    callbackMessageBuffer.Clear();
                }
                foreach (var item in callbackMessages)
                {
                    SendMessage(item.method, item.data);
                }

                callbackMessages = null;
            }

            #endif
        }

        public void Callback(string message)
        {
            GlitchDebug.Log(message, "MP");
        }

        public void Error(string message)
        {
            GlitchDebug.Log(message, GlitchDebug.ERROR);
        }

        public void DidReceiveData(string dataAndPeer)
        {
            string[] dataSplit = dataAndPeer.Split(new char[]{ ';' }, 2);
            string data = "";
            string peer = dataSplit[0];
            if (dataSplit.Length >= 2)
            {
                data = dataSplit[1];
            }
            var glitchData = new GlitchData(peer, data);

            GlitchDebug.Log("Received data: " + data + " from peer:" + peer, "GCDATA");

            connector.ReceivedData(glitchData);
        }

        public void DidChangeState(string state)
        {	
            GlitchDebug.Log("[MP] DidChangeState: " + state, "Multipeer");
            var e = new GlitchConnectionEvent();
            if (state == "MCSessionStateNotConnected")
            {
                e.Type = GlitchConnectionEvent.GlitchConnectionEventType.ConnectionLost;
                connector.ChangedState(e);
            }
            else if (state == "MCSessionStateConnecting")
            {
                //do nothing
            }
            else if (state == "MCSessionStateConnected")
            {
                e.Type = GlitchConnectionEvent.GlitchConnectionEventType.Connected;
                connector.ChangedState(e);
            }

        }
    }
}