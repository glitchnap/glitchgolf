﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GlitchConnect
{

    public interface IConnector
    {

        void Connect(int minPeerConnections, string gameName);

        void Disconnect();

        void SendData(string data);

        void SendDataUnreliably(string data);

        void SendDataStream(string data);


        IEnumerable<GlitchPeer> PeerList { get; }

        event Action<GlitchConnectionEvent> OnConnectionChanged;
        event Action<GlitchData> OnReceiveData;

    }
}
