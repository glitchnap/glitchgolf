

namespace GlitchConnect
{
    
    public class GlitchData
    {
        public GlitchData()
        {
        }

        public GlitchData(string PeerID, string Data)
        {
            this.PeerID = PeerID;
            this.Data = Data;
        }

        public string PeerID;
        public string Data;
    }
}
