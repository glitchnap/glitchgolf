#import "GlitchMCSession.h"


extern "C" {

	extern const char* unityGameObject;
    extern const char* defaultCallbackMethod;

	@interface GlitchMCManager : NSObject

	+(void) StopAdvertisingAndBrowsing;
	void GlitchMultipeerConnect(const char* serviceType);
	void GlitchMultipeerDisconnect();
    void SendDataToAllConnectedPeers(const char* dataString, BOOL reliably);

    @end
}