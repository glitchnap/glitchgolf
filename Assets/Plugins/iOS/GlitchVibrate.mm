#import <AudioToolbox/AudioToolbox.h>

extern "C"
{

    @implementation GlitchVibrateHelper : NSObject

    void GlitchVibrate()
    {   
    	//1351 is SilentVibeChanged, which is shorter than 4095 kSystemSound_Vibrate
        AudioServicesPlayAlertSound(1351); 
    }

    @end

}