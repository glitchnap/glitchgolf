#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "GlitchMCManager.h"
#import "GlitchMCSession.h"

extern "C"
{
    @implementation GlitchMCSession
    -(id)init{
        UnitySendMessage(unityGameObject,"Error","Creating a new GlitchMCSession");
        self = [super init];
        
        if ([super init]) {
            _session = nil;
        }
        
        return self;
    }

    -(void)session:(MCSession*)session peer:(MCPeerID*)peerID didChangeState:(MCSessionState)state {
        if(state == MCSessionStateNotConnected)
        {
            UnitySendMessage(unityGameObject,"DidChangeState","MCSessionStateNotConnected");
            [GlitchMCManager StopAdvertisingAndBrowsing];
            [self disconnect];
        }
        else if(state == MCSessionStateConnecting)
        {
            UnitySendMessage(unityGameObject,"DidChangeState","MCSessionStateConnecting");
        }
        else if(state == MCSessionStateConnected)
        {
            UnitySendMessage(unityGameObject,"DidChangeState","MCSessionStateConnected");
            [GlitchMCManager StopAdvertisingAndBrowsing];
        }
    }

    -(void)session:(MCSession*)session didReceiveData:(NSData*)data fromPeer:(MCPeerID*)peerID{
        NSString* dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //NSString* result = [NSString stringWithFormat:@"%@%@%@", peerID.displayName, @";", dataString];
        NSString* result = [NSString stringWithFormat:@"%@%@%@", @"x", @";", dataString];
        UnitySendMessage(unityGameObject,"DidReceiveData",[result UTF8String]);
    }

    -(void)session:(MCSession*)session didStartReceivingResourceWithName:(NSString*)resourceName fromPeer:(MCPeerID*)peerID withProgress:(NSProgress*)progress {
        UnitySendMessage(unityGameObject,defaultCallbackMethod,"didStartReceivingResourceWithName");
    }

    -(void)session:(MCSession*)session didFinishReceivingResourceWithName:(NSString*)resourceName fromPeer:(MCPeerID*)peerID atURL:(NSURL*)localURL withError:(NSError*)error {
        UnitySendMessage(unityGameObject,defaultCallbackMethod,"didFinishReceivingResourceWithName");
    }

    -(void)session:(MCSession*)session didReceiveStream:(NSInputStream*)stream withName:(NSString *)streamName fromPeer:(MCPeerID*)peerID {
        UnitySendMessage(unityGameObject,defaultCallbackMethod,"didReceiveStream");
    }

    -(void)setupPeerAndSessionWithPeer:(MCPeerID*)peerID {
    	if(_session)
    	{
    		[self disconnect];
    	}
		UnitySendMessage(unityGameObject,"Error","Creating a new MCSession");
        _session = [[MCSession alloc] initWithPeer:peerID];
        _session.delegate = self;
    }

    -(void)sendDataToAllConnectedPeers:(NSData*)data withMode:(BOOL)reliably {
        if(_session)
        {
	        NSError *err = nil;
	        MCSessionSendDataMode mode;
	        if(reliably)
	        {
	            mode = MCSessionSendDataReliable;
	        }
	        else 
	        {
	            mode = MCSessionSendDataUnreliable;
	        }
	        [_session sendData:data toPeers:[_session connectedPeers] withMode:mode error:&err];
	    }
    }

    -(void)disconnect {
    	if(_session)
    	{
	    	[_session disconnect];
	    	_session.delegate = nil;
	    	_session = nil;
	    }
    }

    -(void)dealloc {
    	_session.delegate = nil;
        //[_session release];
        //[super dealloc];
    }

    @end
}