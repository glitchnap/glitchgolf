#import <MultipeerConnectivity/MultipeerConnectivity.h>

extern "C"
{
	@interface GlitchMCNearbyServiceAdvertiser : NSObject <MCNearbyServiceAdvertiserDelegate> 
	
	@property(nonatomic, strong) MCNearbyServiceAdvertiser* advertiser;

    -(void)startAdvertising:(MCSession*)session withServiceType:(NSString*)serviceType;
    -(void)stopAdvertising;

	@end
}