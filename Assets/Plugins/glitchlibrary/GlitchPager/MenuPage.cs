using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using DG.Tweening;

public class MenuPage : MenuPageBase
{
    public Image fakeBackground;

    void Awake()
    {
        fakeBackground.color = backgroundColor;
    }

    void Start()
    {
        Show();
    }

    public void Hide(bool includeBackground = true)
    {
        // fade out background
        if (includeBackground)
        {
            FadeAssistant.FadeOut(fakeBackground).Play();
        }
    }


    public void Show()
    {
        // fade in background
        FadeAssistant.FadeIn(fakeBackground).Play();
    }
       
}
