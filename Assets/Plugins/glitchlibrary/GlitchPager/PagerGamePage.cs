using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Holoville.HOTween;

public class PagerGamePage : MenuPageBase
{
    public CanvasGroup cardFront;
    public CanvasGroup cardBack;
    private bool frontShowing;
    private Tweener cardFrontTweener;
    private Tweener cardBackTweener;

    void Awake()
    {
        frontShowing = true;
        cardBack.gameObject.SetActive(false);
        cardBack.transform.localScale = cardBack.transform.localScale.withX(0);

        cardFront.gameObject.SetActive(true);
    }

    void Start()
    {

    }

    public void ShowScoreCard()
    {
        if (frontShowing) CardFlip();
    }

    public void CardFlip()
    {
        float duration = 0.35f;
        EaseType easein = EaseType.EaseInQuint;
        EaseType easeout = EaseType.EaseOutQuint;

        if(frontShowing)
        {
            frontShowing = false;
            if(cardFrontTweener != null)  cardFrontTweener.Kill();
            cardFrontTweener = HOTween.To(cardFront.transform, duration, new TweenParms()
                .Prop("localScale", cardFront.transform.localScale.withX(0))
                .Ease(easein)
                .OnComplete(() => { 
                    cardFront.gameObject.SetActive(false);
                })
            );

            if (cardBackTweener != null) cardBackTweener.Kill();
            cardBack.gameObject.SetActive(true);
            cardBackTweener = HOTween.To(cardBack.transform, duration, new TweenParms()
                .Prop("localScale", cardBack.transform.localScale.withX(1))
                .Ease(easeout)
                .Delay(duration)
            );
        }
        else
        {
            frontShowing = true;
            if (cardBackTweener != null) cardBackTweener.Kill();
            cardBackTweener = HOTween.To(cardBack.transform, duration, new TweenParms()
                .Prop("localScale", cardBack.transform.localScale.withX(0))
                .Ease(easein)
                .OnComplete(() =>
                {
                    cardBack.gameObject.SetActive(false);
                })
            );

            if (cardFrontTweener != null) cardFrontTweener.Kill();
            cardFront.gameObject.SetActive(true);
            cardFrontTweener = HOTween.To(cardFront.transform, duration, new TweenParms()
                .Prop("localScale", cardFront.transform.localScale.withX(1))
                .Ease(easeout)
                .Delay(duration)
            );
        }
    }
}
