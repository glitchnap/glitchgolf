﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DebugGridSpaceTransforms : MonoBehaviour {

    public Vector2 spacing;
    public int maxOnX;


    void OnEnable()
    {
        Debug.Log("Farts");
        for (int i = 0; i < transform.childCount; i++)
        {
            var t = transform.GetChild(i);
            t.localPosition = new Vector3((i % maxOnX) * spacing.x, 0, (int)(i / maxOnX) * spacing.y);
        }
    }
}

