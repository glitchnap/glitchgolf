﻿using UnityEngine;
using System.Collections;

public class SimpleConstraints : MonoBehaviour
{
    public Transform targetPosition;
    public Vector3 positionWeights = Vector3.one;

    public Transform targetRotation;

    public Transform direction;
    public Vector3 directionRotation;

    public bool useDirection;

    public Vector3 rotationWeights;

    private Vector3 startPosition;
    //private Quaternion startRotation;

    [ContextMenu("Snap")]
    public void Snap()
    {
        if (targetPosition != null)
        {
            transform.position = targetPosition.position;
        }
    }


    void Start()
    {
        //startRotation = transform.rotation;
    }

    void LateUpdate()
    {
        if (targetPosition != null)
        {
            transform.position = Vector3.Scale(targetPosition.position, positionWeights);
        }

        if (targetRotation != null)
        {
            if (useDirection)
            {
                //transform.LookAt2D(transform.position + (directionTo.position - directionFrom.position).normalized, Vector3.up);
                transform.rotation = Quaternion.Euler(directionRotation) * direction.rotation;
            }
            else
            {
                transform.rotation = Quaternion.Euler(Vector3.Scale(targetRotation.eulerAngles, rotationWeights));
            }
        }
    }
}
