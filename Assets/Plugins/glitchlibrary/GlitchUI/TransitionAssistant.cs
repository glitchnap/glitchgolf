using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class TransitionAssistant : MonoBehaviour
{
    public static float defaultDuration = 0.3f;

    public static Dictionary<int, Vector2> positionDictionary;

    public static void Init()
    {
        if (positionDictionary == null)
            positionDictionary = new Dictionary<int, Vector2>();
    }

    public static Sequence SlideIn(Object obj, Vector2 direction, float duration = -1f)
    {
        return Slide(obj, direction, false, duration);
    }

    public static Sequence SlideOut(Object obj, Vector2 direction, float duration = -1f)
    {
        return Slide(obj, direction, true, duration);
    }

    private static Sequence Slide(Object obj, Vector2 direction, bool isSlideOut, float duration)
    {
        Init();

        Vector2 startPos = Vector2.zero;
        bool found = positionDictionary.TryGetValue(obj.GetHashCode(), out startPos);

        var s = DOTween.Sequence();

        if (duration < 0)
        {
            duration = defaultDuration;
        }

        if (obj is CanvasGroup)
        {
            var group = obj as CanvasGroup;

            if (!found)
            {
                startPos = group.GetComponent<RectTransform>().anchoredPosition;
                positionDictionary.Add(obj.GetHashCode(), startPos);
            }
                
            var endPos = new Vector2((Screen.width + startPos.x) * direction.x, (Screen.height + startPos.y) * direction.y);

            if (isSlideOut)
            {
                s.Insert(0, DOTween.To(() => group.GetComponent<RectTransform>().anchoredPosition, (x) => group.GetComponent<RectTransform>().anchoredPosition = x, endPos, duration));

                group.interactable = false;
                group.blocksRaycasts = false;
            }
            else
            {
                group.GetComponent<RectTransform>().anchoredPosition = startPos;
                s.Insert(0, DOTween.To(() => group.GetComponent<RectTransform>().anchoredPosition, (x) => group.GetComponent<RectTransform>().anchoredPosition = x, endPos, duration).From().OnComplete(() =>
                        {
                            group.interactable = true;
                            group.blocksRaycasts = true;
                        }));
            }

        }
        else if (obj is IGlitchUIElement)
        {
            var t = obj as IGlitchUIElement;

            // fade if canvas group
            var group = t.GetCanvasGroup();
            if (group != null)
            {
                s.Insert(0, Slide(group, direction, isSlideOut, duration)
                );
                return s; 
            }
        }
        else if (obj is GlitchUIElement)
        {
            var t = obj as GlitchUIElement;

            // fade if canvas group
            var group = t.GetComponent<CanvasGroup>();
            if (group != null)
            {
                s.Insert(0, Slide(group, direction, isSlideOut, duration)
                );
                return s; 
            }
        }
        // Laziness, you can dump in Transforms and Gameobjects as well as Canvasgroups
        else if (obj is Transform)
        {
            var t = obj as Transform;

            // fade if canvas group
            var group = t.GetComponent<CanvasGroup>();
            if (group != null)
            {
                s.Insert(0,
                    Slide(group, direction, isSlideOut, duration)
                );
                return s; 
            }
        }
        else if (obj is GameObject)
        {
            var g = obj as GameObject;

            // fade if canvas group
            var group = g.GetComponent<CanvasGroup>();
            if (group != null)
            {
                s.Insert(0,
                    Slide(group, direction, isSlideOut, duration)
                );
                return s; 
            }
        }

        return s;
    }
}
