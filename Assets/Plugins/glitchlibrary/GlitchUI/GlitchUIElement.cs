using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using DG.Tweening;

[Serializable]
public struct GlitchUITransition
{
    public GlitchUIElement element;
    public GlitchUITransitionTypes transition;
}

[RequireComponent(typeof(CanvasGroup))]
public class GlitchUIElement : MonoBehaviour, IGlitchUIElement
{
    [SerializeField]
    private bool HideOnAwake;

    [SerializeField]
    [InspectorNote("Glitch UI Element State")]
    private GlitchUIElementState _glitchUIElementState;

    public GlitchUIElementState glitchUIElementState
    {
        get
        {
            return _glitchUIElementState;
        }

        set
        {
            _glitchUIElementState = value;
        }
    }

    protected virtual void Awake()
    {
        if (HideOnAwake && gameObject.activeSelf)
        {
            this.Hide(false);
        }
    }

    public virtual void OnShow() { } //no implementation
    public virtual void OnHide() { } //no implementation
    public virtual void OnToggle() { } //no implementation
    public bool isShowing { get { return glitchUIElementState.isShowing; } }
}
