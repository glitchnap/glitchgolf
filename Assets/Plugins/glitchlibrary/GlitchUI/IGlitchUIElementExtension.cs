﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

public static class IGlitchUIElementExtension
{
    public static CanvasGroup GetCanvasGroup(this IGlitchUIElement element)
    {
        var mb = element as MonoBehaviour;
        if (mb != null)
        {
            var cg = mb.GetComponent<CanvasGroup>();
            if (cg == null)
            {
                cg = mb.gameObject.AddComponent<CanvasGroup>();
            }

            return cg;
        }

        var uib = element as UIBehaviour;
        if (uib != null)
        {
            var cg = uib.GetComponent<CanvasGroup>();
            if (cg == null)
            {
                cg = uib.gameObject.AddComponent<CanvasGroup>();
            }

            return cg;
        }

        Debug.LogError("Something went wrong, we should never get here!");
        return null;
    }

    public static void Toggle(this IGlitchUIElement element, bool animate = true, GlitchUITransitionTypes transitionShow = GlitchUITransitionTypes.Fade, GlitchUITransitionTypes transitionHide = GlitchUITransitionTypes.Fade)
    {
        element.OnToggle();
        if (element.glitchUIElementState.isShowing)
        {
            Hide(element, animate, transitionHide);
        }
        else
        {
            Show(element, animate, transitionShow);
        }
    }

    public static void Hide(this IGlitchUIElement element, bool animate = true, GlitchUITransitionTypes transition = GlitchUITransitionTypes.Fade)
    {
        if (element.glitchUIElementState.isShowing)
        {
            element.OnHide();
        }
        element.glitchUIElementState = new GlitchUIElementState { isShowing = false, isTransitioning = true };

        if (transition == GlitchUITransitionTypes.Fade)
        {
            DoFadeOutTransition(element, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideLeft)
        {
            DoSlideOutTransition(element, Vector2.left, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideRight)
        {
            DoSlideOutTransition(element, Vector2.right, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideUp)
        {
            DoSlideOutTransition(element, Vector2.up, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideDown)
        {
            DoSlideOutTransition(element, Vector2.down, animate);
        }
    }

    public static void Show(this IGlitchUIElement element, bool animate = true, GlitchUITransitionTypes transition = GlitchUITransitionTypes.Fade)
    {
        if (!element.glitchUIElementState.isShowing)
        {
            element.OnShow();
        }
        element.glitchUIElementState = new GlitchUIElementState { isShowing = true, isTransitioning = true };

        if (transition == GlitchUITransitionTypes.Fade)
        {
            DoFadeInTransition(element, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideLeft)
        {
            DoSlideInTransition(element, Vector2.left, animate);
            DoFadeInTransition(element, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideRight)
        {
            DoSlideInTransition(element, Vector2.right, animate);
            DoFadeInTransition(element, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideUp)
        {
            DoSlideInTransition(element, Vector2.up, animate);
            DoFadeInTransition(element, animate);
        }
        else if (transition == GlitchUITransitionTypes.SlideDown)
        {
            DoSlideInTransition(element, Vector2.down, animate);
            DoFadeInTransition(element, animate);
        }
    }

    private static void DoFadeOutTransition(IGlitchUIElement element, bool animate = true)
    {
        FadeAssistant.FadeOut(element.GetCanvasGroup(), animate ? -1 : 0)
            .Play()
            .OnComplete(() =>
            {
                element.glitchUIElementState = new GlitchUIElementState { isShowing = element.glitchUIElementState.isShowing, isTransitioning = false };
            });
    }

    private static void DoFadeInTransition(IGlitchUIElement element, bool animate = true)
    {
        var cg = GetCanvasGroup(element);

        if (!cg.gameObject.activeSelf)
        {
            cg.gameObject.SetActive(true);
        }
        FadeAssistant.FadeIn(element.GetCanvasGroup(), animate ? -1 : 0)
            .Play()
            .OnComplete(() =>
            {
                element.glitchUIElementState = new GlitchUIElementState { isShowing = element.glitchUIElementState.isShowing, isTransitioning = false };
            });
    }

    private static void DoSlideOutTransition(IGlitchUIElement element, Vector2 direction, bool animate = true)
    {
        TransitionAssistant.SlideOut(element.GetCanvasGroup(), direction, animate ? -1 : 0)
            .Play()
            .OnComplete(() =>
            {
                element.glitchUIElementState = new GlitchUIElementState { isShowing = element.glitchUIElementState.isShowing, isTransitioning = false };
            });
    }

    private static void DoSlideInTransition(IGlitchUIElement element, Vector2 direction, bool animate = true)
    {
        var cg = GetCanvasGroup(element);

        if (!cg.gameObject.activeSelf)
        {
            cg.gameObject.SetActive(true);
        }
        TransitionAssistant.SlideIn(element.GetCanvasGroup(), direction, animate ? -1 : 0)
            .Play()
            .OnComplete(() =>
            {
                element.glitchUIElementState = new GlitchUIElementState { isShowing = element.glitchUIElementState.isShowing, isTransitioning = false };
            });
    }
}
