﻿using System;

public enum GlitchUITransitionTypes
{
    Fade,
    SlideLeft,
    SlideRight,
    SlideUp,
    SlideDown,
}

[Serializable]
public struct GlitchUIElementState
{
    [ReadOnly]
    public bool isShowing;
    [ReadOnly]
    public bool isTransitioning;
}

public interface IGlitchUIElement
{
    GlitchUIElementState glitchUIElementState { get; set; }

    void OnShow();
    void OnHide();
    void OnToggle();
}
