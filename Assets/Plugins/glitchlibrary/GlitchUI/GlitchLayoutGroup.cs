using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
public class GlitchLayoutGroup : MonoBehaviour
{
    public float spacing;
    public bool preserveSizes;
    public float topPadding;
    public bool adjustParentHeight;
    public bool horizontal;
    public bool dynamic;

    private int lastChildCount;
    private float lastHeight;

    void Update()
    {
        if(!Application.isPlaying || dynamic)
        {
            if (!Application.isPlaying || transform.childCount != lastChildCount)
            {
                lastChildCount = transform.childCount;

                float counter = topPadding;
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (!horizontal)
                    {
                        float height = 0;
                        if (preserveSizes)
                        {
                            height = transform.GetChild(i).GetComponent<RectTransform>().sizeDelta.y;
                        }
                        transform.GetChild(i).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -counter);
                        counter += spacing + height;
                    }
                    else
                    {
                        float width = 0;
                        if (preserveSizes)
                        {
                            width = transform.GetChild(i).GetComponent<RectTransform>().sizeDelta.x;
                        }
                        transform.GetChild(i).GetComponent<RectTransform>().anchoredPosition = new Vector2(counter, 0);
                        counter += spacing + width;
                    }
                }
                lastHeight = counter;

                if (adjustParentHeight)
                {
                    var size = transform.GetComponent<RectTransform>().sizeDelta;
                    if (!horizontal)
                    {
                        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x, counter);
                    }
                    else
                    {
                        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(counter, size.y);
                    }
                }
            }
        }
    }

    public float GetHeight()
    {
        return lastHeight;
    }
}
