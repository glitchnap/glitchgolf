using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;

[RequireComponent(typeof(Image), typeof(CanvasGroup))]
[ExecuteInEditMode]
public class GlitchUIButtonCustom : GlitchUIButton, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent OnButtonPressed;
    public UnityEvent OnButtonReleased;

    public new void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if (OnButtonPressed != null) OnButtonPressed.Invoke();
        //DebugExtension.Blip("FUCK");
    }

    public new void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        if (OnButtonReleased != null) OnButtonReleased.Invoke();
        //DebugExtension.Blip("FACK");
    }

}
