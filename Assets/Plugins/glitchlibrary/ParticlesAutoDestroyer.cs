using UnityEngine;
using System.Collections;

public class ParticlesAutoDestroyer : MonoBehaviour 
{ 
     private void Start()
     {
         Destroy(gameObject, GetComponent<ParticleSystem>().duration); 
     }
 }
