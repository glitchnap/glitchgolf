
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using Object = UnityEngine.Object;

public class SpriteCopy : EditorWindow
{

    Object copyFrom;
    Object copyTo;
    bool copyAllSettings;

    // Creates a new option in "Windows"
    [MenuItem("GlitchLibrary/Copy Spritesheet pivots and slices")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        SpriteCopy window = (SpriteCopy)EditorWindow.GetWindow(typeof(SpriteCopy));
        window.Show();
    }


    [MenuItem("GlitchLibrary/Set all textures to TrueColor / max 1024")]
    static void FindTextures()
    {
        foreach (var m in AssetDatabase.GetAllAssetPaths())
        {
            if (m.Contains("Plugins")) continue;
            try
            {
                TextureImporter ti = AssetImporter.GetAtPath(m) as TextureImporter;
                bool change = false;
                if(ti.textureFormat != TextureImporterFormat.AutomaticTruecolor)
                {
                    ti.textureFormat = TextureImporterFormat.AutomaticTruecolor;
                    change = true;
                }
                if (ti.maxTextureSize > 1024)
                {
                    ti.maxTextureSize = 1024;
                    change = true;
                }

                if (change)
                {
                    AssetDatabase.ImportAsset(m, ImportAssetOptions.ForceUpdate);
                    Debug.Log("Set color format and size for: " + m);
                }
            }
            catch(Exception)
            {

            }
        }
    }


     



    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Copy from:", EditorStyles.boldLabel);
        copyFrom = EditorGUILayout.ObjectField(copyFrom, typeof(Texture2D), false, GUILayout.Width(220));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Copy to:", EditorStyles.boldLabel);
        copyTo = EditorGUILayout.ObjectField(copyTo, typeof(Texture2D), false, GUILayout.Width(220));
        GUILayout.EndHorizontal();

        GUILayout.Space(25f);
        if (GUILayout.Button("Copy pivots and slices"))
        {
            CopyPivotsAndSlices();
        }
        copyAllSettings = GUILayout.Toggle(copyAllSettings, "Copy all import settings?");
    }

    void CopyPivotsAndSlices()
    {
        if (!copyFrom || !copyTo)
        {
            Debug.Log("Missing one object");
            return;
        }

        if (copyFrom.GetType() != typeof(Texture2D) || copyTo.GetType() != typeof(Texture2D))
        {
            Debug.Log("Cant convert from: " + copyFrom.GetType() + "to: " + copyTo.GetType() + ". Needs two Texture2D objects!");
            return;
        }

        string copyFromPath = AssetDatabase.GetAssetPath(copyFrom);
        TextureImporter ti1 = AssetImporter.GetAtPath(copyFromPath) as TextureImporter;
        ti1.isReadable = true;

        string copyToPath = AssetDatabase.GetAssetPath(copyTo);
        TextureImporter ti2 = AssetImporter.GetAtPath(copyToPath) as TextureImporter;
        ti2.isReadable = true;

        if (ti1.spriteImportMode == SpriteImportMode.Multiple)
        {
            ti2.spriteImportMode = SpriteImportMode.Multiple;

            List<SpriteMetaData> newData = new List<SpriteMetaData>();

            Debug.Log("Amount of slices found: " + ti1.spritesheet.Length);

            for (int i = 0; i < ti1.spritesheet.Length; i++)
            {
                SpriteMetaData d = ti1.spritesheet[i];
                newData.Add(d);
            }
            ti2.spritesheet = newData.ToArray();
        }

        if (copyAllSettings)
        {
            ti2.spritePixelsPerUnit = ti1.spritePixelsPerUnit;
            ti2.spritePivot = ti1.spritePivot;
            ti2.maxTextureSize = ti1.maxTextureSize;
            ti2.textureFormat = ti1.textureFormat;
            ti2.spriteImportMode = ti1.spriteImportMode;
        }

        AssetDatabase.ImportAsset(copyToPath, ImportAssetOptions.ForceUpdate);

    }
}

