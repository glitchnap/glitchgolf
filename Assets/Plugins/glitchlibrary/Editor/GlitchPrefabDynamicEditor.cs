using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.SceneManagement;
using qtools.qhierarchy;
using System.Reflection;
using System;
using System.Text.RegularExpressions;

[CanEditMultipleObjects]
[CustomEditor(typeof(GlitchPrefabDynamic))]
public class GlitchPrefabDynamicEditor : Editor {

    private const BindingFlags bflags = BindingFlags.Public | BindingFlags.Instance;

    private int selectedChild;
    private List<string> childrenTreeNames;
    private List<Transform> childrenTreeTransforms;
    private int counter;
    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var t = (target as GlitchPrefabDynamic);
        
        // Show dynamic fields
        // repopulate tree
        childrenTreeNames = new List<string>();
        childrenTreeTransforms = new List<Transform>();
        counter = 0;
        GetChildrenAsTree(t.prefab.transform);

        // Show Transform Hierarchy of Prefab 
        var tempSelectedChild = EditorGUILayout.Popup("Transform", selectedChild, childrenTreeNames.ToArray());
        if (tempSelectedChild != selectedChild)
        {
            selectedChild = tempSelectedChild;
            t.dynamicField.obj = null;
            if(selectedChild >= 0 && childrenTreeTransforms.Count > 0)
            {
                t.dynamicField.obj = childrenTreeTransforms[selectedChild].gameObject;
            }
            t.dynamicField.comp = null;
            t.dynamicField.compType = "";
            t.dynamicField.subproperty = "";
            t.dynamicField.property = "";
        }
        
        EditorGUI.indentLevel++;

        // get the object's components
        if (t.dynamicField.obj == null)
        {
            EditorGUILayout.HelpBox("Choose a GameObject", MessageType.Warning);
        }
        else
        {
            Component[] components = t.dynamicField.obj.GetComponents<Component>();
            // get the index of the currently selected component
            int componentIndex = 0;
            if (components.Length > 0
                && t.dynamicField.comp != null
                && System.Array.IndexOf(components, t.dynamicField.comp) >= 0)
            {
                componentIndex = System.Array.IndexOf(components, t.dynamicField.comp);
            }

            // make a list of names to display
            string[] componentsLabels = new string[components.Length];
            for (int i = 0; i < components.Length; i++)
            {
                string s = components[i].GetType().ToString();
                s = s.Split('.')[s.Split('.').Length - 1];
                componentsLabels[i] = i + ": " + s + (components[i] == target ? " (Self)" : "");
            }
            componentIndex = EditorGUILayout.Popup("Component", componentIndex, componentsLabels);
            if (componentIndex >= 0)
            {
                if (t.dynamicField.comp != components[componentIndex])
                {
                    // Clear property, subproperty if property changed
                    t.dynamicField.subproperty = "";
                    t.dynamicField.property = "";
                }
                t.dynamicField.comp = components[componentIndex];
                t.dynamicField.compType = t.dynamicField.comp.GetType().ToString();
            }
            else
            {
                Debug.LogWarning("Can't find target component anymore, did the target object change? original " + t.dynamicField.comp, this);
            }

            // get the object's components
            if (t.dynamicField.comp == null)
            {
                EditorGUILayout.HelpBox("Choose a Component", MessageType.Warning);
            }
            else
            {
                // properties
                string[] properties = GetComponentProperties(t.dynamicField.comp).ToArray();
                string[] propertiesLabels = new string[properties.Length];
                if (properties.Length > 0)
                {
                    EditorGUI.indentLevel++;
                    for (int i = 0; i < properties.Length; i++)
                    {
                        propertiesLabels[i] = string.Format("{0} ({1})", properties[i], GetComponentValue(t.dynamicField.comp, properties[i]).GetType().Name);
                    }

                    int propertyIndex = EditorGUILayout.Popup("Property", System.Array.IndexOf(properties, t.dynamicField.property), propertiesLabels);
                    if (propertyIndex >= 0)
                    {
                        if (t.dynamicField.property != properties[propertyIndex])
                        {
                            // Clear subproperty if property changed
                            t.dynamicField.subproperty = "";
                        }
                        t.dynamicField.property = properties[propertyIndex];
                        t.dynamicField.propertyType = GetComponentType(t.dynamicField.comp, t.dynamicField.property);
                        t.dynamicField.fieldValue = GetComponentValue(t.dynamicField.comp, t.dynamicField.property);
                    }
                    else
                    {
                        //gs.singleTargetProperty = "";
                        //Debug.LogWarning("Can't find target property anymore, did the target object change? original value: " + changeComponent.singleTargetProperty, this);
                        EditorGUILayout.HelpBox("Choose a property", MessageType.Warning);
                    }

                    if (!String.IsNullOrEmpty(t.dynamicField.property))
                    {
                        // subproperties
                        object propertyObj = GetComponentValue(t.dynamicField.comp, t.dynamicField.property);
                        List<string> subproperties = GetComponentProperties(propertyObj);
                        List<string> subpropertiesLabels = new List<string>();

                        if (subproperties.Count > 0)
                        {
                            EditorGUI.indentLevel++;

                            subproperties.Insert(0, "");
                            subpropertiesLabels.Add("None");
                            subproperties.RemoveAt(1);  // removing 'broken' Item property
                            for (int i = 1; i < subproperties.Count; i++)
                            {
                                subpropertiesLabels.Add(string.Format("{0} ({1})", subproperties[i].ToString(), ""));
                            }
                            int subpropertyIndex = EditorGUILayout.Popup("Subproperty", subproperties.IndexOf(t.dynamicField.subproperty), subpropertiesLabels.ToArray());
                            // EditorGuiLayout.Popup autoselects the first one
                            if (subpropertyIndex > 0)
                            {
                                t.dynamicField.subproperty = subproperties[subpropertyIndex];
                                t.dynamicField.subpropertyType = GetComponentType(propertyObj, t.dynamicField.subproperty);
                                t.dynamicField.fieldValue = GetComponentValue(propertyObj, t.dynamicField.subproperty);
                            }
                            else 
                            {
                                t.dynamicField.subproperty = "";
                                t.dynamicField.subpropertyType = null;
                                //Debug.LogWarning("Can't find target subproperty anymore, did the target object change? original value: " + subproperty, this);
                            }

                            EditorGUI.indentLevel--;
                        }
                        else
                        {
                            t.dynamicField.subproperty = "";
                        }
                    }
                    EditorGUI.indentLevel--;
                }
                EditorGUI.indentLevel--;
            }
        }

        // Show field value
        if (!String.IsNullOrEmpty(t.dynamicField.subproperty))
        {
            // do subproperty
            t.dynamicField.fieldValue = ShowTypedField(t.dynamicField.subpropertyType, t.dynamicField.fieldValue);
        }
        else if (!String.IsNullOrEmpty(t.dynamicField.property))
        {
            // do property
            t.dynamicField.fieldValue = ShowTypedField(t.dynamicField.propertyType, t.dynamicField.fieldValue);
        }
        else
        {
            // do nothing
        }


        
        // Show button
        if (!Application.isPlaying)
        {
            if (GUILayout.Button("Refresh GlitchPrefab (" + targets.Length + ")"))
            {
                foreach (var item in targets)
                {
                    var gp = (item as GlitchPrefabDynamic);
                    ClearGlitchPrefab(gp);
                    InstantiateGlitchPrefab(gp);
                }
            }
        }

        
    }

    public static void ClearGlitchPrefab(GlitchPrefabDynamic glitchPrefab)
    {
        glitchPrefab.transform.Clear();
    }

    public static void InstantiateGlitchPrefab(GlitchPrefabDynamic glitchPrefab)
    {
        if (glitchPrefab.prefab == null) 
        {
            DebugExtension.BlipError(glitchPrefab,"Can't instantiate a GlitchPrefab because it has no prefab reference.");
            return;
        }

        GameObject go = null;
        if (UnityEditor.PrefabUtility.GetPrefabType(glitchPrefab.prefab) == UnityEditor.PrefabType.Prefab)
        {
            go = UnityEditor.PrefabUtility.InstantiatePrefab(glitchPrefab.prefab) as GameObject;
        }
        else
        {
            go = Instantiate(glitchPrefab.prefab) as GameObject;
        }

        go.transform.SetParent(glitchPrefab.transform);

        go.transform.localPosition = glitchPrefab.prefab.transform.localPosition;
        go.transform.localRotation = glitchPrefab.prefab.transform.localRotation;
        go.transform.localScale = glitchPrefab.prefab.transform.localScale;
        go.name = go.name.Replace("(Clone)", "");
        
        
        if (glitchPrefab.breakPrefabInstance)
        {
            UnityEditor.PrefabUtility.DisconnectPrefabInstance(go);
        }
        else
        {
            // Lock All children
            if (glitchPrefab.autolock)
            {
                var scene = EditorSceneManager.GetActiveScene();
                var objectList = QHierarchy.createObjectListInScene(scene);
                var goList = new List<GameObject>();
                QHierarchy.getGameObjectListRecursive(ref goList, go);
                QHierarchy.setLock(objectList, goList, true);
            }
        }

        if (glitchPrefab.inheritLayer)
        {
            go.SetLayer(glitchPrefab.gameObject.layer);
        }

        foreach (var nestedGlitchPrefab in go.GetComponentsInChildren<GlitchPrefabDynamic>(true))
        {
            ClearGlitchPrefab(nestedGlitchPrefab);
            InstantiateGlitchPrefab(nestedGlitchPrefab);
        }

        DrawIcon(go);
        DrawIcon(glitchPrefab.gameObject);
    }

    //[UnityEditor.MenuItem("GlitchLibrary/GlitchPrefab Refresh All")]
    static void RefreshAllAndDestroyChildren()
    {
        // Clear all GlitchPrefabs
        foreach (var root in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            foreach (var glitchPrefab in root.GetComponentsInChildren<GlitchPrefabDynamic>(true).ToList())
            {
                if(glitchPrefab!= null)
                    ClearGlitchPrefab(glitchPrefab);
            }
        }

        // Instantiate all
        int counter = 0;
        foreach (var root in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            foreach (var glitchPrefab in root.GetComponentsInChildren<GlitchPrefabDynamic>(true))
            {
                InstantiateGlitchPrefab(glitchPrefab);
                counter++;
            }
        }

        if (!Application.isPlaying)
        {
            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
        }
        Debug.Log("Refreshed " + counter + " GlitchPrefabs.");
    }


    public static void DrawIcon(GameObject gameObject)
    {
        var icon = EditorGUIUtility.FindTexture("PrefabNormal Icon");

        var egu = typeof(EditorGUIUtility);
        var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;

        var args = new object[] { gameObject, icon };


        var setIcon = egu.GetMethod(
            "SetIconForObject", 
            flags, 
            null, 
            new Type[] { typeof(UnityEngine.Object), typeof(Texture2D) }, 
            null);
        setIcon.Invoke(null, args);

        //        var editorUtilityType = typeof(EditorUtility);
        //        editorUtilityType.InvokeMember("ForceReloadInspectors", flags, null, null, null);

    }

    public static List<string> GetComponentProperties(object source)
    {
        List<string> props = new List<string>();
        if (source != null)
        {
            foreach (PropertyInfo pi in source.GetType().GetProperties(bflags))
            {
                if (isTypeSupported(pi.PropertyType) && pi.CanWrite && !pi.GetSetMethod().IsStatic && !pi.IsDefined(typeof(System.ObsoleteAttribute), true))
                    props.Add(pi.Name);
            }
            foreach (FieldInfo fi in source.GetType().GetFields(bflags))
            {
                if (isTypeSupported(fi.FieldType) && fi.IsPublic && !fi.IsLiteral && !fi.IsStatic)
                    props.Add(fi.Name);
            }

        }
        return props;
    }

    public static object GetComponentValue(object source, string property)
    {
        object val = null;
        if (source != null && property != null)
        {
            System.Type sourceType = source.GetType();
            PropertyInfo propertyInfo = sourceType.GetProperty(property, bflags);
            if (propertyInfo != null)
                return propertyInfo.GetValue(source, null);
            FieldInfo fieldInfo = sourceType.GetField(property, bflags);
            if (fieldInfo != null)
                return fieldInfo.GetValue(source);
        }
        return val;
    }

    public static Type GetComponentType(object source, string property)
    {
        if (source != null && property != null)
        {
            System.Type sourceType = source.GetType();
            PropertyInfo propertyInfo = sourceType.GetProperty(property, bflags);
            
            if (propertyInfo != null)
                return propertyInfo.PropertyType;
            FieldInfo fieldInfo = sourceType.GetField(property, bflags);
            
            if (fieldInfo != null)
                return fieldInfo.FieldType;
        }
        return typeof(object);
    }


    public static bool isTypeSupported(System.Type PropertyType)
    {
        return true;
        /*
        System.Type[] supportedTypes = {
                typeof(Vector2),
                typeof(Vector3),
                typeof(Vector4),
                typeof(float),
                typeof(double),
                typeof(int),
                typeof(Color)
            };
        return supportedTypes.Contains(PropertyType);
         * */
    }

    public void GetChildrenAsTree(Transform obj, int indent=0)
    {
        var pad = "";
        for (int j = 0; j < indent; j++)
        {
            pad = "   " + pad;
        }
        childrenTreeNames.Add((counter++) + ": " + pad + " " + obj.name);
        childrenTreeTransforms.Add(obj);
        for (int i = 0; i < obj.childCount; i++)
        {
            var child = obj.GetChild(i);
            GetChildrenAsTree(child, indent + 1);
        }
    }

    public object ShowTypedField(Type type, object value)
    {
        var label = "value (" + type.ToString() + ")";

        if (type == typeof(String) || type == typeof(string))
        {
            if (value == null) value = "";
            return EditorGUILayout.TextField(label, value.ToString());
        }
        else if (type == typeof(float))
        {
            if (value == null) value = 0f;
            var temp = EditorGUILayout.FloatField(label, (float)value);
            return temp;
        }
        else if (type == typeof(int))
        {
            if (value == null) value = 0;
            return EditorGUILayout.IntField(label, (int)value);
        }
        else if (type == typeof(bool))
        {
            if (value == null) value = 0;
            return EditorGUILayout.Toggle(label, (bool)value);
        }
        else if (type == typeof(Vector3))
        {
            if (value == null) value = Vector3.zero;
            return EditorGUILayout.Vector3Field(label, (Vector3)value);
        }
        else if (type == typeof(Vector2))
        {
            if (value == null) value = Vector2.zero;
            return EditorGUILayout.Vector2Field(label, (Vector2)value);
        }
        else
        {
            if (value == null) value = null;
            EditorGUILayout.TextField(label + " Unsupported" , value.ToString());
        }

        
        return null;
    }
}
