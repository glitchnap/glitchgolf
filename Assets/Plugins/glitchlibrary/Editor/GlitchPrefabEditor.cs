using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.SceneManagement;
using qtools.qhierarchy;
using System.Reflection;
using System;
using System.Text.RegularExpressions;

[CanEditMultipleObjects]
[CustomEditor(typeof(GlitchPrefab))]
public class GlitchPrefabEditor : Editor
{

    private int counter;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        // Show button
        if (!Application.isPlaying)
        {
            if (GUILayout.Button("Refresh GlitchPrefab (" + targets.Length + ")"))
            {
                foreach (var item in targets)
                {
                    var gp = (item as GlitchPrefab);
                    ClearGlitchPrefab(gp);
                    InstantiateGlitchPrefab(gp);
                }
            }
        }
    }

    public static void ClearGlitchPrefab(GlitchPrefab glitchPrefab)
    {
        if(glitchPrefab != null)
            glitchPrefab.transform.Clear();
    }

    public static void InstantiateGlitchPrefab(GlitchPrefab glitchPrefab)
    {
        if (glitchPrefab == null || glitchPrefab.prefab == null)
        {
            DebugExtension.BlipError(glitchPrefab, "Can't instantiate a GlitchPrefab because it has no prefab reference.");
            return;
        }

        GameObject go = null;
        if (UnityEditor.PrefabUtility.GetPrefabType(glitchPrefab.prefab) == UnityEditor.PrefabType.Prefab)
        {
            go = UnityEditor.PrefabUtility.InstantiatePrefab(glitchPrefab.prefab) as GameObject;
        }
        else
        {
            go = Instantiate(glitchPrefab.prefab) as GameObject;
        }

        go.transform.SetParent(glitchPrefab.transform);

        go.transform.localPosition = glitchPrefab.prefab.transform.localPosition;
        go.transform.localRotation = glitchPrefab.prefab.transform.localRotation;
        go.transform.localScale = glitchPrefab.prefab.transform.localScale;
        go.name = go.name.Replace("(Clone)", "");
        
        
        if (glitchPrefab.breakPrefabInstance)
        {
            UnityEditor.PrefabUtility.DisconnectPrefabInstance(go);
        }
        else
        {
            // Lock All children

            var scene = EditorSceneManager.GetActiveScene();
            var objectList = QHierarchy.createObjectListInScene(scene);
            var goList = new List<GameObject>();
            QHierarchy.getGameObjectListRecursive(ref goList, go);
            QHierarchy.setLock(objectList, goList, true);    
        }

        if (glitchPrefab.inheritLayer)
        {
            go.SetLayer(glitchPrefab.gameObject.layer);
        }

        foreach (var nestedGlitchPrefab in go.GetComponentsInChildren<GlitchPrefab>(true))
        {
            ClearGlitchPrefab(nestedGlitchPrefab);
            InstantiateGlitchPrefab(nestedGlitchPrefab);
        }

        DrawIcon(go);
        DrawIcon(glitchPrefab.gameObject);
    }

    [UnityEditor.MenuItem("GlitchLibrary/GlitchPrefab Refresh All")]
    static void RefreshAllAndDestroyChildren()
    {
        // Clear all GlitchPrefabs
        foreach (var root in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            foreach (var glitchPrefab in root.GetComponentsInChildren<GlitchPrefab>(true).ToList())
            {
                if (glitchPrefab != null)
                    ClearGlitchPrefab(glitchPrefab);
            }
        }

        // Instantiate all
        int counter = 0;
        foreach (var root in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            foreach (var glitchPrefab in root.GetComponentsInChildren<GlitchPrefab>(true))
            {
                InstantiateGlitchPrefab(glitchPrefab);
                counter++;
            }
        }

        if (!Application.isPlaying)
        {
            UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
        }
        Debug.Log("Refreshed " + counter + " GlitchPrefabs.");
    }


    public static void DrawIcon(GameObject gameObject)
    {
        var icon = EditorGUIUtility.FindTexture("PrefabNormal Icon");

        var egu = typeof(EditorGUIUtility);
        var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;

        var args = new object[] { gameObject, icon };


        var setIcon = egu.GetMethod(
                          "SetIconForObject", 
                          flags, 
                          null, 
                          new Type[] { typeof(UnityEngine.Object), typeof(Texture2D) }, 
                          null);
        setIcon.Invoke(null, args);

        //        var editorUtilityType = typeof(EditorUtility);
        //        editorUtilityType.InvokeMember("ForceReloadInspectors", flags, null, null, null);

    }


    
}
