using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Linq;

[InitializeOnLoad]
public class RootFolderCleanupGuard
{
    static RootFolderCleanupGuard()
    {
        System.IO.DirectoryInfo datapath = new DirectoryInfo(Application.dataPath);
        var scripts = datapath.GetFiles("*.cs");
        if(scripts.Length > 0)
        {
            Debug.Log("<color=red>SCRIPTS IN ROOT:</color> you have " + scripts.Length + " .cs files in your root. Don't be that guy.");
        }
    }
}