﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

public class GlitchMultipeerWrapper
{
    
    #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
    
    public delegate void UnityCallbackDelegate(IntPtr commandName,IntPtr commandData);

    [DllImport("multipeer")]
    public static extern void ConnectCallback([MarshalAs(UnmanagedType.FunctionPtr)] UnityCallbackDelegate callbackMethod);

    [DllImport("multipeer")]
    public static extern void GlitchMultipeerConnect(string serviceType);

    [DllImport("multipeer")]
    public static extern void GlitchMultipeerDisconnect();

    [DllImport("multipeer")]
    public static extern void SendDataToAllConnectedPeers(string dataString, bool reliably);

    #elif UNITY_IOS
    
    [DllImport("__Internal")]
    public static extern void GlitchMultipeerConnect(string serviceType);

    [DllImport("__Internal")]
    public static extern void GlitchMultipeerDisconnect();

    [DllImport("__Internal")]
    public static extern void SendDataToAllConnectedPeers(string dataString, bool reliably);

    #endif
}
