#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "GlitchMCManager.h"
#import "GlitchMCSession.h"
#import "GlitchMCNearbyServiceAdvertiser.h"

extern "C"
{
    MCSession *advertiserSession;
    BOOL isAdvertising;

    @implementation GlitchMCNearbyServiceAdvertiser
    -(id)init {
        SendUnityCommand("Error","Creating a new GlitchMCNearbyServiceAdvertiser");
        self = [super init];
        
        if (self) {
        	isAdvertising = false;
            _advertiser = nil;
            advertiserSession = nil;
        }
        
        return self;
    } 

    -(void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didNotStartAdvertisingPeer:(NSError*)error {
        SendUnityCommand("Error","didNotStartAdvertisingPeer");
    }

    -(void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID 
	        withContext:(NSData*)context
	        invitationHandler:(void (^)(BOOL accept, MCSession* session))invitationHandler {
        BOOL accept = YES;
        if (accept && isAdvertising)
        {
            SendUnityCommand(defaultCallbackMethod,"Invitation accepted");
            invitationHandler(YES,advertiserSession);
            [GlitchMCManager StopAdvertisingAndBrowsing];
        }
    }

    -(void)startAdvertising:(MCSession*)session withServiceType:(NSString*)serviceType {
    	if(!isAdvertising)
    	{
	        _advertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:session.myPeerID discoveryInfo:nil serviceType:serviceType];
	        SendUnityCommand("Error","Creating a new MCNearbyServiceAdvertiser");
	        _advertiser.delegate = self;
	        [_advertiser startAdvertisingPeer];
	        advertiserSession = session;
	        isAdvertising = true;
	    }
    }

    -(void)stopAdvertising {
    	if(isAdvertising)
    	{
    		isAdvertising = false;
    		if(_advertiser)
    		{
		        [_advertiser stopAdvertisingPeer]; 
		        advertiserSession = nil;  
		        _advertiser.delegate = nil;
		        _advertiser = nil;
		    }
	    }
    }

    -(void) dealloc {
    	_advertiser.delegate = nil;
        //[_advertiser release];
        //[super dealloc];
    }
    
    @end

}
