#import <MultipeerConnectivity/MultipeerConnectivity.h>

extern "C"
{
	@interface GlitchMCNearbyServiceBrowser : NSObject <MCNearbyServiceBrowserDelegate> 
	
	@property(nonatomic, strong) MCNearbyServiceBrowser* browser;

    -(void)startBrowsing:(MCSession *)session withServiceType:(NSString*)serviceType;
	-(void)stopBrowsing;

	@end
}

