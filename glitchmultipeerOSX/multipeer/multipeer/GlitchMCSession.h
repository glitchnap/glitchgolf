#import <MultipeerConnectivity/MultipeerConnectivity.h>

extern "C"
{
	@interface GlitchMCSession : NSObject <MCSessionDelegate> 
	
	@property(nonatomic, strong) MCSession* session;

	-(void)setupPeerAndSessionWithPeer:(MCPeerID *)peerID;
    -(void)sendDataToAllConnectedPeers:(NSData *)data withMode:(BOOL)reliably;
    -(void)disconnect;

	@end
}