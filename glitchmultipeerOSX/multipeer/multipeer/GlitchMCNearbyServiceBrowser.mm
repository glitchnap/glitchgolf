#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "GlitchMCManager.h"
#import "GlitchMCNearbyServiceBrowser.h"

extern "C"
{
	MCSession* browserSession;
    BOOL isBrowsing;

    @implementation GlitchMCNearbyServiceBrowser
    -(id)init{
    	SendUnityCommand("Error","Creating a new GlitchMCNearbyServiceBrowser");
        self = [super init];
        
        if (self) {
        	isBrowsing = false;
            _browser = nil;
            browserSession = nil;
        }
        
        return self;
    } 

    -(void)browser:(MCNearbyServiceBrowser*)browser didNotStartBrowsingForPeers:(NSError*)error {
        SendUnityCommand("Error","didNotStartBrowsingForPeers");
    }

    -(void)browser:(MCNearbyServiceBrowser*)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary*)info {
        SendUnityCommand(defaultCallbackMethod,"foundPeer");

        BOOL invite = ([browserSession.myPeerID.displayName compare:peerID.displayName]==NSOrderedDescending);
        if(invite && isBrowsing)
        {
            [_browser invitePeer:peerID toSession:browserSession withContext:nil timeout:1000];
            SendUnityCommand(defaultCallbackMethod,"invited peer");
        }
    }

    -(void)browser:(MCNearbyServiceBrowser*)browser lostPeer:(MCPeerID*)peerID {
        SendUnityCommand(defaultCallbackMethod,"lostPeer");
    }

    -(void)startBrowsing:(MCSession*)session withServiceType:(NSString*)serviceType {
    	if(!isBrowsing)
    	{
	        _browser = [[MCNearbyServiceBrowser alloc] initWithPeer:session.myPeerID serviceType:serviceType];
	        _browser.delegate = self;
	        [_browser startBrowsingForPeers];
	        browserSession = session;
	        isBrowsing = true;
	    }
    }

    -(void)stopBrowsing {
    	if(isBrowsing)
    	{
    		isBrowsing = false;
    		if(_browser)
    		{
		        [_browser stopBrowsingForPeers];   
		        _browser.delegate = nil;
		        browserSession = nil;
		        _browser = nil;
		    }
	    }
    } 

    -(void)dealloc {
    	_browser.delegate = nil;
        //[_browser release];
        browserSession = nil;
        //[super dealloc];
    }
    
    @end
}
