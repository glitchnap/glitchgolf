#import "GlitchMCManager.h"
#import "GlitchMCSession.h" 
#import "GlitchMCNearbyServiceAdvertiser.h"
#import "GlitchMCNearbyServiceBrowser.h"

extern "C"
{
	const char* unityGameObject  = "GlitchMultipeerCallbacks";
    const char* defaultCallbackMethod  = "Callback";

    static UnityCommandCallback lastCallback = NULL;
    
    void ConnectCallback(UnityCommandCallback callbackMethod)
    {
        lastCallback = callbackMethod;
    }
    
    void SendUnityCommand(const char * commandName, const char * commandData)
    {
        if(lastCallback != NULL)
        {
            lastCallback(commandName, commandData);
        }
    }
    
    @implementation GlitchMCManager

    MCPeerID* peerID;
	GlitchMCSession* session;
	GlitchMCNearbyServiceBrowser* browser;
	GlitchMCNearbyServiceAdvertiser* advertiser;

	+(void) StopAdvertisingAndBrowsing { // this exists so that we don't have to expose the browser object to the advertiser
    	SendUnityCommand("Error","StopAdvertisingAndBrowsing");
    	if(browser) {
    		[browser stopBrowsing];
    	}
    	if(advertiser) {
    		[advertiser stopAdvertising];
    	}
	}

	NSString* CreateNSString (const char* string)
	{
	   if (string)
           return [NSString stringWithUTF8String: string];
       else
           return [NSString stringWithUTF8String: ""];
	}	

	void GlitchMultipeerConnect(const char* serviceType)
	{

		NSString* serviceTypeString = CreateNSString(serviceType);
		if(!peerID) {
	        peerID = [[MCPeerID alloc] initWithDisplayName:[[NSUUID UUID] UUIDString]];
		}

    	if(!session) {
        	session = [[GlitchMCSession alloc] init];
        }
        [session setupPeerAndSessionWithPeer:peerID];

		if(!advertiser) {
			advertiser = [[GlitchMCNearbyServiceAdvertiser alloc] init];
        }
        [advertiser startAdvertising:[session session] withServiceType:serviceTypeString];

        if(!browser)
        {	
        	browser = [[GlitchMCNearbyServiceBrowser alloc] init];
        }
        [browser startBrowsing:[session session] withServiceType:serviceTypeString];

        SendUnityCommand(defaultCallbackMethod,"Finished initialization");
    }

    void GlitchMultipeerDisconnect() {
    	if(session) {
    		[session disconnect];
    	}
    	[GlitchMCManager StopAdvertisingAndBrowsing];
        SendUnityCommand(defaultCallbackMethod,"Disconnect");
    }

	void SendDataToAllConnectedPeers(const char* dataString, BOOL reliably)
	{
		NSData* data = [CreateNSString(dataString) dataUsingEncoding:NSUTF8StringEncoding];
	    [session sendDataToAllConnectedPeers:data withMode:reliably];
	}

	@end

}