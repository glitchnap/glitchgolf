#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "GlitchMCSession.h"

typedef void (* UnityCommandCallback)(const char * commandName, const char * commandData);

extern "C" {
    
    void ConnectCallback(UnityCommandCallback callbackMethod);
    
    void SendUnityCommand(const char * commandName, const char * commandData);

	extern const char* unityGameObject;
    extern const char* defaultCallbackMethod;

	@interface GlitchMCManager : NSObject

	+(void) StopAdvertisingAndBrowsing;
	void GlitchMultipeerConnect(const char* serviceType);
	void GlitchMultipeerDisconnect();
    void SendDataToAllConnectedPeers(const char* dataString, BOOL reliably);

    @end
}